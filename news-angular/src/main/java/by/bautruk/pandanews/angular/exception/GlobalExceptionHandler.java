package by.bautruk.pandanews.angular.exception;

import by.bautruk.pandanews.common.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author ivan.bautruvkevich
 *         6/7/2016 10:58 AM.
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    private final static String ATTRIBUTE_ERROR = "error";

    private final static Logger LOG = LogManager.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(ServiceException.class)
    public ModelAndView handleServiceException(Exception e) {
        ModelAndView modelAndView = new ModelAndView(ATTRIBUTE_ERROR);
        modelAndView.addObject(ATTRIBUTE_ERROR, e.getMessage());
        LOG.error(e);
        return modelAndView;
    }
}
