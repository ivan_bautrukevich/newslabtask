<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 5/24/2016
  Time: 10:47 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${locale}"/>
<fmt:bundle basename="pagecontent" prefix="page.header.">
    <div class="head">
        <div class="headerLogo">
            <a href="index.jsp"><h1><fmt:message key="header"/></h1></a>
        </div>

        <div class="headerTable">
            <table align="right">
                <tr>
                    <td>
                        <a href="controller?command=language&lang=en"><img src="${pageContext.request.contextPath}/resources/image/eng.png"
                                                                           border="0" alt="eng"></a>
                    </td>
                    <td>
                        <a href="controller?command=language&lang=ru"><img src="${pageContext.request.contextPath}/resources/image/rus.png"
                                                                           border="0" alt="rus"></a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</fmt:bundle>