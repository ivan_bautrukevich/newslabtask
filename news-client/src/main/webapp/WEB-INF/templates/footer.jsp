<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 5/24/2016
  Time: 10:44 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<fmt:setLocale value="${locale}"/>
<fmt:bundle basename="pagecontent" prefix="page.footer.">
    <footer>
        <div class="copyright">
            <fmt:message key="footer"/>
        </div>
    </footer>
</fmt:bundle>
