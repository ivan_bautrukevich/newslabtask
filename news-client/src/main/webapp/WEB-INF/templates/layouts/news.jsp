<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 5/24/2016
  Time: 11:25 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<fmt:setLocale value="${locale}"/>
<fmt:bundle basename="pagecontent" prefix="page.news.">
    <head>
          <link href="${pageContext.request.contextPath}/resources/image/favicon.ico" rel="shortcut icon"
              type="image/x-icon">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>${newsDTO.news.title}</title>
    </head>
    <body>
    <div class="main">
        <div class="newsList">
            <div class="backButton">
                <a href="index.jsp"><fmt:message key="back"/></a>
            </div>
            <div class="news">
                <div class="newsTitle">
                    <b>${newsDTO.news.title}</b>
                </div>
                <div class="newsAuthor">
                    <small>
                        (<fmt:message key="by"/>
                        <c:forEach var="author" items="${newsDTO.authors}">
                            ${author.name},
                        </c:forEach>
                        <fmt:formatDate value="${newsDTO.news.modificationDate}"/>
                        )
                    </small>
                </div>

                <div class="newsFullText">
                    <p> ${newsDTO.news.fullText}</p>
                </div>

                <div class="newsTags">
                    <c:forEach var="tag" items="${newsDTO.tags}">
                        ${tag.name}
                    </c:forEach>
                </div>

                <div>
                    <c:forEach var="comment" items="${newsDTO.comments}">
                        <div class="newsComment">
                            <fmt:formatDate value="${comment.creationDate}"/>
                                ${comment.commentText}
                        </div>
                    </c:forEach>
                </div>

                <div class="showComment">
                    <form name="form" method="POST" action="controller">
                        <input type="hidden" name="command" value="add_comment"/>
                        <input type="hidden" name="newsId" value="${newsDTO.news.id}"/>
                        <textarea class="addComment" name="comment"></textarea><br>
                        <div class="postButton">
                            <input class="secondButton" type="submit" value="<fmt:message key="post"/>"/>
                        </div>
                    </form>
                    <c:if test="${not empty commentError}">
                        <fmt:message key="error"/>
                    </c:if>
                </div>
            </div>
        </div>
        <div class="newsFooterButton">
            <div class="newsFooterPrevious">
                <a href="?command=take_news&newsId=${newsDTO.news.id}&next=false"><fmt:message key="previous"/></a>
            </div>
            <div class="newsFooterNext">
                <a href="?command=take_news&newsId=${newsDTO.news.id}&next=true"><fmt:message key="next"/></a>
            </div>
        </div>
    </div>
    </body>
</fmt:bundle>
</html>

