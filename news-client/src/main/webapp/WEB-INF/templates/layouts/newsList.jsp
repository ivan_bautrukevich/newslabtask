<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 5/24/2016
  Time: 11:14 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>

<html>
<fmt:setLocale value="${locale}"/>
<fmt:bundle basename="pagecontent" prefix="page.newsList.">
    <head>
        <link href="${pageContext.request.contextPath}/resources/image/favicon.ico" rel="shortcut icon" type="image/x-icon">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><fmt:message key="title"/></title>
    </head>
    <body>
    <div class="main">
            <%--dropdown checkbox--%>
        <div class="inline" align="center">
            <div class="inputForm">
                <form name="useFilters" method="POST" action="controller">
                    <input type="hidden" name="command" value="take_filtered_news"/>
                    <div class="dropdown-check-list">
                        <span class="anchor">author(s)</span>
                        <ul class="items">
                            <c:choose>
                                <c:when test="${empty searchAuthors}">
                                    <c:forEach var="author" items="${authors}">
                                        <li><input type="checkbox" name="checkAuthor"
                                                   value="${author.id}">${author.name}
                                        </li>
                                    </c:forEach>
                                </c:when>
                                <c:otherwise>
                                    <c:forEach var="author" items="${authors}">
                                        <c:choose>
                                            <c:when test="${ctg:contains(searchAuthors, author.id)}">
                                                <li><input type="checkbox" name="checkAuthor" value="${author.id}"
                                                           checked>${author.name}</li>
                                            </c:when>
                                            <c:otherwise>
                                                <input type="checkbox" name="checkAuthor"
                                                       value="${author.id}">${author.name}<BR>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </c:otherwise>
                            </c:choose>
                        </ul>
                    </div>
                    <div class="dropdown-check-list">
                        <span class="anchor">tag(s)</span>
                        <ul class="items">
                            <c:choose>
                                <c:when test="${empty searchTags}">
                                    <c:forEach var="tag" items="${tags}">
                                        <input type="checkbox" name="checkTag" value="${tag.id}">${tag.name}<BR>
                                    </c:forEach>
                                </c:when>
                                <c:otherwise>
                                    <c:forEach var="tag" items="${tags}">
                                        <c:choose>
                                            <c:when test="${ctg:contains(searchTags, tag.id)}">
                                                <input type="checkbox" name="checkTag" value="${tag.id}"
                                                       checked>${tag.name}
                                                <BR>
                                            </c:when>
                                            <c:otherwise>
                                                <input type="checkbox" name="checkTag" value="${tag.id}">${tag.name}<BR>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </c:otherwise>
                            </c:choose>
                        </ul>
                    </div>
                    <input class="secondButton" type="submit" value="<fmt:message key="filter"/>"/>
                </form>
            </div>
            <div class="inputForm">
                <form name="resetForm" method="GET" action="controller">
                    <input type="hidden" name="command" value="take_filtered_news"/>
                    <input type="hidden" name="reset" value="true">
                    <button class="secondButton" type="submit"><fmt:message key="reset"/></button>
                </form>
            </div>
        </div>

        <c:choose>
            <c:when test="${news.size() != 0}">
                <div class="newsList">
                    <c:forEach var="oneNews" items="${news}">
                        <div class="newsTitle">
                            <b> ${oneNews.news.title}</b>
                            <small>(by
                                <c:forEach var="oneNewsAuthor" items="${oneNews.authors}">
                                    ${oneNewsAuthor.name},
                                </c:forEach>
                                <fmt:formatDate value="${oneNews.news.modificationDate}"/>)
                            </small>
                        </div>
                        <div class="newsShortText">
                                ${oneNews.news.shortText}
                        </div>
                        <div class="newsFooter">
                            <c:forEach var="oneNewsTag" items="${oneNews.tags}">
                                <span class="newsTag">${oneNewsTag.name}</span>
                            </c:forEach>
                            <small><fmt:message key="comments"/>(${oneNews.comments.size()})</small>
                            <a href="controller?command=take_news&newsId=${oneNews.news.id}"><fmt:message
                                    key="view"/></a>
                        </div>
                    </c:forEach>
                </div>
                <div class="newsPagination">
                    <table align="center">
                        <tr>
                            <c:forEach begin="1" end="${noOfPages}" var="i">
                                <c:choose>
                                    <c:when test="${currentPage eq i}">
                                        <td>
                                            <button class="pressed-button" type="button">${i}</button>
                                        </td>
                                    </c:when>
                                    <c:otherwise>
                                        <td><a href="controller?command=take_filtered_news&page=${i}"/>
                                            <button type="button">${i}</button>
                                        </td>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </tr>
                    </table>
                </div>
            </c:when>
            <c:otherwise>
                <fmt:message key="emptynews"/>
            </c:otherwise>
        </c:choose>
    </div>
    </body>
</fmt:bundle>
</html>