package by.bautruk.pandanews.client.navigation.impl;

import by.bautruk.pandanews.client.config.ConfigManager;
import by.bautruk.pandanews.client.exception.ApplicationException;
import by.bautruk.pandanews.client.navigation.ActionCommand;
import by.bautruk.pandanews.common.entity.Author;
import by.bautruk.pandanews.common.transferobject.NewsDTO;
import by.bautruk.pandanews.common.transferobject.SearchCriteria;
import by.bautruk.pandanews.common.entity.Tag;
import by.bautruk.pandanews.common.exception.ServiceException;
import by.bautruk.pandanews.common.service.AuthorService;
import by.bautruk.pandanews.common.service.NewsDTOService;
import by.bautruk.pandanews.common.service.NewsService;
import by.bautruk.pandanews.common.service.TagService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         5/25/2016 9:40 AM.
 */
@Component(value = "take_filtered_news")
public class TakeNewsListCommand implements ActionCommand {
    private final static Logger LOG = LogManager.getLogger(TakeNewsListCommand.class);

    @Autowired
    private NewsService newsService;

    @Autowired
    private NewsDTOService newsDTOService;

    @Autowired
    private TagService tagService;

    @Autowired
    private AuthorService authorService;

    private static final String ATTRIBUTE_PREVIOUS_COMMAND = "previousCommand";
    private static final String ATTRIBUTE_NEWS = "news";
    private static final String ATTRIBUTE_TAGS = "tags";
    private static final String ATTRIBUTE_AUTHORS = "authors";
    private static final String ATTRIBUTE_SEARCH_TAGS = "searchTags";
    private static final String ATTRIBUTE_SEARCH_AUTHORS = "searchAuthors";
    private static final String ATTRIBUTE_SEARCH_CRITERIA = "searchCriteria";
    private static final String ATTRIBUTE_NUMBER_OF_PAGE = "noOfPages";
    private static final String ATTRIBUTE_CURRENT_PAGE = "currentPage";
    private static final String PARAMETER_RESET = "reset";
    private static final String PARAMETER_CHECK_TAG = "checkTag";
    private static final String PARAMETER_CHECK_AUTHOR = "checkAuthor";
    private static final String PARAMETER_PAGE = "page";
    private static final String COMMAND_NAME = "take_filtered_news";
    private static final String VIEW_PAGE = "page.showAllNews";
    private static final int PAGE_DEFAULT = 1;
    private static final int NEWS_PER_PAGE = 3;
    private static final String EQUALS_VALUE = "true";


    @Override
    public String execute(HttpServletRequest request) throws ApplicationException {

        HttpSession session = request.getSession();

        //Parse checkbox for Author
        String[] selectTagsString = request.getParameterValues(PARAMETER_CHECK_TAG);
        List<Tag> selectTags = new ArrayList<>();
        List<Long> selectTagsId = new ArrayList<>();
        if (selectTagsString != null) {
            selectTags = parseStringToTag(selectTagsString);
            selectTagsId = parseStringToLong(selectTagsString);
        }

        //Parse checkbox for Author
        String[] selectAuthorsString = request.getParameterValues(PARAMETER_CHECK_AUTHOR);
        List<Author> selectAuthors = new ArrayList<>();
        List<Long> selectAuthorsId = new ArrayList<>();
        if (selectAuthorsString != null) {
            selectAuthors = parseStringToAuthor(selectAuthorsString);
            selectAuthorsId = parseStringToLong(selectAuthorsString);
        }

        //Parse session for searchCriteria
        SearchCriteria searchCriteria = new SearchCriteria();
        if (selectTagsString == null && selectAuthorsString == null &&
                session.getAttribute(ATTRIBUTE_SEARCH_CRITERIA) != null &&
                !EQUALS_VALUE.equals(request.getParameter(PARAMETER_RESET))) {
            searchCriteria = (SearchCriteria) session.getAttribute(ATTRIBUTE_SEARCH_CRITERIA);
            selectTagsId = parseTagToLong(searchCriteria.getTags());
            selectAuthorsId = parseAuthorToLong(searchCriteria.getAuthors());
        } else {
            searchCriteria.setAuthors(selectAuthors);
            searchCriteria.setTags(selectTags);
        }


        try {
            //Pagination
            int page = PAGE_DEFAULT;
            int recordsPerPage = NEWS_PER_PAGE;
            if (request.getParameter(PARAMETER_PAGE) != null) {
                page = Integer.parseInt(request.getParameter(PARAMETER_PAGE));
            }
            int noOfRecords = newsService.countNews(searchCriteria);
            int noOfPages = (int) Math.ceil(noOfRecords / recordsPerPage);
            request.setAttribute(ATTRIBUTE_NUMBER_OF_PAGE, noOfPages);
            request.setAttribute(ATTRIBUTE_CURRENT_PAGE, page);
            Long firstOrder = Long.valueOf(recordsPerPage * page - recordsPerPage + 1);
            Long lastOrder = firstOrder + recordsPerPage - 1;

            //
            List<NewsDTO> news = newsDTOService.findNewsByOrder(searchCriteria, firstOrder, lastOrder);
            List<Tag> tags = tagService.takeAll();
            List<Author> authors = authorService.takeAll();

            //Request and session
            request.setAttribute(ATTRIBUTE_NEWS, news);
            request.setAttribute(ATTRIBUTE_TAGS, tags);
            request.setAttribute(ATTRIBUTE_AUTHORS, authors);
            request.setAttribute(ATTRIBUTE_SEARCH_AUTHORS, selectAuthorsId);
            request.setAttribute(ATTRIBUTE_SEARCH_TAGS, selectTagsId);
            session.setAttribute(ATTRIBUTE_SEARCH_CRITERIA, searchCriteria);
            session.setAttribute(ATTRIBUTE_PREVIOUS_COMMAND, COMMAND_NAME);
            return ConfigManager.getProperty(VIEW_PAGE);
        } catch (ServiceException e) {
            throw new ApplicationException(e);
        }
    }

    @Override
    public boolean getIsRedirect() {
        return false;
    }


    private List<Long> parseStringToLong(String[] selectString) {
        List<Long> list = new ArrayList<>();
        for (String select : selectString) {
            list.add(Long.valueOf(select));
        }
        return list;
    }

    private List<Tag> parseStringToTag(String[] selectTagsString) {
        List<Tag> tags = new ArrayList<>();
        for (String selectTag : selectTagsString) {
            Tag tag = new Tag();
            tag.setId(Long.valueOf(selectTag));
            tags.add(tag);
        }
        return tags;
    }

    private List<Long> parseTagToLong(List<Tag> tagList) {
        List<Long> listId = new ArrayList<>();
        for (Tag tag : tagList) {
            listId.add(tag.getId());
        }
        return listId;
    }

    private List<Author> parseStringToAuthor(String[] selectAuthorsString) {
        List<Author> authors = new ArrayList<>();
        for (String selectAuthor : selectAuthorsString) {
            Author author = new Author();
            author.setId(Long.valueOf(selectAuthor));
            authors.add(author);
        }
        return authors;
    }

    private List<Long> parseAuthorToLong(List<Author> authorList) {
        List<Long> listId = new ArrayList<>();
        for (Author author : authorList) {
            listId.add(author.getId());
        }
        return listId;
    }
}
