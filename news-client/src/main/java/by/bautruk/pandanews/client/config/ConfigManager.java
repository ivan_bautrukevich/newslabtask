package by.bautruk.pandanews.client.config;

import java.util.ResourceBundle;

/**
 * @author ivan.bautruvkevich
 *         5/18/2016 3:35 PM.
 */

public class ConfigManager {
    private static final String LINK = "config";

    private final static ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(LINK);

    private ConfigManager() {
    }

    public static String getProperty(String key) {
        return RESOURCE_BUNDLE.getString(key);
    }
}
