package by.bautruk.pandanews.client.navigation;

import by.bautruk.pandanews.client.exception.ApplicationException;

import javax.servlet.http.HttpServletRequest;

/**
 * @author ivan.bautruvkevich
 *         5/18/2016 3:27 PM.
 */
public interface ActionCommand {
    String execute(HttpServletRequest request) throws ApplicationException;
    boolean getIsRedirect();
}
