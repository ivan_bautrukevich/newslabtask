package by.bautruk.pandanews.client.utils;

import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author ivan.bautruvkevich
 *         6/21/2016 12:27 PM.
 */

@Component
public class NewsApplicationValidator {

    private static final String COMMENT = "comment";

    private static final String COMMENT_REGEXP = ".{1,100}";
    private static final int CHECK_COUNT = 1;

    /**
     * Defines the way of data validation.
     *
     * @param field is the string value, which use for find
     *              right regex expression.
     * @param data  is the string value, which have to be validated.
     * @return if the value of {@param data} is valid, returns
     * {@code true}, otherwise returns {@code false}.
     */
    public boolean checkData(String field, String data) {
        int count = 0;
        String regexpPattern = null;
        switch (field) {
            case COMMENT:
                regexpPattern = COMMENT_REGEXP;
                break;
        }
        Pattern pattern = Pattern.compile(regexpPattern);
        Matcher matcher = pattern.matcher(data);
        while (matcher.find()) {
            count++;
        }
        return count == CHECK_COUNT;
    }
}
