package by.bautruk.pandanews.client.controller;

import by.bautruk.pandanews.client.config.ConfigManager;
import by.bautruk.pandanews.client.exception.ApplicationException;
import by.bautruk.pandanews.client.navigation.ActionCommand;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author ivan.bautruvkevich
 *         5/18/2016 2:20 PM.
 */
@WebServlet("/controller")
public class Controller extends HttpServlet {

    private final static Logger LOG = LogManager.getLogger(Controller.class);

    private static final String ATTRIBUTE_ERROR = "error";
    private static final String PARAM_COMMAND = "command";
    private static final String PAGE_ERROR = "page.error";
    private static final String SPRING_CONTEXT = "spring-context-user.xml";

    private ApplicationContext context;

    @Override
    public void init() throws ServletException {
        super.init();
        context = new ClassPathXmlApplicationContext(SPRING_CONTEXT);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String page;
        ActionCommand command = (ActionCommand) context.getBean(request.getParameter(PARAM_COMMAND));
        try {
            page = command.execute(request);
        } catch (ApplicationException e) {
            request.setAttribute(ATTRIBUTE_ERROR, e.getMessage());
            page = ConfigManager.getProperty(PAGE_ERROR);
        }
        if (command.getIsRedirect()) {
            response.sendRedirect(request.getContextPath() + page);
        } else {
            RequestDispatcher requestDispatcher = request.getRequestDispatcher(page.toString());
            requestDispatcher.forward(request, response);
        }
    }
}
