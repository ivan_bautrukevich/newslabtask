package by.bautruk.pandanews.client.exception;

/**
 * @author ivan.bautruvkevich
 *         5/18/2016 2:27 PM.
 */
public class ApplicationException extends Exception {

    public ApplicationException() {

    }

    public ApplicationException(String message) {
        super(message);
    }

    public ApplicationException(Throwable cause) {
        super(cause);
    }

    public ApplicationException(String message, Throwable cause) {
        super(message, cause);
    }
}
