package by.bautruk.pandanews.client.navigation.impl;

import by.bautruk.pandanews.client.config.ConfigManager;
import by.bautruk.pandanews.client.exception.ApplicationException;
import by.bautruk.pandanews.client.navigation.ActionCommand;
import by.bautruk.pandanews.common.entity.Author;
import by.bautruk.pandanews.common.transferobject.NewsDTO;
import by.bautruk.pandanews.common.transferobject.SearchCriteria;
import by.bautruk.pandanews.common.entity.Tag;
import by.bautruk.pandanews.common.exception.ServiceException;
import by.bautruk.pandanews.common.service.NewsDTOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;

/**
 * @author ivan.bautruvkevich
 *         5/25/2016 11:14 AM.
 */
@Component(value = "take_news")
public class TakeNewsCommand implements ActionCommand {

    @Autowired
    private NewsDTOService newsDTOService;

    private static final String ATTRIBUTE_PREVIOUS_COMMAND = "previousCommand";
    private static final String ATTRIBUTE_COMMENT_ERROR = "commentError";
    private static final String ATTRIBUTE_SEARCH_CRITERIA = "searchCriteria";
    private static final String ATTRIBUTE_NEWS_DTO = "newsDTO";
    private static final String EXCEPTION_MESSAGE = "Invalid parameter";
    private static final String PARAMETER_COMMENT = "comment";
    private static final String PARAMETER_PREVIOUS_COMMAND = "take_news&newsId=";
    private static final String PARAMETER_NEWS_ID = "newsId";
    private static final String PARAMETER_NEXT = "next";
    private static final String VIEW_PAGE = "page.showNews";
    private static final String EQUALS_VALUE = "false";

    @Override
    public String execute(HttpServletRequest request) throws ApplicationException {
        HttpSession session = request.getSession();
        if (request.getParameter(PARAMETER_NEWS_ID) == null) throw new ApplicationException(EXCEPTION_MESSAGE);
        Long newsId = Long.valueOf(request.getParameter(PARAMETER_NEWS_ID));

        SearchCriteria searchCriteria;
        if (session.getAttribute(ATTRIBUTE_SEARCH_CRITERIA) == null) {
            searchCriteria = new SearchCriteria();
            searchCriteria.setAuthors(new ArrayList<Author>());
            searchCriteria.setTags(new ArrayList<Tag>());
        } else {
            searchCriteria = (SearchCriteria) session.getAttribute(ATTRIBUTE_SEARCH_CRITERIA);
        }

        try {
            NewsDTO newsDTO;
            if (request.getParameter(PARAMETER_NEXT) == null) {
                newsDTO = newsDTOService.takeNews(newsId);
            } else {
                boolean next = Boolean.valueOf(request.getParameter(PARAMETER_NEXT));
                newsDTO = newsDTOService.findNexNewsByNewsId(searchCriteria, newsId, next);
                //if next/previous news doesn't exist - back current news
                if (newsDTO == null || newsDTO.getNews() == null) {
                    newsDTO = newsDTOService.takeNews(newsId);
                }
            }

            //check for error
            if (request.getParameter(PARAMETER_COMMENT) != null &&
                    EQUALS_VALUE.equals(request.getParameter(PARAMETER_COMMENT))) {
                request.setAttribute(ATTRIBUTE_COMMENT_ERROR, true);
            }
            request.setAttribute(ATTRIBUTE_NEWS_DTO, newsDTO);
            session.setAttribute(ATTRIBUTE_PREVIOUS_COMMAND, PARAMETER_PREVIOUS_COMMAND + newsDTO.getNews().getId());
            return ConfigManager.getProperty(VIEW_PAGE);
        } catch (ServiceException e) {
            throw new ApplicationException(e);
        }
    }

    @Override
    public boolean getIsRedirect() {
        return false;
    }
}
