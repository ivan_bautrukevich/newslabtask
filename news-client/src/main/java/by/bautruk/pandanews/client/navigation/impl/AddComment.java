package by.bautruk.pandanews.client.navigation.impl;

import by.bautruk.pandanews.client.exception.ApplicationException;
import by.bautruk.pandanews.client.navigation.ActionCommand;
import by.bautruk.pandanews.client.utils.NewsApplicationValidator;
import by.bautruk.pandanews.common.entity.Comment;
import by.bautruk.pandanews.common.exception.ServiceException;
import by.bautruk.pandanews.common.service.CommentService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @author ivan.bautruvkevich
 *         5/25/2016 2:20 PM.
 */
@Component(value = "add_comment")
public class AddComment implements ActionCommand {

    private final static Logger LOG = LogManager.getLogger(AddComment.class);

    private static final String CONTROLLER_COMMAND = "/controller?command=take_news&newsId=";
    private static final String PARAMETER_COMMENT = "comment";
    private static final String PARAMETER_NEWS_ID = "newsId";
    private static final String PARAMETER_AND_VALUE = "&comment=false";

    @Autowired
    private CommentService commentService;

    @Autowired
    private NewsApplicationValidator validator;

    @Override
    public String execute(HttpServletRequest request) throws ApplicationException {
        String text = request.getParameter(PARAMETER_COMMENT);
        Long newsId = Long.valueOf(request.getParameter(PARAMETER_NEWS_ID));
        if (validator.checkData(PARAMETER_COMMENT, text)) {
            try {
                Comment comment = new Comment();
                comment.setCommentText(text);
                comment.setNewsId(newsId);
                commentService.insert(comment);
            } catch (ServiceException e) {
                throw new ApplicationException(e);
            }
            return CONTROLLER_COMMAND + newsId;
        } else {
            return CONTROLLER_COMMAND + newsId + PARAMETER_AND_VALUE;
        }
    }

    public boolean getIsRedirect(){
        return true;
    }
}
