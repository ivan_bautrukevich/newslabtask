package by.bautruk.pandanews.client.listner;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * @author ivan.bautruvkevich
 *         5/24/2016 5:41 PM.
 */
@WebListener
public class NewsListener implements HttpSessionListener {

    private static final String LOCALE = "locale";
    private static final String DEFAULT_LOCALE = "en_US";

    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        httpSessionEvent.getSession().setAttribute(LOCALE, DEFAULT_LOCALE);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {

    }
}
