package by.bautruk.pandanews.client.tagfunction;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         6/16/2016 5:42 PM.
 */
public class CheckObjectInList {
    public static boolean contains(List list, Long o) {
        return list.contains(o);
    }
}
