package by.bautruk.pandanews.client.navigation.impl;

import by.bautruk.pandanews.client.exception.ApplicationException;
import by.bautruk.pandanews.client.navigation.ActionCommand;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


/**
 * @author ivan.bautruvkevich
 *         5/18/2016 3:57 PM.
 *         The {@code LanguageCommand} class represents an ability of manipulation
 *         with an instance of {@code HttpServletRequest}.
 */
@Component(value = "language")
public class LanguageCommand implements ActionCommand {
    private static final String ATTRIBUTE_LOCALE = "locale";
    private static final String ATTRIBUTE_PREVIOUS_COMMAND = "previousCommand";
    private static final String PARAMETER_LANGUAGE = "lang";
    private static final String EN = "en";
    private static final String RU = "ru";
    private static final String ATTRIBUTE_EN = "en_US";
    private static final String ATTRIBUTE_RU = "ru_RU";
    private static final String CONTROLLER_COMMAND = "/controller?command=";

    /**
     * Change language of page.
     *
     * @param request is an instance of {@code HttpServletRequest}.
     * @return a result page at which should be forwarded to.
     * @throws ApplicationException if there are any troubles with PaymentAccountService
     *                              if user doesn't have access for this operation.
     */


    @Override
    public String execute(HttpServletRequest request) throws ApplicationException {
        String language = request.getParameter(PARAMETER_LANGUAGE);
        HttpSession session = request.getSession();
        switch (language) {
            case EN:
                session.setAttribute(ATTRIBUTE_LOCALE, ATTRIBUTE_EN);
                break;
            case RU:
                session.setAttribute(ATTRIBUTE_LOCALE, ATTRIBUTE_RU);
                break;
        }
        String previousCommand = (String) session.getAttribute(ATTRIBUTE_PREVIOUS_COMMAND);
        return CONTROLLER_COMMAND + previousCommand;
    }

    @Override
    public boolean getIsRedirect() {
        return false;
    }
}
