create table "ROLES"
(
  ROLE_ID NUMBER(20) not null,
  ROLE_NAME NVARCHAR2(50) not null
);
alter table "ROLES" add primary key (ROLE_ID);
CREATE SEQUENCE roles_seq;

CREATE OR REPLACE TRIGGER roles_bir 
BEFORE INSERT ON "ROLES"
FOR EACH ROW
BEGIN
  if :new.ROLE_ID is null then
    select roles_seq.nextval 
    into :new.ROLE_ID from dual;
  end if;
END;
/

create table Users
(
  USER_ID NUMBER(20) not null,
  USER_NAME NVARCHAR2(50) not null,
  LOGIN VARCHAR2(32) UNIQUE not null,
  ROLE_ID NUMBER(20) not null,
  PASSWORD VARCHAR2(32) not null
);
alter table Users add primary key (USER_ID);
alter table Users
  add constraint USER_FOR_KEY foreign key (ROLE_ID)
  references "ROLES" (ROLE_ID);
CREATE SEQUENCE users_seq;

CREATE OR REPLACE TRIGGER users_bir 
BEFORE INSERT ON Users
FOR EACH ROW
BEGIN
  if :new.USER_ID is null then
    select users_seq.nextval 
    into :new.USER_ID from dual;
  end if;
END;
/

create table News
(
  NEWS_ID NUMBER(20) not null,
   OPTLOCK NUMBER(20) DEFAULT 0,
  TITLE NVARCHAR2(30) not null,
  SHORT_TEXT NVARCHAR2(100) not null,
  FULL_TEXT NVARCHAR2(2000) not null,
  CREATION_DATE TIMESTAMP not null,
  MODIFICATION_DATE TIMESTAMP
);
alter table News add primary key (NEWS_ID);
CREATE SEQUENCE news_seq;

CREATE OR REPLACE TRIGGER news_bir 
BEFORE INSERT ON News
FOR EACH ROW
BEGIN
  if :new.NEWS_ID is null then
    select news_seq.nextval 
    into :new.NEWS_ID from dual;
  end if;
END;
/

create table Tags
(
  TAG_ID NUMBER(20) not null,
  TAG_NAME NVARCHAR2(30) not null
);
alter table Tags add primary key (TAG_ID);
CREATE SEQUENCE tags_seq;

CREATE OR REPLACE TRIGGER tags_bir 
BEFORE INSERT ON Tags
FOR EACH ROW
BEGIN
  if :new.TAG_ID is null then
    select tags_seq.nextval 
    into :new.TAG_ID from dual;
  end if;
END;
/

create table News_Tags
(
  NEWS_ID NUMBER(20) not null,
  TAG_ID NUMBER(20) not null
);
alter table News_Tags
  add constraint NEWS_FOR_KEY foreign key (NEWS_ID)
  references News (NEWS_ID);
alter table News_Tags
  add constraint TAG_FOR_KEY foreign key (TAG_ID)
  references Tags (TAG_ID);

create table Authors
(
  AUTHOR_ID NUMBER(20) not null,
  AUTHOR_NAME NVARCHAR2(20) not null,
  EXPIRED TIMESTAMP
);
alter table Authors add primary key (AUTHOR_ID);
CREATE SEQUENCE authors_seq;

CREATE OR REPLACE TRIGGER authors_bir 
BEFORE INSERT ON Authors
FOR EACH ROW
BEGIN
  if :new.AUTHOR_ID is null then
    select authors_seq.nextval 
    into :new.AUTHOR_ID from dual;
  end if;
END;
/

create table News_Authors
(
  NEWS_ID NUMBER(20) not null,
  AUTHOR_ID NUMBER(20) not null
);
alter table News_Authors
  add constraint AUTH_NEWS_FOR_KEY foreign key (NEWS_ID)
  references News (NEWS_ID);
alter table News_Authors
  add constraint AUTHOR_FOR_KEY foreign key (AUTHOR_ID)
  references Authors (AUTHOR_ID);
  
create table Comments
(
  COMMENT_ID NUMBER(20) not null,
  NEWS_ID NUMBER(20) not null,
  COMMENT_TEXT NVARCHAR2(100) not null,
  CREATION_DATE TIMESTAMP not null
);
alter table Comments add primary key (COMMENT_ID);
alter table Comments
  add constraint COM_NEWS_FOR_KEY foreign key (NEWS_ID)
  references News (NEWS_ID);
CREATE SEQUENCE comments_seq;

CREATE OR REPLACE TRIGGER comments_bir 
BEFORE INSERT ON Comments
FOR EACH ROW
BEGIN
  if :new.COMMENT_ID is null then
    select comments_seq.nextval 
    into :new.COMMENT_ID from dual;
  end if;
END;
/