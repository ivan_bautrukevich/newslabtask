package by.bautruk.pandanews.struts.action;

import by.bautruk.pandanews.common.entity.Author;
import by.bautruk.pandanews.common.exception.ServiceException;
import by.bautruk.pandanews.common.service.AuthorService;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         7/22/2016 9:03 AM.
 */
@Component
@Scope("prototype")
public class AuthorAction extends ActionSupport {
    private final static Logger LOG = LogManager.getLogger(NewsAction.class);

    private Author author;
    private List<Author> authors;
    private Long id;

    private static final String FIELD_NAME = "author.name";
    private static final String ERROR_CODE_NAME_REQUIRED = "page.authors.name.required";
    private static final String ERROR_CODE_NAME_TOO_LONG = "page.authors.name.toolong";

    @Autowired
    private AuthorService authorService;

    @SkipValidation
    public String list() {
        try {
            author = new Author();
            authors = authorService.takeAllNotExpired();
            return SUCCESS;
        } catch (ServiceException e) {
            LOG.error(e);
            return ERROR;
        }
    }

    public String save() {
        try {
            authorService.insert(author);
            return SUCCESS;
        } catch (ServiceException e) {
            LOG.debug(e);
            return ERROR;
        }
    }

    public String edit() {
        try {
            authorService.updateById(author.getId(), author);
            return SUCCESS;
        } catch (ServiceException e) {
            LOG.debug(e);
            return ERROR;
        }
    }

    @SkipValidation
    public String expire() {
        try {
            LOG.debug(id);
            authorService.makeAuthorExpired(id);
            return SUCCESS;
        } catch (ServiceException e) {
            LOG.debug(e);
            return ERROR;
        }
    }

    public void validate() {
        if (getAuthor().getName().length() == 0) {
            addFieldError(FIELD_NAME, getText(ERROR_CODE_NAME_REQUIRED));
        }

        if (getAuthor().getName().length() > 20) {
            addFieldError(FIELD_NAME, getText(ERROR_CODE_NAME_TOO_LONG));
        }
        if (getFieldErrors().size() != 0) {
            try {
                authors = authorService.takeAllNotExpired();
            } catch (ServiceException e) {
                LOG.error(e);
            }
        }
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
