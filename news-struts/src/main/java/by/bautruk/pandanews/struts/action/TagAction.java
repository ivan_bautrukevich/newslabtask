package by.bautruk.pandanews.struts.action;

import by.bautruk.pandanews.common.entity.Tag;
import by.bautruk.pandanews.common.exception.ServiceException;
import by.bautruk.pandanews.common.service.TagService;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         7/22/2016 9:03 AM.
 */
@Component
@Scope("prototype")
public class TagAction extends ActionSupport {

    private final static Logger LOG = LogManager.getLogger(NewsAction.class);

    private Tag tag;
    private List<Tag> tags;
    private Long id;

    private static final String FIELD_NAME = "tag.name";
    private static final String ERROR_CODE_NAME_REQUIRED = "page.tags.name.required";
    private static final String ERROR_CODE_NAME_TOO_LONG = "page.tags.name.toolong";

    @Autowired
    private TagService tagService;

    @SkipValidation
    public String list() {
        try {
            tag = new Tag();
            tags = tagService.takeAll();
            return SUCCESS;
        } catch (ServiceException e) {
            LOG.error(e);
            return ERROR;
        }
    }

    public String save() {
        try {
            tagService.insert(tag);
            return SUCCESS;
        } catch (ServiceException e) {
            LOG.debug(e);
            return ERROR;
        }
    }

    public String edit() {
        try {
            tagService.updateById(tag.getId(), tag);
            return SUCCESS;
        } catch (ServiceException e) {
            LOG.debug(e);
            return ERROR;
        }
    }

    @SkipValidation
    public String delete() {
        try {
            tagService.deleteById(id);
            return SUCCESS;
        } catch (ServiceException e) {
            LOG.debug(e);
            return ERROR;
        }
    }

    public void validate() {
        if (tag.getName().length() == 0) {
            addFieldError(FIELD_NAME, getText(ERROR_CODE_NAME_REQUIRED));
        }

        if (tag.getName().length() > 30) {
            addFieldError(FIELD_NAME, getText(ERROR_CODE_NAME_TOO_LONG));
        }
        if (getFieldErrors().size() != 0) {
            try {
                tags = tagService.takeAll();
            } catch (ServiceException e) {
                LOG.error(e);
            }
        }
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
