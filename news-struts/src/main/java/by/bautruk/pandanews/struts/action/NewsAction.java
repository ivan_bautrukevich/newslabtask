package by.bautruk.pandanews.struts.action;

import by.bautruk.pandanews.common.entity.*;
import by.bautruk.pandanews.common.exception.ServiceException;
import by.bautruk.pandanews.common.service.AuthorService;
import by.bautruk.pandanews.common.service.NewsDTOService;
import by.bautruk.pandanews.common.service.TagService;
import by.bautruk.pandanews.common.transferobject.NewsDTO;
import by.bautruk.pandanews.common.transferobject.NewsTO;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Scope("prototype")
public class NewsAction extends ActionSupport {

    private final static Logger LOG = LogManager.getLogger(NewsAction.class);

    private static final String FIELD_TITLE = "newsDTO.news.title";
    private static final String FIELD_SHORT_TEXT = "newsDTO.news.shortText";
    private static final String FIELD_FULL_TEXT = "newsDTO.news.fullText";
    private static final String FIELD_AUTHOR = "newsTO.authors";
    private static final String FIELD_TAGS = "newsTO.tags";
    private static final String ERROR_CODE_TITLE_REQUIRED = "page.newsForm.title.required";
    private static final String ERROR_CODE_SHORT_TEXT_REQUIRED = "page.newsForm.shortText.required";
    private static final String ERROR_CODE_FULL_TEXT_REQUIRED = "page.newsForm.fullText.required";
    private static final String ERROR_CODE_AUTHOR_REQUIRED = "page.newsForm.tags.required";
    private static final String ERROR_CODE_TAGS_REQUIRED = "page.tags.name.required";
    private static final String ERROR_CODE_TITLE_TOO_LONG = "page.newsForm.title.toolong";
    private static final String ERROR_CODE_SHORT_TEXT_TOO_LONG = "page.newsForm.shortText.toolong";
    private static final String ERROR_CODE_FULL_TEXT_TOO_LONG = "page.newsForm.fullText.toolong";

    @Autowired
    private NewsDTOService newsDTOService;

    @Autowired
    private TagService tagService;

    @Autowired
    private AuthorService authorService;

    private NewsDTO newsDTO;
    private NewsTO newsTO;
    private Long id;
    private List<Author> authors;
    private List<Tag> tags;

    public String list() {
        return SUCCESS;
    }

    @SkipValidation
    public String view() {
        try {
            newsDTO = newsDTOService.takeNews(id);
            return SUCCESS;
        } catch (ServiceException e) {
            LOG.error(e);
            return ERROR;
        }
    }

    @SkipValidation
    public String edit() {
        try {
            authors = authorService.takeAllNotExpired();
            tags = tagService.takeAll();
            newsDTO = newsDTOService.takeNews(id);
            LOG.debug(newsDTO);
            return SUCCESS;
        } catch (ServiceException e) {
            LOG.error(e);
            return ERROR;
        }
    }


    public String cancel() {
        return SUCCESS;
    }

    public String save() {
        try {
            News news = newsDTO.getNews();
            newsTO.setNews(news);
            id = newsDTOService.addNewsTO(newsTO);
            return SUCCESS;
        } catch (ServiceException e) {
            LOG.error(e);
            return ERROR;
        }
    }


    @SkipValidation
    public String add() {
        if (newsDTO == null) {
            newsDTO = new NewsDTO();
        }
        newsDTO.setAuthors(new ArrayList<>());
        newsDTO.setTags(new ArrayList<>());
        try {
            authors = authorService.takeAllNotExpired();
            tags = tagService.takeAll();
            return SUCCESS;
        } catch (ServiceException e) {
            LOG.error(e);
            return ERROR;
        }
    }

    public void validate() {
        if (newsDTO.getNews().getTitle().length() == 0) {
            addFieldError(FIELD_TITLE, getText(ERROR_CODE_TITLE_REQUIRED));
        }
        if (newsDTO.getNews().getTitle().length() > 30) {
            addFieldError(FIELD_TITLE, getText(ERROR_CODE_TITLE_TOO_LONG));
        }
        if (newsDTO.getNews().getShortText().length() == 0) {
            addFieldError(FIELD_SHORT_TEXT, getText(ERROR_CODE_SHORT_TEXT_REQUIRED));
        }
        if (newsDTO.getNews().getShortText().length() > 100) {
            addFieldError(FIELD_SHORT_TEXT, getText(ERROR_CODE_SHORT_TEXT_TOO_LONG));
        }
        if (newsDTO.getNews().getFullText().length() == 0) {
            addFieldError(FIELD_FULL_TEXT, getText(ERROR_CODE_FULL_TEXT_REQUIRED));
        }
        if (newsDTO.getNews().getFullText().length() > 2000) {
            addFieldError(FIELD_FULL_TEXT, getText(ERROR_CODE_FULL_TEXT_TOO_LONG));
        }
        if (newsTO == null || newsTO.getAuthors() == null || newsTO.getAuthors().size() == 0) {
            addFieldError(FIELD_AUTHOR, getText(ERROR_CODE_AUTHOR_REQUIRED));
        }
        if (newsTO == null || newsTO.getTags() == null || newsTO.getTags().size() == 0) {
            addFieldError(FIELD_TAGS, getText(ERROR_CODE_TAGS_REQUIRED));
        }
        if (getFieldErrors().size() != 0) {
            try {
                authors = authorService.takeAllNotExpired();
                tags = tagService.takeAll();
            } catch (ServiceException e) {
                LOG.error(e);
            }
        }
    }

    public NewsDTO getNewsDTO() {
        return newsDTO;
    }

    public void setNewsDTO(NewsDTO newsDTO) {
        this.newsDTO = newsDTO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public NewsTO getNewsTO() {
        return newsTO;
    }

    public void setNewsTO(NewsTO newsTO) {
        this.newsTO = newsTO;
    }
}