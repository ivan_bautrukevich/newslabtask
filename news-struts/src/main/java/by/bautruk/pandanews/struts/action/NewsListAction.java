package by.bautruk.pandanews.struts.action;

import by.bautruk.pandanews.common.entity.Author;
import by.bautruk.pandanews.common.transferobject.NewsDTO;
import by.bautruk.pandanews.common.transferobject.SearchCriteria;
import by.bautruk.pandanews.common.entity.Tag;
import by.bautruk.pandanews.common.exception.ServiceException;
import by.bautruk.pandanews.common.service.NewsDTOService;
import by.bautruk.pandanews.common.service.NewsService;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         7/21/2016 4:58 PM.
 */
@Component
@Scope("prototype")
public class NewsListAction extends ActionSupport {

    private final static Logger LOG = LogManager.getLogger(NewsAction.class);

    private static final int PAGE_DEFAULT = 1;
    private static final int NEWS_PER_PAGE = 3;

    @Autowired
    private NewsDTOService newsDTOService;

    @Autowired
    private NewsService newsService;

    private List<NewsDTO> newsDTOList;
    private List<String> stringIdList;
    private SearchCriteria searchCriteria;
    private int page;
    private int noOfPages;
    private int recordsPerPage = NEWS_PER_PAGE;


    public String list() {
        try {
            if (searchCriteria == null) {
                searchCriteria = new SearchCriteria();
                searchCriteria.setAuthors(new ArrayList<Author>());
                searchCriteria.setTags(new ArrayList<Tag>());
            }
            //pagination
            if (page == 0) {
                page = PAGE_DEFAULT;
            }
            int noOfRecords = newsService.countNews(searchCriteria);
            noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
            Long firstOrder = Long.valueOf(recordsPerPage * page - recordsPerPage + 1);
            Long lastOrder = firstOrder + recordsPerPage - 1;

            newsDTOList = newsDTOService.findNewsByOrder(searchCriteria, firstOrder, lastOrder);
        } catch (ServiceException e) {
            LOG.error(e);
            return ERROR;
        }
        return SUCCESS;
    }

    public String delete() {
        try {
            if (stringIdList != null) {
                newsService.deleteByListOfId(parseStringToLong(stringIdList));
                return SUCCESS;
            } else {
                return ERROR;
            }
        } catch (ServiceException e) {
            LOG.error(e);
            return ERROR;
        }
    }

    public List<NewsDTO> getNewsDTOList() {
        return newsDTOList;
    }

    public void setNewsDTOList(List<NewsDTO> newsDTOList) {
        this.newsDTOList = newsDTOList;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getNoOfPages() {
        return noOfPages;
    }

    public void setNoOfPages(int noOfPages) {
        this.noOfPages = noOfPages;
    }

    public int getRecordsPerPage() {
        return recordsPerPage;
    }

    public void setRecordsPerPage(int recordsPerPage) {
        this.recordsPerPage = recordsPerPage;
    }

    public SearchCriteria getSearchCriteria() {
        return searchCriteria;
    }

    public void setSearchCriteria(SearchCriteria searchCriteria) {
        this.searchCriteria = searchCriteria;
    }

    public List<String> getStringIdList() {
        return stringIdList;
    }

    public void setStringIdList(List<String> stringIdList) {
        this.stringIdList = stringIdList;
    }

    private List<Long> parseStringToLong(List<String> selectString) {
        List<Long> list = new ArrayList<>();
        for (String select : selectString) {
            list.add(Long.valueOf(select));
        }
        return list;
    }
}
