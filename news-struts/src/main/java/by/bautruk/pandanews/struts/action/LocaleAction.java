package by.bautruk.pandanews.struts.action;

import com.opensymphony.xwork2.ActionSupport;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author ivan.bautruvkevich
 *         7/20/2016 2:22 PM.
 */
@Component
@Scope("prototype")
public class LocaleAction extends ActionSupport {

    public String execute() {
        return SUCCESS;
    }
}
