<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 27.05.2016
  Time: 11:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="/struts-tags" prefix="s" %>

<div class="menuAdmin">
    <ul>
        <li><a href="<s:url namespace="/" action="AllNews"/>"> <s:text name="page.menu.newsList"/></a></li>
        <li><a href="<s:url namespace="/"  action="AddNews"/>"> <s:text name="page.menu.addNews"/></a></li>
        <li><a href="<s:url namespace="/tags" action="AllTags"/>"> <s:text name="page.menu.updateTags"/></a></li>
        <li><a href="<s:url namespace="/authors" action="AllAuthors"/>"> <s:text name="page.menu.updateAuthors"/></a>
        </li>
    </ul>
</div>

