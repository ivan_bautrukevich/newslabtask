<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 5/24/2016
  Time: 10:47 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="/struts-tags" prefix="s" %>


<div class="head">
    <div class="headerLogo">

        <a href="<s:url action="AllNews" />"><h1><s:text name="page.header.header"/></h1></a>
    </div>
    <s:url id="localeEN" namespace="/" action="locale" >
        <s:param name="request_locale" >en_US</s:param>
    </s:url>
    <s:url id="localeRU" namespace="/" action="locale" >
        <s:param name="request_locale" >ru_RU</s:param>
    </s:url>
    <div class="headerTable">
        <table align="right">
            <tr>
                <td>
                    <s:a href="%{localeEN}"><img
                            src="<s:url value="/resources/image/eng.png" />" border="0" alt="eng"></s:a>
                </td>
                <td>
                    <s:a href="%{localeRU}"><img
                            src="<s:url value="/resources/image/rus.png" />" border="0" alt="rus"></s:a>
                </td>
            </tr>
        </table>
    </div>
</div>
