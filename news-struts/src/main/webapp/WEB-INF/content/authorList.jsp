<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 6/3/2016
  Time: 3:29 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script type="text/javascript" src="<s:url value="/resources/js/appscript.js" />"></script>
    <title><s:text name="page.authors.title"/></title>
</head>
<body>
<div class="main">
    <div class="mainList">
        <s:iterator value="authors" var="author">
            <s:div cssClass="inline" theme="simple">

                <s:form id="updateAuthor%{id}" cssClass="inputForm" namespace="/authors" action="EditAuthor"
                        method="POST">
                    <s:textarea cssClass="inputObject" id="inputObject%{id}" key="page.authors.author"
                                name="author.name" value="%{#author.name}" disabled="true"/>
                    <s:hidden name="author.id" value="%{id}"/>
                </s:form>

                <s:div cssClass="hiddenDib" id="buttonSet%{#author.id}" style="display:none" theme="simple">
                    <s:submit cssClass="hiddenButton" form="updateAuthor%{#author.id}" key="page.authors.update"
                              theme="simple"/>
                    <s:submit cssClass="hiddenButton" form="deleteAuthor%{#author.id}" key="page.authors.expire"
                              theme="simple"/>
                    <input class="cancelButton" type="button"
                           value="<s:text name="page.authors.cancel"/>">
                </s:div>

                <s:submit cssClass="showButton" data-id="%{#author.id}" type="button" key="page.authors.edit"
                          theme="simple"/>

                <s:form id="deleteAuthor%{id}" namespace="/authors" action="DeleteAuthor" method="POST">
                    <s:hidden name="id" value="%{id}"/>
                </s:form>
            </s:div>
        </s:iterator>


        <s:div cssClass="inline" theme="simple">
            <s:form id="newAuthor" cssClass="cancelButton" namespace="/authors" action="SaveAuthor" method="POST">
                <s:textarea cssClass="textareaForm" key="page.authors.addauthor" name="author.name"/>
            </s:form>
            <s:submit form="newAuthor" cssClass="submitButton" key="page.authors.save" theme="simple"/>
        </s:div>
    </div>
</div>
</body>
