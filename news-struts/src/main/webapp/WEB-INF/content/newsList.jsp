<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 5/24/2016
  Time: 11:14 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="/struts-tags" prefix="s" %>

<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script type="text/javascript" src="<s:url value="/resources/js/appscript.js" />"></script>
    <title><s:text name="page.newsList.title"/></title>
</head>
<body>
<div class="main">
    <div class="newsList">
        <s:form namespace="/" action="DeleteNews">
            <s:iterator value="newsDTOList">
                <div class="newsTitle">
                    <b><s:property value="news.title"/></b>
                    <small>(by
                        <s:iterator value="authors">
                            <s:property value="name"/>,
                        </s:iterator>
                        <s:date name="news.modificationDate" format="dd/MM/yyyy"/>)
                    </small>
                </div>

                <div class="newsShortText">
                    <s:property value="news.shortText"/>
                </div>

                <div class="newsFooter">
                    <s:iterator value="tags">
                        <span class="newsTag"><s:property value="name"/></span>
                    </s:iterator>

                </div>

                <div class="newsFooter">
                    <s:url id="showId" namespace="/" action="ShowNews">
                        <s:param name="id"><s:property value="news.id"/></s:param>
                    </s:url>

                    <s:a href="%{showId}"><s:text name="page.newsList.view"/></s:a>

                    <s:url id="editId" namespace="/" action="EditNews">
                        <s:param name="id"><s:property value="news.id"/></s:param>
                    </s:url>

                    <s:a href="%{editId}"><s:text name="page.newsList.edit"/></s:a>
                    <s:checkbox name="stringIdList" fieldValue="%{news.id}" theme="simple"/>
                </div>

            </s:iterator>
            <s:div cssClass="deleteButton">
                <s:submit cssClass="secondButton" id="deleteButton" theme="simple" key="page.newsList.delete"
                          align="right"/>
            </s:div>
        </s:form>
    </div>

    <div class="pageNumber">
        <s:iterator var="currentPage" begin="1" end="noOfPages">
            <s:if test="#currentPage!= page">
                <s:url id="changePage" namespace="/" action="AllNews">
                    <s:param name="page"><s:property/></s:param>
                </s:url>
                <s:a href="%{changePage}"> <s:submit type="button" theme="simple"><s:property/></s:submit> </s:a>
            </s:if>
            <s:else>
                <s:submit cssClass="pressed-button" type="button" theme="simple"><s:property/></s:submit>
            </s:else>
        </s:iterator>
    </div>
</div>
</body>
</html>