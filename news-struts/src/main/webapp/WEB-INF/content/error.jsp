<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 7/22/2016
  Time: 9:36 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="/struts-tags" prefix="s" %>
<html>
<head>
    <title>
        <s:text name="page.error.title"/>
    </title>
</head>
<body>
<div class="main">
    <div class="newsList">
        <s:text name="page.error.message"/>
    </div>
</div>
</body>
</html>
