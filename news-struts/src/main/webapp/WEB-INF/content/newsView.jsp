<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 5/31/2016
  Time: 4:59 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib uri="/struts-tags" prefix="s" %>

<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script type="text/javascript" src="<s:url value="/resources/js/appscript.js" />"></script>
    <title><s:property value="newsDTO.news.title"/></title>
</head>
<body>
<div class="main">
    <div class="showNews">
        <table align="center">
            <tr>
                <td height="20"></td>
            </tr>
            <tr>
                <td colspan="2">
                    <h3><s:property value="newsDTO.news.title"/></h3>
                </td>
            </tr>
            <tr class="authorDateSize">
                <td align="left">
                    <small>
                        (by
                        <s:iterator value="newsDTO.authors" var="author">
                            <s:property value="#author.name"/>
                        </s:iterator>
                        <s:date name="newsDTO.news.creationDate" format="dd/MM/yyyy"/>
                        )
                    </small>
                </td>
                <td align="right">

                </td>
            </tr>
            <tr>
                <td height="20"></td>
            <tr>
                <td><s:property value="newsDTO.news.shortText"/></td>
            </tr>
            <tr>
                <td height="20"></td>
            </tr>
            <tr>
                <td><s:property value="newsDTO.news.fullText"/></td>
            </tr>

            <tr>
                <td height="20"></td>
            </tr>
            <tr>
                <td height="20"></td>
            </tr>
            <tr>
                <td align="right">
                    <s:iterator value="newsDTO.tags" var="tag">
                        <s:property value="#tag.name"/>
                    </s:iterator>
                </td>
            </tr>
            <tr>
                <td align="right" valign="top">
                    <s:url id="editId" namespace="/" action="EditNews">
                        <s:param name="id"><s:property value="newsDTO.news.id"/></s:param>
                    </s:url>
                    <s:a href="%{editId}"><s:submit cssClass="secondButton" type="button" theme="simple"><s:text
                            name="page.newsList.edit"/></s:submit> </s:a>
                </td>
                <td>
                    <s:form namespace="/" action="DeleteNews" theme="simple">
                        <s:hidden name="stringIdList" value="%{newsDTO.news.id}"/>
                        <s:submit cssClass="secondButton" id="deleteButton" theme="simple" key="page.newsList.delete"
                                  align="right"/>
                    </s:form>
                </td>
            </tr>
        </table>
    </div>
</div>
</body>
