<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 5/31/2016
  Time: 3:28 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib uri="/struts-tags" prefix="s" %>

<head>
    <title><s:text name="page.newsPage.title"/></title>
</head>
<body>
<div class="main">
    <s:form action="SaveNews" namespace="/" method="POST">
        <table align="center">
            <tr>
                <td height="20"></td>
            </tr>
            <tr>
                <td><s:textarea cssClass="inputtitle" key="page.news.title" name="newsDTO.news.title"/></td>
            </tr>
            <tr>
                <td height="20"></td>
            </tr>
            <tr>
                <td><s:textarea cssClass="inputbrief" key="page.news.brief" name="newsDTO.news.shortText"/></td>
            </tr>
            <tr>
                <td height="20"></td>
            </tr>
            <tr>
                <td><s:textarea cssClass="inputcontent" key="page.news.content" name="newsDTO.news.fullText"/></td>
            </tr>
            <s:iterator value="newsDTOList">
                <s:property value="news.title"/>
            </s:iterator>
            <tr>
                <td colspan="2" align="center">
                    <s:iterator value="authors" var="author">
                        <s:set var="count" value="%{0}"/>
                        <s:iterator value="newsDTO.authors" var="checkAuthor">
                            <s:if test="#author.id == #checkAuthor.id">
                                <s:set var="count" value="1"/>
                            </s:if>
                        </s:iterator>
                        <s:if test="#count == 1">
                            <s:checkbox name="newsTO.authors" label="%{name}" fieldValue="%{id}" value="true"/>
                        </s:if>
                        <s:else>
                            <s:checkbox name="newsTO.authors" label="%{name}" fieldValue="%{id}" value="false"/>
                        </s:else>
                    </s:iterator>
                </td>
            </tr>
            <tr>
                <td height="20"></td>
            <tr>
                <td colspan="2" align="center">
                    <s:iterator value="tags" var="tag">
                        <s:set var="count" value="%{0}"/>
                        <s:iterator value="newsDTO.tags" var="checkTag">
                            <s:if test="#tag.id == #checkTag.id">
                                <s:set var="count" value="1"/>
                            </s:if>
                        </s:iterator>
                        <s:if test="#count == 1">
                            <s:checkbox name="newsTO.tags" label="%{#tag.name}" fieldValue="%{#tag.id}" value="true"/>
                        </s:if>
                        <s:else>
                            <s:checkbox name="newsTO.tags" label="%{#tag.name}" fieldValue="%{#tag.id}" value="false"/>
                        </s:else>
                    </s:iterator>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right">
                    <s:submit cssClass="secondButton" key="page.news.save" name="submit"/>
                </td>
                <td colspan="2" align="right">
                    <s:form action="CancelForm" theme="simple">
                        <s:submit cssClass="secondButton" key="page.news.cancel"/>
                    </s:form>
                </td>
            </tr>
        </table>
    </s:form>
</div>
</body>
