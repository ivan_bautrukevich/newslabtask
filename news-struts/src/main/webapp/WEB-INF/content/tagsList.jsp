<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 6/2/2016
  Time: 3:31 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="/struts-tags" prefix="s" %>

<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script type="text/javascript" src="<s:url value="/resources/js/appscript.js" />"></script>
    <title><s:text name="page.tags.title"/></title>
</head>
<body>
<div class="main">
    <div class="mainList">
        <div class="inline">
            <s:iterator value="tags" var="tag">

                <s:form id="updateTag%{id}" cssClass="inputForm" namespace="/tags" action="EditTag"
                        method="POST">
                    <s:textarea cssClass="inputObject" id="inputObject%{id}" key="page.tags.tag"
                                name="tag.name" value="%{#tag.name}" disabled="true"/>
                    <s:hidden name="tag.id" value="%{id}"/>
                </s:form>

                <s:div cssClass="hiddenDib" id="buttonSet%{#tag.id}" style="display:none" theme="simple">
                    <s:submit cssClass="hiddenButton" form="updateTag%{#tag.id}" key="page.tags.update" theme="simple"/>
                    <s:submit cssClass="hiddenButton" form="deleteTag%{#tag.id}" key="page.tags.delete" theme="simple"/>
                    <input class="cancelButton" type="button"
                           value="<s:text name="page.tags.cancel"/>">
                </s:div>

                <s:submit cssClass="showButton" data-id="%{#tag.id}" type="button" key="page.tags.edit" theme="simple"/>

                <s:form id="deleteTag%{id}" namespace="/tags" action="DeleteTag" method="POST">
                    <s:hidden name="id" value="%{id}"/>
                </s:form>
            </s:iterator>
        </div>

        <s:div cssClass="inline" theme="simple">
            <s:form id="newTag" cssClass="cancelButton" namespace="/tags" action="SaveTag" method="POST">
                <s:textarea cssClass="textareaForm" key="page.tags.addtag" name="tag.name"/>
            </s:form>
            <s:submit form="newTag" cssClass="submitButton" key="page.tags.save" theme="simple"/>
        </s:div>

    </div>
</div>
</body>

