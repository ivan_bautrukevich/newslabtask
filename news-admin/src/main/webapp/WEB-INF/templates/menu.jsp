<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 27.05.2016
  Time: 11:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="menuAdmin">
    <ul>
        <li><a href="<s:url value="/news-list"/>"><spring:message code="page.menu.newsList"/></a></li>
        <li><a href="<s:url value="/news/edit/new"/>"><spring:message code="page.menu.addNews"/></a></li>
        <li><a href="<s:url value="/authors/"/>"><spring:message code="page.menu.updateAuthors"/></a></li>
        <li><a href="<s:url value="/tags/"/>"><spring:message code="page.menu.updateTags"/></a></li>
    </ul>
</div>

