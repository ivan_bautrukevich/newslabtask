<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 5/24/2016
  Time: 10:47 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>


<div class="head">
    <div class="headerLogo">
        <a href="<c:url value='/news-list' />"><h1><spring:message code="page.header.header"/></h1></a>
    </div>
    <div class="headerName">
        <security:authentication var="user" property="principal"/>
        <security:authorize access="hasRole('ROLE_ADMIN')">
            <spring:message code="page.header.hello"/>${user}
        </security:authorize>
    </div>
    <div class="headerTable">
        <table align="right">
            <tr>
                <td>
                    <a href="?locale=en_US"><img
                            src="<c:url value="/resources/image/eng.png" />" border="0" alt="eng"></a>
                </td>
                <td>
                    <a href="?locale=ru_RU"><img
                            src="<c:url value="/resources/image/rus.png" />" border="0" alt="rus"></a>
                </td>
            </tr>
            <security:authorize access="hasRole('ROLE_ADMIN')">
                <tr>
                    <td colspan="2">
                        <form name='loginForm' action="<c:url value='/j_spring_security_logout' />" method='POST'>
                            <input class="secondButton" name="submit" type="submit"
                                   value="<spring:message code="page.header.logout"/>"/>
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        </form>
                    </td>
                </tr>
            </security:authorize>
        </table>
    </div>
</div>
