<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 27.05.2016
  Time: 16:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<head>
    <title><spring:message code="page.login.title"/></title>
</head>
<body>
<div class="login">
    <form class="loginForm" name='loginForm' action="<c:url value='j_spring_security_check' />" method='POST'>
        <table align="center">
            <tr>
                <td><spring:message code="page.login.username"/></td>
                <td><input type='text' name='username'></td>
            </tr>
            <tr>
                <td><spring:message code="page.login.password"/></td>
                <td><input type='password' name='password'/></td>
            </tr>
            <c:if test="${not empty error}">
                <tr>
                    <td colspan='2' align="center">
                        <p align="center"><spring:message code="page.login.error"/></p>
                    </td>
                </tr>
            </c:if>
            <tr>
                <td colspan='2' align="center">
                    <input class="firstButton" name="submit" type="submit"
                           value="<spring:message code="page.login.login"/>"/>
                </td>
            </tr>
        </table>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </form>
</div>
</body>


