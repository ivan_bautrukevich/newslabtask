<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 6/3/2016
  Time: 3:29 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/appscript.js" />"></script>
    <title><spring:message code="page.authors.title"/></title>
</head>
<body>
<div class="main">
    <div class="mainList">
        <c:forEach items="${authors}" var="author" varStatus="status">
            <div class="inline">

                <form:form cssClass="inputForm" id="updateAuthor${author.id}" action="update" method="post"
                           modelAttribute="newAuthor">
                    <spring:message code="page.authors.author"/>
                    <form:input cssClass="inputObject" id="inputObject${author.id}" path="name" value="${author.name}"
                                disabled="true"/>
                    <form:hidden path="id" value="${author.id}"/>
                </form:form>

                <div class="hiddenDib" id="buttonSet${author.id}" style="display:none">
                    <input form="updateAuthor${author.id}" type="submit"
                           value="<spring:message code="page.authors.update"/>">
                    <input form="deleteAuthor${author.id}" type="submit"
                           value="<spring:message code="page.authors.expire"/>">
                    <input class="cancelButton" type="button" value="<spring:message code="page.authors.cancel"/>">
                </div>

                <input class="showButton" data-id="${author.id}" type="button"
                       value="<spring:message code="page.authors.edit"/>">

                <form:form name="deleteAuthor"  id="deleteAuthor${author.id}" action="${author.id}" method="delete"/>
            </div>
        </c:forEach>

        <form:form method="put" modelAttribute="newAuthor">
            <spring:message code="page.authors.addauthor"/>
            <form:input path="name"/>
            <input type="submit" value="<spring:message code="page.authors.save"/>">
            <form:errors path="name"/>
        </form:form>
    </div>
</div>
</body>
