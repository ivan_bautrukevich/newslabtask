<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 6/2/2016
  Time: 3:31 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/appscript.js" />"></script>
    <title><spring:message code="page.tags.title"/></title>
</head>
<body>
<div class="main">
    <div class="mainList">
        <c:forEach items="${tags}" var="tag" varStatus="status">
            <div class="inline">

                <form:form cssClass="inputForm" id="updateTag${tag.id}" action="update" method="post"
                           modelAttribute="newTag">
                    <spring:message code="page.tags.tag"/>
                    <form:input cssClass="inputObject" id="inputObject${tag.id}" path="name" value="${tag.name}"
                                disabled="true"/>
                    <form:hidden path="id" value="${tag.id}"/>
                </form:form>

                <div class="hiddenDib" id="buttonSet${tag.id}" style="display:none">
                    <input form="updateTag${tag.id}" type="submit"
                           value="<spring:message code="page.tags.update"/>">
                    <input form="deleteTag${tag.id}" type="submit"
                           value="<spring:message code="page.tags.delete"/>">
                    <input class="cancelButton" type="button" value="<spring:message code="page.tags.cancel"/>">
                </div>

                <input class="showButton" data-id="${tag.id}" type="button"
                       value="<spring:message code="page.tags.edit"/>">

                <form:form name="deleteTag"  id="deleteTag${tag.id}" action="${tag.id}" method="delete"/>
            </div>
        </c:forEach>

        <form:form method="put" modelAttribute="newTag">
            <spring:message code="page.tags.addtag"/>
            <form:input path="name"/>
            <input type="submit" value="<spring:message code="page.tags.save"/>">
            <form:errors path="name"/>
        </form:form>
    </div>
</div>
</body>

