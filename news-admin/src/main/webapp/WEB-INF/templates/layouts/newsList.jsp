<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 5/24/2016
  Time: 11:14 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title><spring:message code="page.newsList.title"/></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/appscript.js" />"></script>
</head>
<body>
<div class="main">
    <div class="inline" align="center">
        <div class="inputForm">
            <form name="useFilters" method="get" action="news-list">
                <div class="dropdown-check-list">
                    <span class="anchor"><spring:message code="page.newsList.author"/></span>
                    <ul class="items">
                        <c:choose>
                            <c:when test="${empty searchAuthors}">
                                <c:forEach var="author" items="${authors}">
                                    <li><input type="checkbox" name="checkAuthor"
                                               value="${author.id}">${author.name}
                                    </li>
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                                <c:forEach var="author" items="${authors}">
                                    <c:choose>
                                        <c:when test="${ctg:contains(searchAuthors, author.id)}">
                                            <li><input type="checkbox" name="checkAuthor" value="${author.id}"
                                                       checked>${author.name}</li>
                                        </c:when>
                                        <c:otherwise>
                                            <input type="checkbox" name="checkAuthor"
                                                   value="${author.id}">${author.name}<BR>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>
                    </ul>
                </div>
                <div class="dropdown-check-list">
                    <span class="anchor"><spring:message code="page.newsList.tags"/></span>
                    <ul class="items">
                        <c:choose>
                            <c:when test="${empty searchTags}">
                                <c:forEach var="tag" items="${tags}">
                                    <input type="checkbox" name="checkTag" value="${tag.id}">${tag.name}<BR>
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                                <c:forEach var="tag" items="${tags}">
                                    <c:choose>
                                        <c:when test="${ctg:contains(searchTags, tag.id)}">
                                            <input type="checkbox" name="checkTag" value="${tag.id}"
                                                   checked>${tag.name}
                                            <BR>
                                        </c:when>
                                        <c:otherwise>
                                            <input type="checkbox" name="checkTag" value="${tag.id}">${tag.name}<BR>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>
                    </ul>
                </div>
                <input class="secondButton" type="submit" value="<spring:message code="page.newsList.filter"/>"/>
            </form>
        </div>
        <div class="inputForm">
            <form name="resetForm" method="GET" action="news-list">
                <input type="hidden" name="reset" value="true">
                <button class="secondButton" type="submit"><spring:message code="page.newsList.reset"/></button>
            </form>
        </div>
    </div>
    <c:choose>
        <c:when test="${news.size() != 0}">
            <form:form name="deleteListNews" method="delete">
                <div class="newsList">
                    <c:forEach var="oneNews" items="${news}">

                        <div class="newsTitle">
                            <b><a href="<spring:url value="/news/${oneNews.news.id}"/>">${oneNews.news.title}</a></b>
                            <small>(by
                                <c:forEach var="oneNewsAuthor" items="${oneNews.authors}">
                                    ${oneNewsAuthor.name},
                                </c:forEach>
                                <fmt:formatDate value="${oneNews.news.modificationDate}"/>)
                            </small>
                        </div>

                        <div class="newsShortText">${oneNews.news.shortText}</div>

                        <div class="newsFooter">
                            <c:forEach var="oneNewsTag" items="${oneNews.tags}">
                                <span class="newsTag">${oneNewsTag.name}</span>
                            </c:forEach>
                            <small><spring:message code="page.newsList.comments"/>(${oneNews.comments.size()})</small>
                            <a href="<spring:url value="/news/edit/${oneNews.news.id}"/>"/>
                                <spring:message code="page.newsList.edit"/></a>
                            <input type="checkbox" name="listOfIdForDelete" value="${oneNews.news.id}">
                        </div>

                    </c:forEach>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    <div class="alignRight">
                        <input class="secondButton" type="submit" value="<spring:message code="page.newsList.delete"/>">
                    </div>
                </div>


                <div class="newsPagination">
                    <table align="center">
                        <tr>
                            <c:forEach begin="1" end="${noOfPages}" var="i">
                                <c:choose>
                                    <c:when test="${currentPage eq i}">
                                        <td>
                                            <button class="pressed-button" type="button">${i}</button>
                                        </td>
                                    </c:when>
                                    <c:otherwise>
                                        <td>
                                            <a href="news-list?page=${i}">
                                                <button type="button">${i}</button>
                                            </a>
                                        </td>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </tr>
                    </table>
                </div>
            </form:form>
        </c:when>
        <c:otherwise>
            <spring:message code="page.newsList.emptynews"/>
        </c:otherwise>
    </c:choose>
</div>
</body>
</html>