<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 5/31/2016
  Time: 4:59 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<head>
    <title>${newsDTO.news.title}</title>
</head>
<body>
<div class="main">
    <div class="newsList">
        <table align="center">
            <tr>
                <td height="20"></td>
            </tr>
            <tr>
                <td colspan="2">
                    <h3>${newsDTO.news.title}</h3>
                </td>
            </tr>
            <tr class="authorDateSize">
                <td align="left">
                    <small>
                        (by
                        <c:forEach var="author" items="${newsDTO.authors}">
                            ${author.name},
                        </c:forEach>
                        <fmt:formatDate type="date" value="${newsDTO.news.creationDate}"/>
                        )
                    </small>
                </td>
                <td align="right">

                </td>
            </tr>
            <tr>
                <td height="20"></td>
            <tr>
                <td>${newsDTO.news.shortText}</td>
            </tr>
            <tr>
                <td height="20"></td>
            </tr>
            <tr>
                <td>${newsDTO.news.fullText}</td>
            </tr>

            <tr>
                <td height="20"></td>
            </tr>
            <tr>
                <td height="20"></td>
            </tr>
            <tr>
                <td colspan="2" align="right">
                    <c:forEach var="tag" items="${newsDTO.tags}">
                        ${tag.name}
                    </c:forEach>
                </td>
            </tr>
            <c:forEach var="comment" items="${newsDTO.comments}">
                <tr>
                    <td>
                        <div class="comment">
                            <form:form name="deleteComment" method="delete" action="${comment.id}/delete-comment">
                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                <input type="hidden" name="newsId" value="${newsDTO.news.id}">
                                <div class="commentInfo">
                                        ${comment.creationDate}
                                        ${comment.commentText}
                                </div>
                                <div class="submitX">
                                    <input type="submit" value="X">
                                </div>
                            </form:form>
                        </div>
                    </td>
                </tr>
            </c:forEach>
        </table>
        <div class="showComment">
            <form:form method="put" modelAttribute="comment" action="${newsDTO.news.id}/add-comment">
                <form:textarea cssClass="addComment" path="commentText"/>
                <form:hidden path="newsId" value="${newsDTO.news.id}"/>
                <form:errors path="commentText"/>
                <div class="postButton">
                    <input class="firstButton" type="submit" value="<spring:message code="page.news.save"/>">
                </div>
            </form:form>
        </div>
    </div>
    <div class="newsFooterButton">
        <div class="newsFooterPrevious">
            <a href="<spring:url value="/news/${newsDTO.news.id}?next=false"/>">PREVIOUS</a>
        </div>
        <div class="newsFooterNext">
            <a href="<spring:url value="/news/${newsDTO.news.id}?next=true"/>">NEXT</a>
        </div>
    </div>
</div>
</body>
