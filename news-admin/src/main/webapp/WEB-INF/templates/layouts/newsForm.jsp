<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 5/31/2016
  Time: 3:28 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/appscript.js" />"></script>
    <title><spring:message code="page.news.add"/></title>
</head>
<body>
<div class="main">
    <div class="newsList">
        <c:if test="${not empty newsDTO.news.id}">
            <c:set var="action" scope="request" value="${newsDTO.news.id}"/>
            <c:set var="method" scope="request" value="post"/>
        </c:if>
        <c:if test="${empty newsDTO.news.id}">
            <c:set var="action" scope="request" value=""/>
            <c:set var="method" scope="request" value="put"/>
        </c:if>
        <c:if test="${not empty locked}">
            <spring:message code="page.news.locked"/>
        </c:if>
        <form:form method="${method}" modelAttribute="newsDTO" action="${action}">
            <table align="center">
                <tr>
                    <td height="20"></td>
                </tr>
                <tr>
                    <td><spring:message code="page.news.title"/></td>
                    <td><form:textarea cssClass="inputtitle" path="news.title"/></td>
                    <td><form:errors path="news.title"/></td>
                </tr>
                <tr>
                    <td height="20"></td>
                </tr>
                <tr>
                    <td><spring:message code="page.news.brief"/></td>
                    <td><form:textarea cssClass="inputbrief" path="news.shortText"/></td>
                    <td><form:errors path="news.shortText"/></td>
                </tr>
                <tr>
                    <td height="20"></td>
                </tr>
                <tr>
                    <td><spring:message code="page.news.content"/></td>
                    <td><form:textarea cssClass="inputcontent" path="news.fullText"/></td>
                    <td><form:errors path="news.fullText"/></td>
                </tr>
                <tr>
                    <td height="20"></td>
                <tr>
                    <td colspan="2" align="center">
                        <form:checkboxes cssClass="chosen-select" path="authors" itemValue="id" itemLabel="name"
                                         items="${authors}"/>
                    </td>
                    <td><form:errors path="authors"/></td>
                </tr>
                <tr>
                    <td height="20"></td>
                <tr>
                    <td colspan="2" align="center">
                        <form:checkboxes cssClass="chosen-select" path="tags" itemValue="id" itemLabel="name"
                                         items="${tags}"/>
                    </td>
                    <td><form:errors path="tags"/></td>
                </tr>

                    <%--<div class="dropdown-check-list">--%>
                    <%--<span class="anchor"><spring:message code="page.newsList.tags"/></span>--%>
                    <%--<ul class="items">--%>
                    <%--<c:forEach var="tag" items="${tags}" varStatus="status">--%>
                    <%--<li><form:checkbox path="tags" value="${tag.id}"/>${tag.name}</li>--%>
                    <%--</c:forEach>--%>
                    <%--</ul>--%>
                    <%--</div>--%>
                    <%--<div class="dropdown-check-list">--%>
                    <%--<span class="anchor"><spring:message code="page.newsList.author"/></span>--%>
                    <%--<ul class="items">--%>
                    <%--<c:forEach var="author" items="${authors}" varStatus="status">--%>
                    <%--<li><form:checkbox path="authors" value="${author.id}"/>${author.name}</li>--%>
                    <%--</c:forEach>--%>
                    <%--</ul>--%>
                    <%--</div>--%>
                <tr>
                    <td colspan="2" align="right">
                        <input class="firstButton" type="submit" value="<spring:message code="page.news.save"/>">
                    </td>
                </tr>
            </table>
            <form:hidden path="news.id"/>
            <form:hidden path="news.vers"/>
        </form:form>
    </div>
</div>
</body>
