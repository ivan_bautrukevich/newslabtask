<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 5/24/2016
  Time: 10:44 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<footer>
    <div class="copyright">
        <spring:message code="page.footer.footer"/>
    </div>
</footer>

