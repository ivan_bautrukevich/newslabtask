$(document).ready(function() {
    $(".showButton").click(function(){
        $(".hiddenDib").hide();
        $(".showButton").show();
        $(".inputObject").prop('disabled', true);
        $(this).hide();
        $("#buttonSet" + $(this).data("id")).show();
        $("#inputObject" + $(this).data("id")).prop('disabled', false);
    });

    $(".cancelButton").click(function(){
        $(".hiddenDib").hide();
        $(".showButton").show();
        $(".inputObject").prop('disabled', true);
    });

    $(function() {
        var checkList = $('.dropdown-check-list');
        checkList.on('click', 'span.anchor', function(event){
            var element = $(this).parent();

            if ( element.hasClass('visible') )
            {
                element.removeClass('visible');
            }
            else
            {
                element.addClass('visible');
            }
        });
    });
});