package by.bautruk.pandanews.admin.controller;

import by.bautruk.pandanews.common.entity.Tag;
import by.bautruk.pandanews.common.exception.ServiceException;
import by.bautruk.pandanews.common.service.TagService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         6/2/2016 3:23 PM.
 */
@Controller
@RequestMapping(value = "/tags/")
public class TagController {

    private final static Logger LOG = LogManager.getLogger(TagController.class);

    private static final String ATTRIBUTE_TAG = "newTag";
    private static final String ATTRIBUTE_TAGS = "tags";
    private static final String PARAMETER_ID = "id";
    private static final String VALIDATOR_CLASS = "tagValidator";
    private static final String MAP_URL_UPDATE = "/{id}";
    private static final String MAP_URL_DELETE = "/{id}";
    private static final String REDIRECT_URL = "redirect:/tags/";
    private static final String VIEW_PAGE = "tagsView";
    private static final String ATTRIBUTE_BINDING_RESULT = "org.springframework.validation.BindingResult.newTag";

    @Autowired
    private TagService tagService;

    @Autowired
    @Qualifier(VALIDATOR_CLASS)
    private Validator validator;

    // Set a form newsTOValidator
    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    @RequestMapping(method = RequestMethod.GET)
    public String showAllTags(Model model) throws ServiceException {
        List<Tag> tags = tagService.takeAll();
        model.addAttribute(ATTRIBUTE_TAGS, tags);
        if (!model.containsAttribute(ATTRIBUTE_TAG)) {
            model.addAttribute(ATTRIBUTE_TAG, new Tag());
        }
        return VIEW_PAGE;
    }

    @RequestMapping(value = MAP_URL_UPDATE, method = RequestMethod.POST)
    public ModelAndView updateTag(@ModelAttribute(ATTRIBUTE_TAG) Tag tag, BindingResult result,
                                  final RedirectAttributes redirectAttributes) throws ServiceException {
        validator.validate(tag, result);
        if (!result.hasErrors()) {
            tagService.updateById(tag.getId(), tag);
            return new ModelAndView(REDIRECT_URL);
        } else {
            redirectAttributes.addFlashAttribute(ATTRIBUTE_BINDING_RESULT, result);
            redirectAttributes.addFlashAttribute(ATTRIBUTE_TAG, tag);
            return new ModelAndView(REDIRECT_URL);
        }
    }

    @RequestMapping(method = {RequestMethod.PUT})
    public ModelAndView addTag(@ModelAttribute(ATTRIBUTE_TAG) Tag tag, BindingResult result,
                               final RedirectAttributes redirectAttributes) throws ServiceException {
        validator.validate(tag, result);
        if (!result.hasErrors()) {
            tagService.insert(tag);
            return new ModelAndView(REDIRECT_URL);
        } else {
            redirectAttributes.addFlashAttribute(ATTRIBUTE_BINDING_RESULT, result);
            redirectAttributes.addFlashAttribute(ATTRIBUTE_TAG, tag);
            return new ModelAndView(REDIRECT_URL);
        }
    }

    @RequestMapping(value = MAP_URL_DELETE, method = RequestMethod.DELETE)
    public ModelAndView deleteTag(@PathVariable(PARAMETER_ID) Long id) throws ServiceException {
        tagService.deleteLinksWithNewsByTagId(id);
        tagService.deleteById(id);
        return new ModelAndView(REDIRECT_URL);
    }
}
