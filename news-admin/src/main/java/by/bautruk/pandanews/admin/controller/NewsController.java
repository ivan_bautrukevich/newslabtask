package by.bautruk.pandanews.admin.controller;

import by.bautruk.pandanews.common.entity.*;
import by.bautruk.pandanews.common.exception.ServiceException;
import by.bautruk.pandanews.common.service.AuthorService;
import by.bautruk.pandanews.common.service.NewsDTOService;
import by.bautruk.pandanews.common.service.TagService;
import by.bautruk.pandanews.common.transferobject.NewsDTO;
import by.bautruk.pandanews.common.transferobject.NewsTO;
import by.bautruk.pandanews.common.transferobject.SearchCriteria;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * @author ivan.bautruvkevich
 *         5/31/2016 3:23 PM.
 */
@Controller
@RequestMapping(value = "/news")
public class NewsController {

    private static final Logger LOG = LogManager.getLogger(NewsController.class);

    private static final String ATTRIBUTE_NEWSDTO = "newsDTO";
    private static final String ATTRIBUTE_SEARCH_CRITERIA = "searchCriteria";
    private static final String ATTRIBUTE_COMMENT = "comment";
    private static final String ATTRIBUTE_AUTHORS = "authors";
    private static final String ATTRIBUTE_TAGS = "tags";
    private static final String COMMENT_VALIDATOR_CLASS = "commentValidator";
    private static final String MAP_URL_SHOW = "/{id}";
    private static final String MAP_URL_ADD = "/edit/new";
    private static final String MAP_URL_EDIT = "edit/{id}";
    private static final String NEWS_VALIDATOR_CLASS = "newsTOValidator";
    private static final String FORM_PAGE = "newsForm";
    private static final String VIEW_PAGE = "newsView";
    private static final String REDIRECT_URL = "redirect:/news/";
    private static final String REDIRECT_URL_EDIT = "redirect:/news/edit/";
    private static final String REDIRECT_URL_NEW = "redirect:/news/edit/new";
    private static final String ATTRIBUTE_BINDING_RESULT = "org.springframework.validation.BindingResult.newsDTO";
    private static final String ATTRIBUTE_MESSAGE = "locked";

    @Autowired
    @Qualifier(NEWS_VALIDATOR_CLASS)
    private Validator newsTOValidator;

    //Set a form validator
    @InitBinder(ATTRIBUTE_NEWSDTO)
    protected void initNewsDTOBinder(WebDataBinder binder) {
        binder.setValidator(newsTOValidator);
    }

    @Autowired
    @Qualifier(COMMENT_VALIDATOR_CLASS)
    private Validator validator;

    //Set a form validator
    @InitBinder(ATTRIBUTE_COMMENT)
    private void initCommentBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    @Autowired
    private AuthorService authorService;

    @Autowired
    private TagService tagService;

    @Autowired
    private NewsDTOService newsDTOService;

    @RequestMapping(value = MAP_URL_ADD, method = RequestMethod.GET)
    public String showFormForAddingNews(Model model) throws ServiceException {
        if (!model.containsAttribute(ATTRIBUTE_NEWSDTO)) {
            News news = new News();
            NewsDTO newsDTO = new NewsDTO();
            newsDTO.setNews(news);
            model.addAttribute(ATTRIBUTE_NEWSDTO, newsDTO);
        }
        model.addAttribute(ATTRIBUTE_AUTHORS, authorService.takeAll());
        model.addAttribute(ATTRIBUTE_TAGS, tagService.takeAll());
        return FORM_PAGE;
    }

    @RequestMapping(value = {MAP_URL_ADD}, method = RequestMethod.PUT)
    public ModelAndView addNews(@ModelAttribute(ATTRIBUTE_NEWSDTO) NewsTO newsTO,
                                BindingResult result,
                                final RedirectAttributes redirectAttributes) throws ServiceException {
        newsTOValidator.validate(newsTO, result);
        if (!result.hasErrors()) {
            if (newsTO.getNews().getId() == null) {
                Long id = newsDTOService.addNewsTO(newsTO);
                return new ModelAndView(REDIRECT_URL + id);
            } else {
                newsDTOService.updateNewsTO(newsTO);
                return new ModelAndView(REDIRECT_URL + newsTO.getNews().getId());
            }
        } else {
            redirectAttributes.addFlashAttribute(ATTRIBUTE_BINDING_RESULT, result);
            redirectAttributes.addFlashAttribute(ATTRIBUTE_NEWSDTO, newsTO);
            return new ModelAndView(REDIRECT_URL_NEW);
        }
    }

    @RequestMapping(value = MAP_URL_EDIT, method = RequestMethod.GET)
    public String editNews(@PathVariable Long id, Model model) throws ServiceException {
        if (!model.containsAttribute(ATTRIBUTE_NEWSDTO)) {
            model.addAttribute(ATTRIBUTE_NEWSDTO, newsDTOService.takeNews(id));
        }
        LOG.debug(model.containsAttribute(ATTRIBUTE_NEWSDTO));
        model.addAttribute(ATTRIBUTE_AUTHORS, authorService.takeAll());
        model.addAttribute(ATTRIBUTE_TAGS, tagService.takeAll());
        return FORM_PAGE;
    }

    @RequestMapping(value = {MAP_URL_EDIT}, method = RequestMethod.POST)
    public ModelAndView updateNews(@ModelAttribute(ATTRIBUTE_NEWSDTO) NewsTO newsTO,
                                   BindingResult result,
                                   final RedirectAttributes redirectAttributes) throws ServiceException {
        newsTOValidator.validate(newsTO, result);
        if (!result.hasErrors()) {
            try {
                newsDTOService.updateNewsTO(newsTO);
                return new ModelAndView(REDIRECT_URL + newsTO.getNews().getId());
            } catch (StaleObjectStateException e) {
                redirectAttributes.addFlashAttribute(ATTRIBUTE_MESSAGE, result);
                redirectAttributes.addFlashAttribute(ATTRIBUTE_NEWSDTO, newsTO);
                return new ModelAndView(REDIRECT_URL_EDIT + newsTO.getNews().getId());
            }
        } else {
            redirectAttributes.addFlashAttribute(ATTRIBUTE_BINDING_RESULT, true);
            redirectAttributes.addFlashAttribute(ATTRIBUTE_NEWSDTO, newsTO);
            return new ModelAndView(REDIRECT_URL_EDIT + newsTO.getNews().getId());
        }
    }

    @RequestMapping(value = MAP_URL_SHOW, method = RequestMethod.GET)
    public String showNews(@PathVariable Long id, Model model) throws ServiceException {
        NewsDTO checkedNewsDTO = newsDTOService.takeNews(id);
        model.addAttribute(ATTRIBUTE_NEWSDTO, checkedNewsDTO);
        if (!model.containsAttribute(ATTRIBUTE_COMMENT)) {
            model.addAttribute(ATTRIBUTE_COMMENT, new Comment());
        }
        return VIEW_PAGE;
    }

    @RequestMapping(value = MAP_URL_SHOW, params = {"next"}, method = RequestMethod.GET)
    public ModelAndView showNews(@PathVariable Long id, @RequestParam Boolean next,
                                 HttpServletRequest request) throws ServiceException {
        ModelAndView modelAndView = new ModelAndView();
        SearchCriteria searchCriteria = (SearchCriteria) request.getSession().getAttribute(ATTRIBUTE_SEARCH_CRITERIA);
        if (searchCriteria == null) {
            searchCriteria = new SearchCriteria();
            searchCriteria.setAuthors(new ArrayList<>());
            searchCriteria.setTags(new ArrayList<>());
        }
        NewsDTO checkedNewsDTO = newsDTOService.findNexNewsByNewsId(searchCriteria, id, next);
        if (checkedNewsDTO != null && checkedNewsDTO.getNews() != null) {
            Comment comment = new Comment();
            modelAndView.addObject(ATTRIBUTE_NEWSDTO, checkedNewsDTO);
            modelAndView.addObject(ATTRIBUTE_COMMENT, comment);
            modelAndView.setViewName(VIEW_PAGE);
            return modelAndView;
        } else {
            return new ModelAndView(REDIRECT_URL + id);
        }
    }
}
