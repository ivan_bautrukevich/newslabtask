package by.bautruk.pandanews.admin.utils.validator;

import by.bautruk.pandanews.common.entity.Tag;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * @author ivan.bautruvkevich
 *         6/2/2016 3:18 PM.
 */
@Component("tagValidator")
public class TagValidator implements Validator {

    private static final String FIELD_DAME = "name";
    private static final String ERROR_CODE_NAME_REQUIRED = "page.tags.name.required";

    @Override
    public boolean supports(Class<?> aClass) {
        return Tag.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, FIELD_DAME, ERROR_CODE_NAME_REQUIRED);
    }
}
