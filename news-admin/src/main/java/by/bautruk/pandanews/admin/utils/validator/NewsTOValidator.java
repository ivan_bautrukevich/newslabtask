package by.bautruk.pandanews.admin.utils.validator;

import by.bautruk.pandanews.common.transferobject.NewsDTO;
import by.bautruk.pandanews.common.transferobject.NewsTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * @author ivan.bautruvkevich
 *         6/7/2016 10:33 AM.
 */
@Component("newsTOValidator")
public class NewsTOValidator implements Validator {

    private final static Logger LOG = LogManager.getLogger(NewsTOValidator.class);

    private static final String FIELD_TITLE = "news.title";
    private static final String FIELD_SHORT_TEXT = "news.shortText";
    private static final String FIELD_FULL_TEXT = "news.fullText";
    private static final String FIELD_AUTHOR = "authors";
    private static final String FIELD_TAGS = "tags";
    private static final String ERROR_CODE_TITLE_REQUIRED = "page.newsForm.title.required";
    private static final String ERROR_CODE_SHORT_TEXT_REQUIRED = "page.newsForm.shortText.required";
    private static final String ERROR_CODE_FULL_TEXT_REQUIRED = "page.newsForm.fullText.required";
    private static final String ERROR_CODE_AUTHOR_REQUIRED = "page.newsForm.tags.required";
    private static final String ERROR_CODE_TAGS_REQUIRED = "page.tags.name.required";
    private static final String ERROR_CODE_TITLE_TOO_LONG = "page.newsForm.title.toolong";
    private static final String ERROR_CODE_SHORT_TEXT_TOO_LONG = "page.newsForm.shortText.toolong";
    private static final String ERROR_CODE_FULL_TEXT_TOO_LONG = "page.newsForm.fullText.toolong";

    @Override
    public boolean supports(Class<?> aClass) {
        return NewsTO.class.isAssignableFrom(aClass) || NewsDTO.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

        ValidationUtils.rejectIfEmpty(errors, FIELD_TITLE, ERROR_CODE_TITLE_REQUIRED);
        ValidationUtils.rejectIfEmpty(errors, FIELD_SHORT_TEXT, ERROR_CODE_SHORT_TEXT_REQUIRED);
        ValidationUtils.rejectIfEmpty(errors, FIELD_FULL_TEXT, ERROR_CODE_FULL_TEXT_REQUIRED);
        ValidationUtils.rejectIfEmpty(errors, FIELD_AUTHOR, ERROR_CODE_AUTHOR_REQUIRED);
        ValidationUtils.rejectIfEmpty(errors, FIELD_TAGS, ERROR_CODE_TAGS_REQUIRED);

        NewsTO newsTO = (NewsTO) o;
        if (newsTO.getNews().getTitle().length() > 30) {
            errors.rejectValue(FIELD_TITLE, ERROR_CODE_TITLE_TOO_LONG);
        }
        if (newsTO.getNews().getShortText().length() > 100) {
            errors.rejectValue(FIELD_SHORT_TEXT, ERROR_CODE_SHORT_TEXT_TOO_LONG);
        }
        if (newsTO.getNews().getFullText().length() > 2000) {
            errors.rejectValue(FIELD_FULL_TEXT, ERROR_CODE_FULL_TEXT_TOO_LONG);
        }
    }
}
