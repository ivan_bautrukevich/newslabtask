package by.bautruk.pandanews.admin.controller;

import by.bautruk.pandanews.common.entity.Author;
import by.bautruk.pandanews.common.transferobject.NewsDTO;
import by.bautruk.pandanews.common.transferobject.SearchCriteria;
import by.bautruk.pandanews.common.entity.Tag;
import by.bautruk.pandanews.common.exception.ServiceException;
import by.bautruk.pandanews.common.service.AuthorService;
import by.bautruk.pandanews.common.service.NewsDTOService;
import by.bautruk.pandanews.common.service.NewsService;
import by.bautruk.pandanews.common.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         5/31/2016 2:55 PM.
 */
@Controller
@RequestMapping(value = "/news-list")
public class NewsListController {

    @Autowired
    private NewsService newsService;

    @Autowired
    private NewsDTOService newsDTOService;

    @Autowired
    private TagService tagService;

    @Autowired
    private AuthorService authorService;

    private static final String ATTRIBUTE_NEWS = "news";
    private static final String ATTRIBUTE_TAGS = "tags";
    private static final String ATTRIBUTE_AUTHORS = "authors";
    private static final String ATTRIBUTE_SEARCH_TAGS = "searchTags";
    private static final String ATTRIBUTE_SEARCH_AUTHORS = "searchAuthors";
    private static final String ATTRIBUTE_SEARCH_CRITERIA = "searchCriteria";
    private static final String ATTRIBUTE_NUMBER_OF_PAGE = "noOfPages";
    private static final String ATTRIBUTE_CURRENT_PAGE = "currentPage";
    private static final String PARAMETER_RESET = "reset";
    private static final String PARAMETER_CHECK_TAG = "checkTag";
    private static final String PARAMETER_CHECK_AUTHOR = "checkAuthor";
    private static final String PARAMETER_PAGE = "page";
    private static final String PARAMETER_LIST_OF_ID = "listOfIdForDelete";
    private static final String VIEW_PAGE = "newsList";
    private static final int PAGE_DEFAULT = 1;
    private static final int NEWS_PER_PAGE = 3;
    private static final String REDIRECT_URL = "redirect:/news-list";
    private static final String EQUALS_VALUE = "true";


    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView showListNews(HttpServletRequest request) throws ServiceException {
        ModelAndView modelAndView = new ModelAndView();
        HttpSession session = request.getSession();

        //Parse checkbox for Author
        String[] selectTagsString = request.getParameterValues(PARAMETER_CHECK_TAG);
        List<Tag> selectTags = new ArrayList<>();
        List<Long> selectTagsId = new ArrayList<>();
        if (selectTagsString != null) {
            selectTags = parseStringToTag(selectTagsString);
            selectTagsId = parseStringToLong(selectTagsString);
        }

        //Parse checkbox for Author
        String[] selectAuthorsString = request.getParameterValues(PARAMETER_CHECK_AUTHOR);
        List<Author> selectAuthors = new ArrayList<>();
        List<Long> selectAuthorsId = new ArrayList<>();
        if (selectAuthorsString != null) {
            selectAuthors = parseStringToAuthor(selectAuthorsString);
            selectAuthorsId = parseStringToLong(selectAuthorsString);
        }

        //Parse session for searchCriteria
        SearchCriteria searchCriteria = new SearchCriteria();
        if (selectTagsString == null && selectAuthorsString == null && session.getAttribute(ATTRIBUTE_SEARCH_CRITERIA) != null
                && !EQUALS_VALUE.equals(request.getParameter(PARAMETER_RESET))) {
            searchCriteria = (SearchCriteria) session.getAttribute(ATTRIBUTE_SEARCH_CRITERIA);
            selectTagsId = parseTagToLong(searchCriteria.getTags());
            selectAuthorsId = parseAuthorToLong(searchCriteria.getAuthors());
        } else {
            searchCriteria.setAuthors(selectAuthors);
            searchCriteria.setTags(selectTags);
        }

        //Pagination
        int page = PAGE_DEFAULT;
        int recordsPerPage = NEWS_PER_PAGE;
        if (request.getParameter(PARAMETER_PAGE) != null) {
            page = Integer.parseInt(request.getParameter(PARAMETER_PAGE));
        }
        int noOfRecords = newsService.countNews(searchCriteria);
        int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
        request.setAttribute(ATTRIBUTE_NUMBER_OF_PAGE, noOfPages);
        request.setAttribute(ATTRIBUTE_CURRENT_PAGE, page);
        Long firstOrder = Long.valueOf(recordsPerPage * page - recordsPerPage + 1);
        Long lastOrder = firstOrder + recordsPerPage - 1;

        //
        List<NewsDTO> news = newsDTOService.findNewsByOrder(searchCriteria, firstOrder, lastOrder);
        List<Tag> tags = tagService.takeAll();
        List<Author> authors = authorService.takeAll();

        //Request and session
        request.setAttribute(ATTRIBUTE_NEWS, news);
        request.setAttribute(ATTRIBUTE_TAGS, tags);
        request.setAttribute(ATTRIBUTE_AUTHORS, authors);
        request.setAttribute(ATTRIBUTE_SEARCH_AUTHORS, selectAuthorsId);
        request.setAttribute(ATTRIBUTE_SEARCH_TAGS, selectTagsId);
        session.setAttribute(ATTRIBUTE_SEARCH_CRITERIA, searchCriteria);
        modelAndView.setViewName(VIEW_PAGE);
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public ModelAndView deleteListNews(HttpServletRequest request) throws ServiceException {
        if (request.getParameterValues(PARAMETER_LIST_OF_ID) != null) {
            String[] stringListOfNewsId = request.getParameterValues(PARAMETER_LIST_OF_ID);
            List<Long> listOfNewsId = parseStringToLong(stringListOfNewsId);
            newsService.deleteByListOfId(listOfNewsId);
        }
        return new ModelAndView(REDIRECT_URL);
    }

    private List<Long> parseStringToLong(String[] selectString) {
        List<Long> list = new ArrayList<>();
        for (String select : selectString) {
            list.add(Long.valueOf(select));
        }
        return list;
    }

    private List<Tag> parseStringToTag(String[] selectTagsString) {
        List<Tag> tags = new ArrayList<>();
        for (String selectTag : selectTagsString) {
            Tag tag = new Tag();
            tag.setId(Long.valueOf(selectTag));
            tags.add(tag);
        }
        return tags;
    }

    private List<Long> parseTagToLong(List<Tag> tagList) {
        List<Long> lisdId = new ArrayList<>();
        for (Tag tag : tagList) {
            lisdId.add(tag.getId());
        }
        return lisdId;
    }

    private List<Author> parseStringToAuthor(String[] selectAuthorsString) {
        List<Author> authors = new ArrayList<>();
        for (String selectAuthor : selectAuthorsString) {
            Author author = new Author();
            author.setId(Long.valueOf(selectAuthor));
            authors.add(author);
        }
        return authors;
    }

    private List<Long> parseAuthorToLong(List<Author> authorList) {
        List<Long> lisdId = new ArrayList<>();
        for (Author author : authorList) {
            lisdId.add(author.getId());
        }
        return lisdId;
    }
}
