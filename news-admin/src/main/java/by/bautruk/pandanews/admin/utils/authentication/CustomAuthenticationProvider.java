package by.bautruk.pandanews.admin.utils.authentication;

import by.bautruk.pandanews.common.entity.User;
import by.bautruk.pandanews.common.exception.ServiceException;
import by.bautruk.pandanews.common.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         6/9/2016 12:23 PM.
 */
@Component
public class CustomAuthenticationProvider  implements AuthenticationProvider {

    public CustomAuthenticationProvider(){
        encoder = new Md5PasswordEncoder();
    }

    private final static Logger LOG = LogManager.getLogger(CustomAuthenticationProvider.class);

    @Autowired
    private UserService userService;

    private Md5PasswordEncoder encoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String passwordString = (String) authentication.getCredentials();
        String password = encoder.encodePassword(passwordString, null);
        User user;
        try {
            user = userService.defineUser(username, password);
        } catch (ServiceException e) {
            throw new BadCredentialsException("");
        }
        if (user.getId() == null){
            throw new BadCredentialsException("");
        }
        List<GrantedAuthority> grantedAuthors = new ArrayList<>();
        grantedAuthors.add(new SimpleGrantedAuthority(user.getRole().getName()));
        return new UsernamePasswordAuthenticationToken(user.getName(), password, grantedAuthors);
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }
}
