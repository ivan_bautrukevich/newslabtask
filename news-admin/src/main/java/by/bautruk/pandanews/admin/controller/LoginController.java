package by.bautruk.pandanews.admin.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


/**
 * @author ivan.bautruvkevich
 *         27.05.2016 12:07.
 */
@Controller
public class LoginController {

    private final static Logger LOG = LogManager.getLogger(LoginController.class);

    private static final String ATTRIBUTE_ERROR = "error";
    private static final String EQUALS_VALUE = "true";
    private static final String MAP_URL_DEFAULT = "/";
    private static final String MAP_URL_LOGIN = "/login";
    private static final String REDIRECT_URL = "redirect:/login";
    private static final String VIEW_PAGE = "loginPage";
    private static final String DEFAULT_FOR_ATTRIBUTE_ERROR = "";

    @RequestMapping(value = MAP_URL_DEFAULT, method = RequestMethod.GET)
    public String showAdminPage() {
        return REDIRECT_URL;
    }

    @RequestMapping(value = MAP_URL_LOGIN, method = RequestMethod.GET)
    public ModelAndView loginAdmin(@RequestParam(value = ATTRIBUTE_ERROR, defaultValue = DEFAULT_FOR_ATTRIBUTE_ERROR)
                                       String error) {
        ModelAndView modelAndView = new ModelAndView();
        if (error.equals(EQUALS_VALUE)) {
            modelAndView.addObject(ATTRIBUTE_ERROR, error);
        }
        modelAndView.setViewName(VIEW_PAGE);
        return modelAndView;
    }
}
