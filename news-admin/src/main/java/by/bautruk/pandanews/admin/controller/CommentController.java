package by.bautruk.pandanews.admin.controller;

import by.bautruk.pandanews.common.entity.Comment;
import by.bautruk.pandanews.common.exception.ServiceException;
import by.bautruk.pandanews.common.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @author ivan.bautruvkevich
 *         6/21/2016 2:18 PM.
 */
@Controller
@RequestMapping(value = "/news")
public class CommentController {

    private static final String PARAMETER_NEWS_ID = "newsId";
    private static final String PARAMETER_ID = "id";
    private static final String ATTRIBUTE_NEWSDTO = "newsDTO";
    private static final String ATTRIBUTE_COMMENT = "comment";
    private static final String MAP_URL_ADD_COMMENT = "/{newsId}/add-comment";
    private static final String MAP_URL_DELETE_COMMENT = "/{id}/delete-comment";
    private static final String REDIRECT_URL = "redirect:/news/";
    private static final String VALIDATOR_CLASS = "commentValidator";
    private static final String NEWS_VALIDATOR_CLASS = "newsTOValidator";
    private static final String ATTRIBUTE_BINDING_RESULT = "org.springframework.validation.BindingResult.comment";

    @Autowired
    @Qualifier(VALIDATOR_CLASS)
    private Validator validator;

    @InitBinder(ATTRIBUTE_COMMENT)
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    @Autowired
    @Qualifier(NEWS_VALIDATOR_CLASS)
    private Validator newsTOValidator;

    //Set a form validator
    @InitBinder(ATTRIBUTE_NEWSDTO)
    protected void initNewsDTOBinder(WebDataBinder binder) {
        binder.setValidator(newsTOValidator);
    }

    @Autowired
    private CommentService commentService;

    @RequestMapping(value = {MAP_URL_ADD_COMMENT}, method = RequestMethod.PUT)
    public ModelAndView addComment(@PathVariable Long newsId,
                                   @ModelAttribute(ATTRIBUTE_COMMENT) Comment comment,
                                   BindingResult result,
                                   final RedirectAttributes redirectAttributes) throws ServiceException {
        validator.validate(comment, result);
        if (!result.hasErrors()) {
            commentService.insert(comment);
            return new ModelAndView(REDIRECT_URL + newsId);
        } else {
            redirectAttributes.addFlashAttribute(ATTRIBUTE_BINDING_RESULT, result);
            redirectAttributes.addFlashAttribute(ATTRIBUTE_COMMENT, comment);
            return new ModelAndView(REDIRECT_URL + newsId);
        }
    }

    @RequestMapping(value = MAP_URL_DELETE_COMMENT, method = RequestMethod.DELETE)
    public ModelAndView deleteComment(@PathVariable(PARAMETER_ID) Long id,
                                      HttpServletRequest request) throws ServiceException {
        String newsId = request.getParameter(PARAMETER_NEWS_ID);
        commentService.deleteById(id);
        return new ModelAndView(REDIRECT_URL + newsId);
    }
}
