package by.bautruk.pandanews.admin.utils.validator;

import by.bautruk.pandanews.common.entity.Author;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * @author ivan.bautruvkevich
 *         6/14/2016 5:55 PM.
 */
@Component("authorValidator")
public class AuthorValidator implements Validator {

    private static final String FIELD_NAME = "name";
    private static final String ERROR_CODE_NAME_REQUIRED = "page.authors.name.required";
    
    @Override
    public boolean supports(Class<?> aClass) {
        return Author.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        //// TODO: 6/24/2016 more condition for validator
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, FIELD_NAME, ERROR_CODE_NAME_REQUIRED);
    }
}
