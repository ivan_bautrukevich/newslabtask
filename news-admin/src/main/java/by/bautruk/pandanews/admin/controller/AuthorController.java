package by.bautruk.pandanews.admin.controller;

import by.bautruk.pandanews.common.entity.Author;
import by.bautruk.pandanews.common.exception.ServiceException;
import by.bautruk.pandanews.common.service.AuthorService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         6/3/2016 3:56 PM.
 */
@Controller
@RequestMapping(value = "/authors/")
public class AuthorController {

    private final static Logger LOG = LogManager.getLogger(AuthorController.class);

    private static final String ATTRIBUTE_AUTHOR = "newAuthor";
    private static final String ATTRIBUTE_AUTHORS = "authors";
    private static final String PARAMETER_ID = "id";
    private static final String MAP_URL_ADD = "/";
    private static final String MAP_URL_UPDATE = "/{id}";
    private static final String MAP_URL_EXPIRE = "/{id}";
    private static final String REDIRECT_URL = "redirect:/authors/";
    private static final String VIEW_PAGE = "authorView";
    private static final String VALIDATOR_CLASS = "authorValidator";
    private static final String ATTRIBUTE_BINDING_RESULT = "org.springframework.validation.BindingResult.newAuthor";

    @Autowired
    private AuthorService authorService;

    @Autowired
    @Qualifier(VALIDATOR_CLASS)
    private Validator validator;

    // Set a form validator
    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    @RequestMapping(method = RequestMethod.GET)
    public String showAllAuthors(Model model) throws ServiceException {
        List<Author> authors = authorService.takeAllNotExpired();
        model.addAttribute(ATTRIBUTE_AUTHORS, authors);
        if (!model.containsAttribute(ATTRIBUTE_AUTHOR)) {
            model.addAttribute(ATTRIBUTE_AUTHOR, new Author());
        }
        return VIEW_PAGE;
    }


    @RequestMapping(value = MAP_URL_UPDATE, method = RequestMethod.POST)
    public ModelAndView updateTag(@ModelAttribute(ATTRIBUTE_AUTHOR) Author author, BindingResult result,
                                  final RedirectAttributes redirectAttributes) throws ServiceException {
        validator.validate(author, result);
        if (!result.hasErrors()) {
            authorService.updateById(author.getId(), author);
            return new ModelAndView(REDIRECT_URL);
        } else {
            redirectAttributes.addFlashAttribute(ATTRIBUTE_BINDING_RESULT, result);
            redirectAttributes.addFlashAttribute(ATTRIBUTE_AUTHOR, author);
            return new ModelAndView(REDIRECT_URL);
        }
    }

    @RequestMapping(value = MAP_URL_ADD, method = RequestMethod.PUT)
    public ModelAndView addTag(@ModelAttribute(ATTRIBUTE_AUTHOR) Author author, BindingResult result,
                               final RedirectAttributes redirectAttributes) throws ServiceException {
        validator.validate(author, result);
        if (!result.hasErrors()) {
            authorService.insert(author);
            return new ModelAndView(REDIRECT_URL);
        } else {
            redirectAttributes.addFlashAttribute(ATTRIBUTE_BINDING_RESULT, result);
            redirectAttributes.addFlashAttribute(ATTRIBUTE_AUTHOR, author);
            return new ModelAndView(REDIRECT_URL);
        }
    }

    @RequestMapping(value = MAP_URL_EXPIRE, method = RequestMethod.DELETE)
    public ModelAndView deleteTag(@PathVariable(PARAMETER_ID) Long id) throws ServiceException {
        authorService.makeAuthorExpired(id);
        return new ModelAndView(REDIRECT_URL);
    }
}
