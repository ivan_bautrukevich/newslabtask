package by.bautruk.pandanews.admin.utils.validator;

import by.bautruk.pandanews.common.entity.Comment;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * @author ivan.bautruvkevich
 *         6/21/2016 2:21 PM.
 */
@Component("commentValidator")
public class CommentValidator implements Validator {

    private static final String FIELD_TEXT = "commentText";
    private static final String ERROR_CODE_TEXT_REQUIRED = "page.news.text.required";

    @Override
    public boolean supports(Class<?> aClass) {
        return Comment.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        //// TODO: 6/24/2016 more condition for validator
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, FIELD_TEXT, ERROR_CODE_TEXT_REQUIRED);
    }
}
