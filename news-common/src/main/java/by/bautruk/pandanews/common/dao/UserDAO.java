package by.bautruk.pandanews.common.dao;

import by.bautruk.pandanews.common.entity.User;
import by.bautruk.pandanews.common.exception.DAOException;

/**
 * @author ivan.bautruvkevich
 *         4/12/2016 7:51 PM.
 */
public interface UserDAO extends BaseDAO<User> {
    User defineUser(String username, String password) throws DAOException;
}
