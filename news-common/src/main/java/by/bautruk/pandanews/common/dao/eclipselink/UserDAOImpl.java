package by.bautruk.pandanews.common.dao.eclipselink;

import by.bautruk.pandanews.common.dao.UserDAO;
import by.bautruk.pandanews.common.entity.User;
import by.bautruk.pandanews.common.exception.DAOException;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         8/8/2016 9:14 AM.
 */
@Transactional
public class UserDAOImpl implements UserDAO{

    private static final String JPQL_FIND_ALL = "select u from User u";
    private static final String JPQL_FIND_BY_LOGIN_AND_PASSWORD = "select u from User u where u.login=:log and u.password =:pass";

    @PersistenceContext
    EntityManager entityManager;

    /**
     * Gives the list of all users from database.
     *
     * @return the list of users.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<User> takeAll() throws DAOException {
        Query query = entityManager.createQuery(JPQL_FIND_ALL);
        return (List<User>) query.getResultList();
    }

    /**
     * Represents the way of getting user as an instance of
     * {@code User} through {@code id} value from database.
     *
     * @param id is user's ID.
     * @return the only instance of {@code User} that defined just by using
     * {@code id}.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public User takeById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        return entityManager.find(User.class, id);
    }

    /**
     * Represents the way of adding user as an instance of
     * {@code User} from database.
     *
     * @param user is an instance of {@code User} class that
     *             has to be placed into table.
     * @return ID of inserting user
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Long insert(User user) throws DAOException {
        if (user == null) throw new DAOException("Invalid user");
        entityManager.persist(user);
        return user.getId();
    }

    /**
     * Represents the way of deleting user from database by ID.
     *
     * @param id is user's ID which will be delete.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        User user = entityManager.find(User.class, id);
        entityManager.remove(user);
        return true;
    }

    /**
     * Represents the way of updating user as an instance of
     * {@code User} in database.
     *
     * @param id   is user's ID which will be update.
     * @param user is an instance of {@code User} class that
     *             has to be updating into table.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean updateById(Long id, User user) throws DAOException {
        if (id == null || user == null) throw new DAOException("Invalid id or user");
        user.setId(id);
        entityManager.merge(user);
        return true;
    }

    @Override
    public User defineUser(String username, String password) throws DAOException {
        Query query = entityManager.createQuery(JPQL_FIND_BY_LOGIN_AND_PASSWORD);
        query.setParameter("log", username);
        query.setParameter("pass", password);
        return (User) query.getSingleResult();
    }
}
