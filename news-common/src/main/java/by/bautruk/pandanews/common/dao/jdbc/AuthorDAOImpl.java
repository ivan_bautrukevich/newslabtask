package by.bautruk.pandanews.common.dao.jdbc;

import by.bautruk.pandanews.common.entity.Author;
import by.bautruk.pandanews.common.dao.AuthorDAO;
import by.bautruk.pandanews.common.exception.DAOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         4/12/2016 7:14 PM.
 *         The {@code AuthorDAOImpl} class implements all the necessary methods
 *         which implements {@code AuthorDAO} interface for manipulation
 *         with author's data in database.
 */

public class AuthorDAOImpl implements AuthorDAO {

    private final static Logger LOG = LogManager.getLogger(AuthorDAOImpl.class);

    @Autowired
    private DataSource dataSource;

    private static final String COLUMN_ID = "AUTHOR_ID";
    private static final String COLUMN_NAME = "AUTHOR_NAME";
    private static final String COLUMN_EXPIRED = "EXPIRED";
    private static final String SQL_FIND_ALL = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHORS";
    private static final String SQL_FIND_ALL_NOT_EXPIRED = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHORS" +
            " WHERE EXPIRED IS NULL ";
    private static final String SQL_FIND_BY_ID = SQL_FIND_ALL + " WHERE AUTHORS.AUTHOR_ID = ?";
    private static final String SQL_INSERT = "INSERT INTO AUTHORS (AUTHOR_NAME) VALUES (?)";
    private static final String SQL_UPDATE = "UPDATE AUTHORS SET AUTHOR_NAME=?,EXPIRED=? WHERE AUTHOR_ID = ?";
    private static final String SQL_UPDATE_EXPIRED = "UPDATE AUTHORS SET EXPIRED=? WHERE AUTHOR_ID = ?";
    private static final String SQL_DELETE = "DELETE FROM AUTHORS WHERE AUTHOR_ID= ?";
    private static final String SQL_CREATE_LINK_NEWS = "INSERT INTO NEWS_AUTHORS (NEWS_ID, AUTHOR_ID) VALUES (?, ?)";
    private static final String SQL_DELETE_LINK_NEWS = "DELETE FROM NEWS_AUTHORS WHERE NEWS_ID=?";
    private static final String SQL_FIND_BY_NEWS_ID = "SELECT  AUTHORS.AUTHOR_ID, AUTHORS.AUTHOR_NAME, " +
            "AUTHORS.EXPIRED FROM AUTHORS JOIN NEWS_AUTHORS ON AUTHORS.AUTHOR_ID = NEWS_AUTHORS.AUTHOR_ID" +
            " WHERE NEWS_AUTHORS.NEWS_ID=?";

    /**
     * Gives the list of all authors from database.
     *
     * @return the list of authors.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<Author> takeAll() throws DAOException {
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_FIND_ALL)) {
            ResultSet rs = ps.executeQuery();
            return buildAuthorList(rs);
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during all authors taking", e);
        }
    }

    /**
     * Represents the way of getting author as an instance of
     * {@code Author} through {@code id} value from database.
     *
     * @param id is author's ID.
     * @return the only instance of {@code Author} that defined just by using
     * {@code id}.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Author takeById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_FIND_BY_ID)) {
            Author author = new Author();
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                author.setId(rs.getLong(COLUMN_ID));
                author.setName(rs.getString(COLUMN_NAME));
                author.setExpired(rs.getTimestamp(COLUMN_EXPIRED));
            }
            return author;
        } catch (Exception e) {
            throw new DAOException("Invalid database operation occurred during author taking", e);
        }
    }

    /**
     * Represents the way of adding author as an instance of
     * {@code Author} from database.
     *
     * @param author is an instance of {@code Author} class that
     *               has to be placed into table.
     * @return ID of inserting author
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Long insert(Author author) throws DAOException {
        if (author == null) throw new DAOException("Invalid author");
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_INSERT, new String[]{COLUMN_ID})) {
            LOG.debug(author);
            ps.setString(1, author.getName());
            ps.executeUpdate();
            Long id = null;
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getLong(1);
            }
            return id;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during author adding", e);
        }
    }

    /**
     * Represents the way of deleting author from database by ID.
     *
     * @param id is author's ID which will be delete.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_DELETE)) {
            ps.setLong(1, id);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during author deleting", e);
        }
    }

    /**
     * Represents the way of updating author as an instance of
     * {@code Author} in database.
     *
     * @param id     is author's ID which will be update.
     * @param author is an instance of {@code Author} class that
     *               has to be updating into table.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean updateById(Long id, Author author) throws DAOException {
        if (id == null || author == null) throw new DAOException("Invalid id or author");
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_UPDATE)) {
            ps.setString(1, author.getName());
            ps.setTimestamp(2, author.getExpired());
            ps.setLong(3, id);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during author updating", e);
        }
    }

    /**
     * Gives the list of all authors from database by News' ID.
     *
     * @param id is news' ID.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<Author> takeByNewsId(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_FIND_BY_NEWS_ID)) {
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            return buildAuthorList(rs);
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during all authors taking", e);
        }
    }

    /**
     * Represents the way of adding links from news to author in database.
     *
     * @param newsId   is news' ID.
     * @param authorId is author's ID.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean createLinkWithNews(Long newsId, List<Long> authorId) throws DAOException {
        if (newsId == null || authorId == null) throw new DAOException("Invalid newsId or authorId");
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_CREATE_LINK_NEWS)) {
            for (Long id : authorId) {
                ps.setLong(1, newsId);
                ps.setLong(2, id);
                ps.addBatch();
            }
            ps.executeBatch();
            return true;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during link adding", e);
        }
    }

    /**
     * Represents the way of deleting links from news to author in database.
     *
     * @param newsId is list of news' ID.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteLinksWithNewsById(List<Long> newsId) throws DAOException {
        if (newsId == null) throw new DAOException("Invalid newsId");
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_DELETE_LINK_NEWS)) {
            for (Long id : newsId) {
                ps.setLong(1, id);
                ps.addBatch();
            }
            ps.executeBatch();
            return true;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during link deleting", e);
        }
    }

    /**
     * Gives the list of all authors from database which don't have expired date.
     *
     * @return the list of authors.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<Author> takeAllNotExpired() throws DAOException {
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_FIND_ALL_NOT_EXPIRED)) {
            ResultSet rs = ps.executeQuery();
            return buildAuthorList(rs);
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during all authors taking", e);
        }
    }

    /**
     * Represents the way of updating author as an instance of
     * {@code Author} in database.
     *
     * @param id is author's ID which will be expired.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean updateAuthorExpired(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id or author");
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_UPDATE_EXPIRED)) {
            ps.setTimestamp(1, new Timestamp(new java.util.Date().getTime()));
            ps.setLong(2, id);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during author updating", e);
        }
    }

    private List<Author> buildAuthorList(ResultSet rs) throws SQLException {
        List<Author> result = new ArrayList<>();
        while (rs.next()) {
            Author author = new Author();
            author.setId(rs.getLong(COLUMN_ID));
            author.setName(rs.getString(COLUMN_NAME));
            author.setExpired(rs.getTimestamp(COLUMN_EXPIRED));
            result.add(author);
        }
        return result;
    }
}
