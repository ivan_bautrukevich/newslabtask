package by.bautruk.pandanews.common.service.impl;

import by.bautruk.pandanews.common.dao.AuthorDAO;
import by.bautruk.pandanews.common.entity.Author;
import by.bautruk.pandanews.common.exception.DAOException;
import by.bautruk.pandanews.common.exception.ServiceException;
import by.bautruk.pandanews.common.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         4/12/2016 8:10 PM.
 */
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = ServiceException.class)
public class AuthorServiceImpl implements AuthorService {

    @Autowired
    private AuthorDAO authorDAO;

    @Override
    public List<Author> takeAll() throws ServiceException {
        try {
            return authorDAO.takeAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Author takeById(Long id) throws ServiceException {
        try {
            return authorDAO.takeById(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Long insert(Author author) throws ServiceException {
        try {
            return authorDAO.insert(author);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean deleteById(Long id) throws ServiceException {
        try {
            return authorDAO.deleteById(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean updateById(Long newsId, Author author) throws ServiceException {
        try {
            return authorDAO.updateById(newsId, author);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Author> takeByNewsId(Long id) throws ServiceException {
        try {
            return authorDAO.takeByNewsId(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean createLinkWithNewsId(Long newsId, List<Long> authorId) throws ServiceException {
        try {
            return authorDAO.createLinkWithNews(newsId, authorId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }


    @Override
    public boolean deleteLinksWithNewsById(List<Long> newsId) throws ServiceException {
        try {
            return authorDAO.deleteLinksWithNewsById(newsId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Author> takeAllNotExpired() throws ServiceException {
        try {
            return authorDAO.takeAllNotExpired();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean makeAuthorExpired(Long id) throws ServiceException {
        try {
            return authorDAO.updateAuthorExpired(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
