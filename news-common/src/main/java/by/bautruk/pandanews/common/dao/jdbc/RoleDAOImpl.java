package by.bautruk.pandanews.common.dao.jdbc;

import by.bautruk.pandanews.common.dao.RoleDAO;
import by.bautruk.pandanews.common.entity.Role;
import by.bautruk.pandanews.common.exception.DAOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         11.04.2016 9:44.
 *         The {@code RoleDAOImpl} class implements all the necessary methods
 *         which implements {@code RoleDAO} interface for manipulation
 *         with role's data in database.
 */

public class RoleDAOImpl implements RoleDAO {

    private final static Logger LOG = LogManager.getLogger(RoleDAOImpl.class);

    @Autowired
    private DataSource dataSource;

    private static final String COLUMN_ID = "ROLE_ID";
    private static final String COLUMN_NAME = "ROLE_NAME";
    private static final String SQL_FIND_ALL = "SELECT ROLE_ID, ROLE_NAME FROM ROLES";
    private static final String SQL_FIND_BY_ID = SQL_FIND_ALL + " WHERE ROLE_ID= ?";
    private static final String SQL_INSERT = "INSERT INTO ROLES (ROLE_ID, ROLE_NAME) VALUES (?, ?)";
    private static final String SQL_UPDATE = "UPDATE ROLES SET ROLE_NAME = ? WHERE ROLE_ID = ?";
    private static final String SQL_DELETE = "DELETE FROM ROLES WHERE ROLE_ID = ?";

    /**
     * Gives the list of all roles from database.
     *
     * @return the list of roles.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<Role> takeAll() throws DAOException {
        List<Role> result = new ArrayList<>();
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_FIND_ALL)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Role role = new Role();
                role.setId(rs.getLong(COLUMN_ID));
                role.setName(rs.getString(COLUMN_NAME));
                result.add(role);
            }
            return result;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during all roles taking", e);
        }
    }

    /**
     * Represents the way of getting role as an instance of
     * {@code Role} through {@code id} value from database.
     *
     * @param id is role's ID.
     * @return the only instance of {@code Role} that defined just by using
     * {@code id}.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Role takeById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        Role role = null;
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_FIND_BY_ID)) {
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                role = new Role();
                role.setId(rs.getLong(COLUMN_ID));
                role.setName(rs.getString(COLUMN_NAME));
            }
            return role;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during role taking", e);
        }
    }

    /**
     * Represents the way of adding role as an instance of
     * {@code Role} from database.
     *
     * @param role is an instance of {@code Role} class that
     *             has to be placed into table.
     * @return ID of inserting role
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Long insert(Role role) throws DAOException {
        if (role == null) throw new DAOException("Invalid role");
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_INSERT, new String[]{COLUMN_ID})) {
            ps.setLong(1, role.getId());
            ps.setString(2, role.getName());
            ps.executeUpdate();
            Long id = null;
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getLong(1);
            }
            return id;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during role adding", e);
        }
    }

    /**
     * Represents the way of deleting role from database by ID.
     *
     * @param id is role's ID which will be delete.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_DELETE)) {
            ps.setLong(1, id);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during role deleting", e);
        }
    }

    /**
     * Represents the way of updating role as an instance of
     * {@code Role} in database.
     *
     * @param id   is role's ID which will be update.
     * @param role is an instance of {@code Role} class that
     *             has to be updating into table.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean updateById(Long id, Role role) throws DAOException {
        if (id == null || role == null) throw new DAOException("Invalid id or role");
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_UPDATE)) {
            ps.setString(1, role.getName());
            ps.setLong(2, id);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during role updating", e);
        }
    }
}
