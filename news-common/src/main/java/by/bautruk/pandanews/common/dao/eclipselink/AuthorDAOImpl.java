package by.bautruk.pandanews.common.dao.eclipselink;

import by.bautruk.pandanews.common.dao.AuthorDAO;
import by.bautruk.pandanews.common.entity.Author;
import by.bautruk.pandanews.common.exception.DAOException;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.sql.Timestamp;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         8/8/2016 9:13 AM.
 *         The {@code AuthorDAOImpl} class implements all the necessary methods
 *         which implements {@code AuthorDAO} interface for manipulation
 *         with author's data in database.
 */
@Transactional
public class AuthorDAOImpl implements AuthorDAO {

    private static final String JPQL_FIND_ALL = "select a from Author a";

    @PersistenceContext
    EntityManager entityManager;

    /**
     * Gives the list of all authors from database.
     *
     * @return the list of authors.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<Author> takeAll() throws DAOException {
        Query query = entityManager.createQuery(JPQL_FIND_ALL);
        return (List<Author>) query.getResultList();
    }

    /**
     * Represents the way of getting author as an instance of
     * {@code Author} through {@code id} value from database.
     *
     * @param id is author's ID.
     * @return the only instance of {@code Author} that defined just by using
     * {@code id}.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Author takeById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        return entityManager.find(Author.class, id);
    }

    /**
     * Represents the way of adding author as an instance of
     * {@code Author} from database.
     *
     * @param author is an instance of {@code Author} class that
     *               has to be placed into table.
     * @return ID of inserting author
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Long insert(Author author) throws DAOException {
        if (author == null) throw new DAOException("Invalid author");
        entityManager.persist(author);
        return author.getId();
    }

    /**
     * Represents the way of deleting author from database by ID.
     *
     * @param id is author's ID which will be delete.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        Author author = entityManager.find(Author.class, id);
        entityManager.remove(author);
        return true;
    }

    /**
     * Represents the way of updating author as an instance of
     * {@code Author} in database.
     *
     * @param id     is author's ID which will be update.
     * @param author is an instance of {@code Author} class that
     *               has to be updating into table.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean updateById(Long id, Author author) throws DAOException {
        if (id == null || author == null) throw new DAOException("Invalid id or author");
        author.setId(id);
        entityManager.merge(author);
        return true;
    }

    /**
     * Gives the list of all authors from database by News' ID.
     *
     * @param id is news' ID.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<Author> takeByNewsId(Long id) throws DAOException {
        return null;
    }

    /**
     * Represents the way of adding links from news to author in database.
     *
     * @param newsId   is news' ID.
     * @param authorId is author's ID.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean createLinkWithNews(Long newsId, List<Long> authorId) throws DAOException {
        return false;
    }

    /**
     * Represents the way of deleting links from news to author in database.
     *
     * @param newsId is list of news' ID.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteLinksWithNewsById(List<Long> newsId) throws DAOException {
        return false;
    }

    /**
     * Gives the list of all authors from database which don't have expired date.
     *
     * @return the list of authors.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<Author> takeAllNotExpired() throws DAOException {
        return null;
    }

    /**
     * Represents the way of updating author as an instance of
     * {@code Author} in database.
     *
     * @param id is author's ID which will be expired.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean updateAuthorExpired(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        Author author = entityManager.find(Author.class, id);
        author.setExpired(new Timestamp(new java.util.Date().getTime()));
        entityManager.merge(author);
        return true;
    }
}
