package by.bautruk.pandanews.common.service;

import by.bautruk.pandanews.common.entity.News;
import by.bautruk.pandanews.common.transferobject.SearchCriteria;
import by.bautruk.pandanews.common.exception.ServiceException;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         4/12/2016 8:08 PM.
 */
public interface NewsService extends BaseService<News> {

    List<News> searchNews(SearchCriteria searchCriteria) throws ServiceException;

    Integer countNews(SearchCriteria searchCriteria) throws ServiceException;

    List<News> findListIdOfNews(SearchCriteria searchCriteria, Long firstOrder, Long lastOrder) throws ServiceException;

    News findNexNewsByNewsId(SearchCriteria searchCriteria, Long newsId, boolean next) throws ServiceException;

    boolean deleteByListOfId(List<Long> listOfNewsId) throws ServiceException;
}
