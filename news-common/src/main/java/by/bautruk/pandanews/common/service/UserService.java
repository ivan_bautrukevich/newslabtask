package by.bautruk.pandanews.common.service;

import by.bautruk.pandanews.common.entity.User;
import by.bautruk.pandanews.common.exception.ServiceException;

/**
 * @author ivan.bautruvkevich
 *         4/12/2016 8:09 PM.
 */
public interface UserService extends BaseService<User> {
    User defineUser(String username, String password) throws ServiceException;
}
