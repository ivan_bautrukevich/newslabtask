package by.bautruk.pandanews.common.service;

import by.bautruk.pandanews.common.entity.Author;
import by.bautruk.pandanews.common.exception.ServiceException;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         4/12/2016 7:59 PM.
 */
public interface AuthorService extends BaseService<Author> {
    List<Author> takeByNewsId(Long id) throws ServiceException;

    boolean createLinkWithNewsId(Long newsId, List<Long> authorId) throws ServiceException;

    boolean deleteLinksWithNewsById(List<Long> newsId) throws ServiceException;

    List<Author> takeAllNotExpired() throws ServiceException;

    boolean makeAuthorExpired(Long id) throws ServiceException;
}
