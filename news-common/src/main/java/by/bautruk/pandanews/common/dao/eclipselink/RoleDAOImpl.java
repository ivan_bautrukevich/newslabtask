package by.bautruk.pandanews.common.dao.eclipselink;

import by.bautruk.pandanews.common.dao.RoleDAO;
import by.bautruk.pandanews.common.entity.Role;
import by.bautruk.pandanews.common.exception.DAOException;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         8/8/2016 9:13 AM.
 *         The {@code RoleDAOImpl} class implements all the necessary methods
 *         which implements {@code RoleDAO} interface for manipulation
 *         with role's data in database.
 */
@Transactional
public class RoleDAOImpl implements RoleDAO {

    private static final String JPQL_FIND_ALL = "select r from Role r";

    @PersistenceContext
    EntityManager entityManager;

    /**
     * Gives the list of all roles from database.
     *
     * @return the list of roles.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<Role> takeAll() throws DAOException {
        Query query = entityManager.createQuery(JPQL_FIND_ALL);
        return (List<Role>) query.getResultList();
    }

    /**
     * Represents the way of getting role as an instance of
     * {@code Role} through {@code id} value from database.
     *
     * @param id is role's ID.
     * @return the only instance of {@code Role} that defined just by using
     * {@code id}.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Role takeById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        return entityManager.find(Role.class, id);
    }

    /**
     * Represents the way of adding role as an instance of
     * {@code Role} from database.
     *
     * @param role is an instance of {@code Role} class that
     *             has to be placed into table.
     * @return ID of inserting role
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Long insert(Role role) throws DAOException {
        if (role == null) throw new DAOException("Invalid role");
        entityManager.persist(role);
        return role.getId();
    }

    /**
     * Represents the way of deleting role from database by ID.
     *
     * @param id is role's ID which will be delete.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        Role role = entityManager.find(Role.class, id);
        entityManager.remove(role);
        return true;
    }

    /**
     * Represents the way of updating role as an instance of
     * {@code Role} in database.
     *
     * @param id   is role's ID which will be update.
     * @param role is an instance of {@code Role} class that
     *             has to be updating into table.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean updateById(Long id, Role role) throws DAOException {
        if (id == null || role == null) throw new DAOException("Invalid id or role");
        role.setId(id);
        entityManager.merge(role);
        return true;
    }
}
