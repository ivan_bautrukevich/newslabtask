package by.bautruk.pandanews.common.dao.hibernate;

import by.bautruk.pandanews.common.dao.UserDAO;
import by.bautruk.pandanews.common.entity.User;
import by.bautruk.pandanews.common.exception.DAOException;
import org.hibernate.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         7/13/2016 4:51 PM.
 *         The {@code UserDAOImpl} class implements all the necessary methods
 *         which implements {@code UserDAO} interface for manipulation
 *         with role's data in database.
 */
//@Transactional
public class UserDAOImpl implements UserDAO {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Gives the list of all users from database.
     *
     * @return the list of users.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<User> takeAll() throws DAOException {
        try {
            List<User> users = sessionFactory.getCurrentSession()
                    .createCriteria(User.class)
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
            return users;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during all users taking", e);
        }
    }

    /**
     * Represents the way of getting user as an instance of
     * {@code User} through {@code id} value from database.
     *
     * @param id is user's ID.
     * @return the only instance of {@code User} that defined just by using
     * {@code id}.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public User takeById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        try {
            User user = (User) sessionFactory.getCurrentSession().get(User.class, id);
            return user;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during user taking", e);
        }
    }

    /**
     * Represents the way of adding user as an instance of
     * {@code User} from database.
     *
     * @param user is an instance of {@code User} class that
     *             has to be placed into table.
     * @return ID of inserting user
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Long insert(User user) throws DAOException {
        if (user == null) throw new DAOException("Invalid user");
        try {
            return (Long) sessionFactory.getCurrentSession().save(user);
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during user adding", e);
        }
    }

    /**
     * Represents the way of deleting user from database by ID.
     *
     * @param id is user's ID which will be delete.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        try {
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery("delete from User where id =:userId");
            query.setParameter("userId", id);
            query.executeUpdate();
            return true;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during user deleting", e);
        }
    }

    /**
     * Represents the way of updating user as an instance of
     * {@code User} in database.
     *
     * @param id   is user's ID which will be update.
     * @param user is an instance of {@code User} class that
     *             has to be updating into table.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean updateById(Long id, User user) throws DAOException {
        if (id == null || user == null) throw new DAOException("Invalid id or user");
        try {
            user.setId(id);
            sessionFactory.getCurrentSession().update(user);
            return true;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during user updating", e);
        }
    }

    @Override
    public User defineUser(String username, String password) throws DAOException {
        if (username == null || password == null) throw new DAOException("Invalid login or password");
        try {
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery("from User where login =:login and password=:password");
            query.setParameter("login", username);
            query.setParameter("password", password);
            User user = (User) query.uniqueResult();
            return user;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during user taking", e);
        }
    }
}
