package by.bautruk.pandanews.common.exception;

/**
 * @author ivan.bautruvkevich
 *         11.04.2016 9:21.
 */
public class ServiceException extends Exception {

    public ServiceException() {
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
