package by.bautruk.pandanews.common.exception;

/**
 * @author ivan.bautruvkevich
 *         11.04.2016 9:40.
 */
public class DAOException extends Exception {
    public DAOException() {
    }

    public DAOException(String message) {
        super(message);
    }

    public DAOException(Throwable cause) {
        super(cause);
    }

    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }
}
