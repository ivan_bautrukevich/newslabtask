package by.bautruk.pandanews.common.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * @author ivan.bautruvkevich
 *         11.04.2016 9:27.
 *         The {@code Tag} class is an entity-class which includes all
 *         the necessary getters and setters for its fields and also overrides
 *         {@code equals()}, {@code hashCode()} and {@code toString()}.
 */
@Entity
@Table(name = "TAGS")
@SequenceGenerator(name = "TAGS_SEQ", sequenceName = "TAGS_SEQ", initialValue = 1, allocationSize = 1)
public class Tag implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "TAG_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TAGS_SEQ")
    private Long id;

    @Column(name = "TAG_NAME")
    private String name;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "NEWS_TAGS",
            joinColumns = @JoinColumn(name = "TAG_ID"),
            inverseJoinColumns = @JoinColumn(name = "NEWS_ID"))
    private Set<News> newsSet;

    public Tag() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<News> getNewsSet() {
        return newsSet;
    }

    public void setNewsSet(Set<News> news) {
        this.newsSet = news;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tag)) return false;

        Tag tag = (Tag) o;

        if (id != null ? !id.equals(tag.id) : tag.id != null) return false;
        if (name != null ? !name.equals(tag.name) : tag.name != null) return false;
        return newsSet != null ? newsSet.equals(tag.newsSet) : tag.newsSet == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (newsSet != null ? newsSet.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Tag{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", news=" + newsSet +
                '}';
    }
}
