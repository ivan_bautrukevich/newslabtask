package by.bautruk.pandanews.common.service.impl;

import by.bautruk.pandanews.common.dao.UserDAO;
import by.bautruk.pandanews.common.entity.User;
import by.bautruk.pandanews.common.exception.DAOException;
import by.bautruk.pandanews.common.exception.ServiceException;
import by.bautruk.pandanews.common.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         4/12/2016 8:18 PM.
 */
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = ServiceException.class)
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDAO;

    @Override
    public List<User> takeAll() throws ServiceException {
        try {
            return userDAO.takeAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public User takeById(Long id) throws ServiceException {
        try {
            return userDAO.takeById(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Long insert(User user) throws ServiceException {
        try {
            return userDAO.insert(user);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean deleteById(Long id) throws ServiceException {
        try {
            return userDAO.deleteById(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean updateById(Long id, User user) throws ServiceException {
        try {
            return userDAO.updateById(id, user);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public User defineUser(String enteredLogin, String enteredPassword) throws ServiceException {
        try {
            return userDAO.defineUser(enteredLogin, enteredPassword);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
