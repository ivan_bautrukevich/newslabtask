package by.bautruk.pandanews.common.dao;

import by.bautruk.pandanews.common.exception.DAOException;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         4/12/2016 6:51 PM.
 */
public interface BaseDAO<T> {
    List<T> takeAll() throws DAOException;

    T takeById(Long id) throws DAOException;

    Long insert(T t) throws DAOException;

    boolean deleteById(Long id) throws DAOException;

    boolean updateById(Long id, T t) throws DAOException;
}
