package by.bautruk.pandanews.common.dao;

import by.bautruk.pandanews.common.entity.Author;
import by.bautruk.pandanews.common.exception.DAOException;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         4/12/2016 7:14 PM.
 */
public interface AuthorDAO extends BaseDAO<Author> {
    List<Author> takeByNewsId(Long id) throws DAOException;

    boolean createLinkWithNews(Long newsId, List<Long> authorId) throws DAOException;

    boolean deleteLinksWithNewsById(List<Long> newsId) throws DAOException;

    List<Author> takeAllNotExpired() throws DAOException;

    boolean updateAuthorExpired(Long id) throws DAOException;
}
