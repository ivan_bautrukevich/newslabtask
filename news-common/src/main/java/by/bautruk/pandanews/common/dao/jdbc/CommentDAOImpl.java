package by.bautruk.pandanews.common.dao.jdbc;

import by.bautruk.pandanews.common.dao.CommentDAO;
import by.bautruk.pandanews.common.entity.Comment;
import by.bautruk.pandanews.common.entity.News;
import by.bautruk.pandanews.common.exception.DAOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         4/12/2016 7:53 PM.
 *         The {@code CommentDAOImpl} class implements all the necessary methods
 *         which implements {@code CommentDAO} interface for manipulation
 *         with comment's data in database.
 */

public class CommentDAOImpl implements CommentDAO {

    private final static Logger LOG = LogManager.getLogger(CommentDAOImpl.class);

    @Autowired
    private DataSource dataSource;

    private static final String COLUMN_ID = "COMMENT_ID";
    private static final String COLUMN_NEWS_ID = "NEWS_ID";
    private static final String COLUMN_TEXT = "COMMENT_TEXT";
    private static final String COLUMN_DATE = "CREATION_DATE";
    private static final String SQL_FIND_ALL = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM COMMENTS";
    private static final String SQL_FIND_BY_ID = SQL_FIND_ALL + " WHERE COMMENT_ID = ?";
    private static final String SQL_INSERT = "INSERT INTO COMMENTS (NEWS_ID, COMMENT_TEXT, CREATION_DATE) " +
            "VALUES (?, ?, ?)";
    private static final String SQL_UPDATE = "UPDATE COMMENTS SET NEWS_ID=?, COMMENT_TEXT=?, CREATION_DATE=? " +
            "WHERE COMMENT_ID=?";
    private static final String SQL_DELETE = "DELETE FROM COMMENTS WHERE COMMENT_ID=?";
    private static final String SQL_DELETE_BY_NEWS_ID = "DELETE FROM COMMENTS WHERE COMMENTS.NEWS_ID=?";
    private static final String SQL_FIND_BY_NEWS_ID = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE " +
            "FROM COMMENTS WHERE NEWS_ID=? ORDER BY CREATION_DATE DESC";

    /**
     * Gives the list of all comments from database.
     *
     * @return the list of comments.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<Comment> takeAll() throws DAOException {
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_FIND_ALL)) {
            ResultSet rs = ps.executeQuery();
            return buildCommentList(rs);
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during all comments taking", e);
        }
    }

    /**
     * Represents the way of getting comment as an instance of
     * {@code Comment} through {@code id} value from database.
     *
     * @param id is comment's ID.
     * @return the only instance of {@code Comment} that defined just by using
     * {@code id}.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Comment takeById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        Comment comment = new Comment();
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_FIND_BY_ID)) {
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                comment.setId(rs.getLong(COLUMN_ID));
                comment.setNewsId(rs.getLong(COLUMN_NEWS_ID));
                comment.setCommentText(rs.getString(COLUMN_TEXT));
                comment.setCreationDate(rs.getTimestamp(COLUMN_DATE));
            }
            return comment;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during comment taking", e);
        }
    }

    /**
     * Represents the way of adding comment as an instance of
     * {@code Comment} from database.
     *
     * @param comment is an instance of {@code Comment} class that
     *                has to be placed into table.
     * @return ID of inserting comment
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Long insert(Comment comment) throws DAOException {
        if (comment == null) throw new DAOException("Invalid comment");
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_INSERT, new String[]{COLUMN_ID})) {
            ps.setLong(1, comment.getNewsId());
            ps.setString(2, comment.getCommentText());
            ps.setTimestamp(3, new Timestamp(new java.util.Date().getTime()));
            ps.executeUpdate();
            Long id = null;
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getLong(1);
            }
            return id;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during comment adding", e);
        }
    }

    /**
     * Represents the way of deleting comment from database by ID.
     *
     * @param id is comment's ID which will be delete.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_DELETE)) {
            ps.setLong(1, id);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during comment deleting", e);
        }
    }

    /**
     * Represents the way of updating comment as an instance of
     * {@code Comment} in database.
     *
     * @param id      is comment's ID which will be update.
     * @param comment is an instance of {@code Comment} class that
     *                has to be updating into table.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean updateById(Long id, Comment comment) throws DAOException {
        if (id == null || comment == null) new DAOException("Invalid id or comment");
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_UPDATE)) {
            ps.setLong(1, comment.getNewsId());
            ps.setString(2, comment.getCommentText());
            ps.setTimestamp(3, comment.getCreationDate());
            ps.setLong(4, id);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during comment updating", e);
        }
    }

    /**
     * Gives the list of all comments from database by news' id.
     *
     * @param news is news' instance.
     * @return the list of comments.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<Comment> takeCommentByNews(News news) throws DAOException {
        if (news == null) throw new DAOException("Invalid news");
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_FIND_BY_NEWS_ID)) {
            ps.setLong(1, news.getId());
            ResultSet rs = ps.executeQuery();
            return buildCommentList(rs);
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during comments taking by news id", e);
        }
    }

    /**
     * Represents the way of deleting comment from database by list of news' ID.
     *
     * @param newsId is list of news' ID.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteByNewsId(List<Long> newsId) throws DAOException {
        if (newsId == null) throw new DAOException("Invalid newsId");
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_DELETE_BY_NEWS_ID)) {
            for (Long id: newsId) {
                ps.setLong(1, id);
                ps.addBatch();
            }
            ps.executeBatch();
            return true;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during comment deleting", e);
        }
    }

    private List<Comment> buildCommentList(ResultSet rs) throws SQLException {
        List<Comment> result = new ArrayList<>();
        while (rs.next()) {
            Comment comment = new Comment();
            comment.setId(rs.getLong(COLUMN_ID));
            comment.setNewsId(rs.getLong(COLUMN_NEWS_ID));
            comment.setCommentText(rs.getString(COLUMN_TEXT));
            comment.setCreationDate(rs.getTimestamp(COLUMN_DATE));
            result.add(comment);
        }
        return result;
    }
}
