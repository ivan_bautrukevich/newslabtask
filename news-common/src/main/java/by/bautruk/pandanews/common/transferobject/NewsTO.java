package by.bautruk.pandanews.common.transferobject;

import by.bautruk.pandanews.common.entity.News;

import java.io.Serializable;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         6/1/2016 5:36 PM.
 *         The {@code NewsTO} class is a data trasnsfer object class which includes all
 *         the necessary getters and setters for its fields and also overrides
 *         {@code equals()}, {@code hashCode()} and {@code toString()}.
 */
public class NewsTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private News news;
    private List<Long> authors;
    private List<Long> tags;

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public List<Long> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Long> authors) {
        this.authors = authors;
    }

    public List<Long> getTags() {
        return tags;
    }

    public void setTags(List<Long> tags) {
        this.tags = tags;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NewsTO)) return false;

        NewsTO newsTO = (NewsTO) o;

        if (news != null ? !news.equals(newsTO.news) : newsTO.news != null) return false;
        if (authors != null ? !authors.equals(newsTO.authors) : newsTO.authors != null) return false;
        return tags != null ? tags.equals(newsTO.tags) : newsTO.tags == null;

    }

    @Override
    public int hashCode() {
        int result = news != null ? news.hashCode() : 0;
        result = 31 * result + (authors != null ? authors.hashCode() : 0);
        result = 31 * result + (tags != null ? tags.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "NewsTO{" +
                "news=" + news +
                ", authors=" + authors +
                ", tags=" + tags +
                '}';
    }
}
