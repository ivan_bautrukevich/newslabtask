package by.bautruk.pandanews.common.service.impl;

import by.bautruk.pandanews.common.entity.News;
import by.bautruk.pandanews.common.transferobject.NewsDTO;
import by.bautruk.pandanews.common.transferobject.NewsTO;
import by.bautruk.pandanews.common.transferobject.SearchCriteria;
import by.bautruk.pandanews.common.exception.ServiceException;
import by.bautruk.pandanews.common.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         4/19/2016 11:35 AM.
 */
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = ServiceException.class)
public class NewsDTOServiceImpl implements NewsDTOService {

    @Autowired
    private NewsService newsService;

    @Autowired
    private TagService tagService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private AuthorService authorService;


    @Override
    public Long addNewsTO(NewsTO newsTO) throws ServiceException {
        Long newsId = newsService.insert(newsTO.getNews());
        if (newsTO.getTags() != null) {
            tagService.createLinkWithNews(newsId, newsTO.getTags());
        }
        if (newsTO.getAuthors() != null) {
            authorService.createLinkWithNewsId(newsId, newsTO.getAuthors());
        }
        return newsId;
    }

    @Override
    public boolean updateNewsTO(NewsTO newsTO) throws ServiceException {
        newsService.updateById(newsTO.getNews().getId(), newsTO.getNews());

        tagService.deleteLinksWithNewsByNewsId(Arrays.asList(newsTO.getNews().getId()));
        if (newsTO.getTags() != null) {
            tagService.createLinkWithNews(newsTO.getNews().getId(), newsTO.getTags());
        }
        authorService.deleteLinksWithNewsById(Arrays.asList(newsTO.getNews().getId()));
        if (newsTO.getAuthors() != null) {
            authorService.createLinkWithNewsId(newsTO.getNews().getId(), newsTO.getAuthors());
        }
        return true;
    }

    @Override
    public NewsDTO takeNews(Long id) throws ServiceException {
        NewsDTO newsDTO = new NewsDTO();
        newsDTO.setNews(newsService.takeById(id));
        newsDTO.setAuthors(authorService.takeByNewsId(id));
        News news = new News();
        news.setId(id);
        newsDTO.setComments(commentService.takeCommentByNewsId(news));
        newsDTO.setTags(tagService.takeByNewsId(id));
        return newsDTO;
    }

    @Override
    public List<NewsDTO> findNewsByOrder(SearchCriteria searchCriteria,
                                         Long firstOrder, Long lastOrder) throws ServiceException {
        List<NewsDTO> listDTO = new ArrayList<>();
        List<News> newsList = newsService.findListIdOfNews(searchCriteria, firstOrder, lastOrder);
        for (News news : newsList) {
            NewsDTO newsDTO = new NewsDTO();
            newsDTO.setNews(news);
            newsDTO.setAuthors(authorService.takeByNewsId(news.getId()));
            newsDTO.setComments(commentService.takeCommentByNewsId(news));
            newsDTO.setTags(tagService.takeByNewsId(news.getId()));
            listDTO.add(newsDTO);
        }
        return listDTO;
    }

    public NewsDTO findNexNewsByNewsId(SearchCriteria searchCriteria, Long newsId, boolean next) throws ServiceException {
        News news = newsService.findNexNewsByNewsId(searchCriteria, newsId, next);
        NewsDTO newsDTO = new NewsDTO();
        if (news != null && news.getId() != null) {
            newsDTO.setNews(news);
            newsDTO.setAuthors(authorService.takeByNewsId(news.getId()));
            newsDTO.setComments(commentService.takeCommentByNewsId(news));
            newsDTO.setTags(tagService.takeByNewsId(news.getId()));
            return newsDTO;
        } else {
            return null;
        }
    }
}
