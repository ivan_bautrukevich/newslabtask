package by.bautruk.pandanews.common.dao;

import by.bautruk.pandanews.common.entity.Comment;
import by.bautruk.pandanews.common.entity.News;
import by.bautruk.pandanews.common.exception.DAOException;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         4/12/2016 7:50 PM.
 */
public interface CommentDAO extends BaseDAO<Comment> {

    List<Comment> takeCommentByNews(News news) throws DAOException;

    boolean deleteByNewsId(List<Long> newsId) throws DAOException;
}
