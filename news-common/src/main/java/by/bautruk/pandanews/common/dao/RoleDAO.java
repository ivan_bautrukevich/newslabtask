package by.bautruk.pandanews.common.dao;

import by.bautruk.pandanews.common.entity.Role;

/**
 * @author ivan.bautruvkevich
 *         4/12/2016 1:58 PM.
 */
public interface RoleDAO extends BaseDAO<Role> {

}
