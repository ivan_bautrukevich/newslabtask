package by.bautruk.pandanews.common.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

/**
 * @author ivan.bautruvkevich
 *         11.04.2016 9:28.
 *         The {@code Author} class is an entity-class which includes all
 *         the necessary getters and setters for its fields and also overrides
 *         {@code equals()}, {@code hashCode()} and {@code toString()}.
 */
@Entity
@Table(name = "AUTHORS")
@SequenceGenerator(name = "AUTHORS_SEQ", sequenceName = "AUTHORS_SEQ", initialValue = 1, allocationSize = 1)
public class Author implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "AUTHOR_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AUTHORS_SEQ")
    private Long id;

    @Column(name = "AUTHOR_NAME")
    private String name;

    @Column(name = "EXPIRED")
    private Timestamp expired;


    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "NEWS_AUTHORS",
            joinColumns = @JoinColumn(name = "AUTHOR_ID"),
            inverseJoinColumns = @JoinColumn(name = "NEWS_ID"))
    private Set<News> newsSet;

    public Author() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getExpired() {
        return expired;
    }

    public void setExpired(Timestamp expired) {
        this.expired = expired;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Set<News> getNewsSet() {
        return newsSet;
    }

    public void setNewsSet(Set<News> newsSet) {
        this.newsSet = newsSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Author)) return false;

        Author author = (Author) o;

        if (id != null ? !id.equals(author.id) : author.id != null) return false;
        if (name != null ? !name.equals(author.name) : author.name != null) return false;
        if (expired != null ? !expired.equals(author.expired) : author.expired != null) return false;
        return newsSet != null ? newsSet.equals(author.newsSet) : author.newsSet == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (expired != null ? expired.hashCode() : 0);
        result = 31 * result + (newsSet != null ? newsSet.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Author{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", expired=" + expired +
                ", newsSet=" + newsSet +
                '}';
    }
}
