package by.bautruk.pandanews.common.dao.eclipselink;

import by.bautruk.pandanews.common.dao.CommentDAO;
import by.bautruk.pandanews.common.entity.Comment;
import by.bautruk.pandanews.common.entity.News;
import by.bautruk.pandanews.common.exception.DAOException;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         8/8/2016 9:13 AM.
 *         The {@code CommentDAOImpl} class implements all the necessary methods
 *         which implements {@code CommentDAO} interface for manipulation
 *         with comment's data in database.
 */
@Transactional
public class CommentDAOImpl implements CommentDAO {

    private static final String JPQL_FIND_ALL = "select c from Comment c";
    private static final String JPQL_FIND_BY_NEWS = "select c from Comment c join c.news n where n=:paramNews";
    private static final String JPQL_DELETE_BY_NEWS_ID = "delete from Comment c where newsId=:newsId";

    @PersistenceContext
    EntityManager entityManager;

    /**
     * Gives the list of all comments from database.
     *
     * @return the list of comments.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<Comment> takeAll() throws DAOException {
        Query query = entityManager.createQuery(JPQL_FIND_ALL);
        return (List<Comment>) query.getResultList();
    }

    /**
     * Represents the way of getting comment as an instance of
     * {@code Comment} through {@code id} value from database.
     *
     * @param id is comment's ID.
     * @return the only instance of {@code Comment} that defined just by using
     * {@code id}.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Comment takeById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        return entityManager.find(Comment.class, id);
    }

    /**
     * Represents the way of adding comment as an instance of
     * {@code Comment} from database.
     *
     * @param comment is an instance of {@code Comment} class that
     *                has to be placed into table.
     * @return ID of inserting comment
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Long insert(Comment comment) throws DAOException {
        if (comment == null) throw new DAOException("Invalid comment");
        entityManager.persist(comment);
        return comment.getId();
    }

    /**
     * Represents the way of deleting comment from database by ID.
     *
     * @param id is comment's ID which will be delete.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        Comment comment = entityManager.find(Comment.class, id);
        entityManager.remove(comment);
        return true;
    }

    /**
     * Represents the way of updating comment as an instance of
     * {@code Comment} in database.
     *
     * @param id      is comment's ID which will be update.
     * @param comment is an instance of {@code Comment} class that
     *                has to be updating into table.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean updateById(Long id, Comment comment) throws DAOException {
        if (id == null || comment == null) throw new DAOException("Invalid id or user");
        comment.setId(id);
        entityManager.merge(comment);
        return true;
    }

    /**
     * Gives the list of all comments from database by news' id.
     *
     * @param news is news' instance.
     * @return the list of comments.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<Comment> takeCommentByNews(News news) throws DAOException {
        if (news == null) throw new DAOException("Invalid news");
        Query query = entityManager.createQuery(JPQL_FIND_BY_NEWS);
        query.setParameter("paramNews", news);
        return (List<Comment>) query.getResultList();
    }

    /**
     * Represents the way of deleting comment from database by list of news' ID.
     *
     * @param newsId is list of news' ID.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteByNewsId(List<Long> newsId) throws DAOException {
        if (newsId == null) throw new DAOException("Invalid newsId");
        for (Long id: newsId){
            Query query = entityManager.createQuery(JPQL_DELETE_BY_NEWS_ID);
            query.setParameter("newsId", id);
            query.executeUpdate();
        }
        return true;
    }
}
