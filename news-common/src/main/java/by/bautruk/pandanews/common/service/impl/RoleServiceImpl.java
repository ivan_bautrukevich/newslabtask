package by.bautruk.pandanews.common.service.impl;

import by.bautruk.pandanews.common.dao.RoleDAO;
import by.bautruk.pandanews.common.entity.Role;
import by.bautruk.pandanews.common.exception.DAOException;
import by.bautruk.pandanews.common.exception.ServiceException;
import by.bautruk.pandanews.common.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         11.04.2016 9:45.
 */
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = ServiceException.class)
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleDAO roleDAO;

    @Override
    public List<Role> takeAll() throws ServiceException {
        try {
            return roleDAO.takeAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Role takeById(Long id) throws ServiceException {
        try {
            return roleDAO.takeById(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Long insert(Role role) throws ServiceException {
        try {
            return roleDAO.insert(role);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean deleteById(Long id) throws ServiceException {
        try {
            return roleDAO.deleteById(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean updateById(Long id, Role role) throws ServiceException {
        try {
            return roleDAO.updateById(id, role);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

}
