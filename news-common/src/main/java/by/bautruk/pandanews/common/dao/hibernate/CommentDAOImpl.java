package by.bautruk.pandanews.common.dao.hibernate;

import by.bautruk.pandanews.common.dao.CommentDAO;
import by.bautruk.pandanews.common.entity.Comment;
import by.bautruk.pandanews.common.entity.News;
import by.bautruk.pandanews.common.entity.Tag;
import by.bautruk.pandanews.common.exception.DAOException;
import org.hibernate.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         7/13/2016 4:49 PM.
 *         The {@code CommentDAOImpl} class implements all the necessary methods
 *         which implements {@code CommentDAO} interface for manipulation
 *         with comment's data in database.
 */
//@Transactional
public class CommentDAOImpl implements CommentDAO {

    private static final String SQL_DELETE_LINK_BY_NEWS = "DELETE FROM COMMENTS WHERE NEWS_ID=?";
    private static final String HQL_TAKE_COMMENTS_BY_NEWS = "select c from Comment c join c.news n where n.id =:newsId";
    private static final String HQL_DELETE_BY_ID = "delete from Comment where id =:commentId";
    private static final String TRY = "try";


    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Gives the list of all comments from database.
     * sdcsdc
     * @return the list of comments.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<Comment> takeAll() throws DAOException {
        try {
            List<Comment> comments = sessionFactory.getCurrentSession()
                    .createCriteria(Comment.class)
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
            return comments;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during all comments taking", e);
        }
    }

    /**
     * Represents the way of getting comment as an instance of
     * {@code Comment} through {@code id} value from database.
     *
     * @param id is comment's ID.
     * @return the only instance of {@code Comment} that defined just by using
     * {@code id}.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Comment takeById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        try {
            return (Comment) sessionFactory.getCurrentSession().get(Comment.class, id);
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during comment taking", e);
        }
    }

    /**
     * Represents the way of adding comment as an instance of
     * {@code Comment} from database.
     *
     * @param comment is an instance of {@code Comment} class that
     *                has to be placed into table.
     * @return ID of inserting comment
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Long insert(Comment comment) throws DAOException {
        if (comment == null) throw new DAOException("Invalid comment");
        try {
            comment.setCreationDate(new Timestamp(new java.util.Date().getTime()));
            return (Long) sessionFactory.getCurrentSession().save(comment);
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during comment adding", e);
        }
    }

    /**
     * Represents the way of deleting comment from database by ID.
     *
     * @param id is comment's ID which will be delete.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        try {
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(HQL_DELETE_BY_ID);
            query.setParameter("commentId", id);
            query.executeUpdate();
            return true;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during comment deleting", e);
        }
    }

    /**
     * Represents the way of updating comment as an instance of
     * {@code Comment} in database.
     *
     * @param id      is comment's ID which will be update.
     * @param comment is an instance of {@code Comment} class that
     *                has to be updating into table.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean updateById(Long id, Comment comment) throws DAOException {
        if (id == null || comment == null) new DAOException("Invalid id or comment");
        try {
            comment.setId(id);
            sessionFactory.getCurrentSession().update(comment);
            return true;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during comment updating", e);
        }
    }

    /**
     * Represents the way of deleting comment from database by list of news' ID.
     *
     * @param newsId is list of news' ID.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteByNewsId(List<Long> newsId) throws DAOException {
        if (newsId == null) throw new DAOException("Invalid newsId");
        try {
            Session session = sessionFactory.getCurrentSession();
            for (Long id : newsId) {
                Query query = session.createSQLQuery(SQL_DELETE_LINK_BY_NEWS);
                query.setParameter(0, id);
                query.executeUpdate();
            }
            session.flush();
            return true;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during comment deleting", e);
        }
    }

    /**
     * Gives the list of all comments from database by news' id.
     *
     * @param news is news' instance.
     * @return the list of comments.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<Comment> takeCommentByNews(News news) throws DAOException {
        if (news == null) throw new DAOException("Invalid news");
        try {
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(HQL_TAKE_COMMENTS_BY_NEWS);
            query.setParameter("newsId", news.getId());
            List<Comment> list = query.list();
            return list;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during comments taking by news id", e);
        }
    }
}
