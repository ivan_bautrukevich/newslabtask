package by.bautruk.pandanews.common.transferobject;

import by.bautruk.pandanews.common.entity.Author;
import by.bautruk.pandanews.common.entity.Tag;

import java.io.Serializable;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         4/24/2016 6:37 PM.
 *         The {@code SearchCriteria} class is an entity-class which includes all
 *         the necessary getters and setters for its fields and also overrides
 *         {@code equals()}, {@code hashCode()} and {@code toString()}.
 */
public class SearchCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<Author> authors;
    private List<Tag> tags;

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SearchCriteria)) return false;

        SearchCriteria that = (SearchCriteria) o;

        if (authors != null ? !authors.equals(that.authors) : that.authors != null) return false;
        return tags != null ? tags.equals(that.tags) : that.tags == null;

    }

    @Override
    public int hashCode() {
        int result = authors != null ? authors.hashCode() : 0;
        result = 31 * result + (tags != null ? tags.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SearchCriteria{" +
                "authors=" + authors +
                ", tags=" + tags +
                '}';
    }
}
