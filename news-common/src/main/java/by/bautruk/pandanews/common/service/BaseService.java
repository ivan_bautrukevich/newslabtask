package by.bautruk.pandanews.common.service;

import by.bautruk.pandanews.common.exception.ServiceException;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         11.04.2016 9:21.
 */
public interface BaseService<T> {
    List<T> takeAll() throws ServiceException;

    T takeById(Long id) throws ServiceException;

    Long insert(T t) throws ServiceException;

    boolean deleteById(Long id) throws ServiceException;

    boolean updateById(Long id, T t) throws ServiceException;
}
