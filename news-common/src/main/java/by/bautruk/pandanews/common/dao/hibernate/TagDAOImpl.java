package by.bautruk.pandanews.common.dao.hibernate;

import by.bautruk.pandanews.common.dao.TagDAO;
import by.bautruk.pandanews.common.entity.Tag;
import by.bautruk.pandanews.common.exception.DAOException;
import org.hibernate.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         7/13/2016 2:34 PM.
 *         The {@code TagDAOImpl} class implements all the necessary methods
 *         which implements {@code TagDAO} interface for manipulation
 *         with tag's data in database.
 */
//@Transactional
public class TagDAOImpl implements TagDAO {

    private static final String SQL_CREATE_LINK_NEWS = "INSERT INTO NEWS_TAGS (NEWS_ID, TAG_ID) VALUES (?, ?)";
    private static final String SQL_DELETE_LINK_BY_NEWS = "DELETE FROM NEWS_TAGS WHERE NEWS_ID=?";
    private static final String SQL_DELETE_LINK_BY_TAG = "DELETE FROM NEWS_TAGS WHERE TAG_ID=?";
    private static final String HQL_DELETE_TAG_BY_ID = "delete from Tag where id =:tagId";
    private static final String HQL_TAKE_TAGS_BY_NEWS_ID = "select t from Tag t join t.newsSet n where n.id =:newsId";
    private static final String NEWS_ID_PARAMETER = "newsId";
    private static final String TAG_ID_PARAMETER = "tagId";


    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Gives the list of all tags from database.
     *
     * @return the list of tags.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<Tag> takeAll() throws DAOException {
        try {
            List<Tag> tags = sessionFactory.getCurrentSession()
                    .createCriteria(Tag.class)
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
            return tags;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during all tags taking", e);
        }
    }

    /**
     * Represents the way of getting tag as an instance of
     * {@code Tag} through {@code id} value from database.
     *
     * @param id is tag's ID.
     * @return the only instance of {@code Tag} that defined just by using
     * {@code id}.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Tag takeById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        try {
            return (Tag) sessionFactory.getCurrentSession().get(Tag.class, id);
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during tag taking", e);
        }
    }

    /**
     * Represents the way of adding tag as an instance of
     * {@code Tag} from database.
     *
     * @param tag is an instance of {@code Tag} class that
     *            has to be placed into table.
     * @return ID of inserting tag
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Long insert(Tag tag) throws DAOException {
        if (tag == null) throw new DAOException("Invalid tag");
        try {
            return (Long) sessionFactory.getCurrentSession().save(tag);
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during tag adding", e);
        }
    }

    /**
     * Represents the way of deleting tag from database by ID.
     *
     * @param id is tag's ID which will be delete.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        try {
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(HQL_DELETE_TAG_BY_ID);
            query.setParameter(TAG_ID_PARAMETER, id);
            query.executeUpdate();
            return true;
        } catch (HibernateException e) {
            throw new DAOException(e);
        }
    }

    /**
     * Represents the way of updating tag as an instance of
     * {@code Tag} in database.
     *
     * @param id  is tag's ID which will be update.
     * @param tag is an instance of {@code Tag} class that
     *            has to be updating into table.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean updateById(Long id, Tag tag) throws DAOException {
        if (id == null || tag == null) throw new DAOException("Invalid id or tag");
        try {
            tag.setId(id);
            sessionFactory.getCurrentSession().update(tag);
            return true;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during tag deleting", e);
        }
    }

    /**
     * Gives the list of all tags from database by news' ID.
     *
     * @param id is news' ID.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<Tag> takeByNewsId(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        try {
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(HQL_TAKE_TAGS_BY_NEWS_ID);
            query.setParameter(NEWS_ID_PARAMETER, id);
            List<Tag> list = query.list();
            return list;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during all tags taking by id", e);
        }
    }

    /**
     * Represents the way of adding links from news to author in database.
     *
     * @param newsId is news' ID.
     * @param tagId  is tag's ID.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean createLinkWithNews(Long newsId, List<Long> tagId) throws DAOException {
        if (newsId == null || tagId == null) new DAOException("Invalid newsId or authorId");
        try {
            Session session = sessionFactory.getCurrentSession();
            for (Long id : tagId) {
                Query query = session.createSQLQuery(SQL_CREATE_LINK_NEWS);
                query.setParameter(0, newsId);
                query.setParameter(1, id);
                query.executeUpdate();
            }
            session.flush();
            return true;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during link adding", e);
        }
    }

    /**
     * Represents the way of deleting links from news to author in database
     * by news' ID.
     *
     * @param newsId is news' ID.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteLinksWithNewsByNewsId(List<Long> newsId) throws DAOException {
        if (newsId == null) throw new DAOException("Invalid newsId");
        try {
            Session session = sessionFactory.getCurrentSession();
            for (Long id : newsId) {
                Query query = session.createSQLQuery(SQL_DELETE_LINK_BY_NEWS);
                query.setParameter(0, id);
                query.executeUpdate();
            }
            session.flush();
            return true;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during link deleting", e);
        }
    }

    /**
     * Represents the way of deleting links from news to author in database
     * by tag's ID.
     *
     * @param tagId is news' ID.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteLinksWithNewsByTagId(Long tagId) throws DAOException {
        if (tagId == null) throw new DAOException("Invalid newsId");
        try {
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createSQLQuery(SQL_DELETE_LINK_BY_TAG);
            query.setParameter(0, tagId);
            query.executeUpdate();
            session.flush();
            return true;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during link deleting", e);
        }
    }
}
