package by.bautruk.pandanews.common.dao.jdbc;

import by.bautruk.pandanews.common.entity.Tag;
import by.bautruk.pandanews.common.dao.TagDAO;
import by.bautruk.pandanews.common.exception.DAOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         4/12/2016 7:54 PM.
 *         The {@code TagDAOImpl} class implements all the necessary methods
 *         which implements {@code TagDAO} interface for manipulation
 *         with tag's data in database.
 */

public class TagDAOImpl implements TagDAO {

    private final static Logger LOG = LogManager.getLogger(TagDAOImpl.class);

    @Autowired
    private DataSource dataSource;

    private static final String COLUMN_ID = "TAG_ID";
    private static final String COLUMN_NAME = "TAG_NAME";
    private static final String SQL_FIND_ALL = "SELECT TAG_ID, TAG_NAME FROM TAGS";
    private static final String SQL_FIND_BY_ID = SQL_FIND_ALL + " WHERE TAG_ID= ?";
    private static final String SQL_INSERT = "INSERT INTO TAGS (TAG_NAME) VALUES (?)";
    private static final String SQL_UPDATE = "UPDATE TAGS SET TAG_NAME=? WHERE TAG_ID = ?";
    private static final String SQL_DELETE = "DELETE FROM TAGS WHERE TAG_ID=?";
    private static final String SQL_CREATE_LINK_NEWS = "INSERT INTO NEWS_TAGS (NEWS_ID, TAG_ID) VALUES (?, ?)";
    private static final String SQL_DELETE_LINK_BY_NEWS = "DELETE FROM NEWS_TAGS WHERE NEWS_ID=?";
    private static final String SQL_DELETE_LINK_BY_TAG = "DELETE FROM NEWS_TAGS WHERE TAG_ID=?";
    private static final String SQL_FIND_BY_NEWS_ID = "SELECT TAGS.TAG_ID, TAGS.TAG_NAME FROM TAGS " +
            "JOIN NEWS_TAGS ON TAGS.TAG_ID = NEWS_TAGS.TAG_ID WHERE NEWS_TAGS.NEWS_ID=?";

    /**
     * Gives the list of all tags from database.
     *
     * @return the list of tags.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<Tag> takeAll() throws DAOException {
        List<Tag> result = new ArrayList<>();
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_FIND_ALL)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Tag tag = new Tag();
                tag.setId(rs.getLong(COLUMN_ID));
                tag.setName(rs.getString(COLUMN_NAME));
                result.add(tag);
            }
            return result;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during all tags taking", e);
        }
    }

    /**
     * Represents the way of getting tag as an instance of
     * {@code Tag} through {@code id} value from database.
     *
     * @param id is tag's ID.
     * @return the only instance of {@code Tag} that defined just by using
     * {@code id}.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Tag takeById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        Tag tag = new Tag();
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_FIND_BY_ID)) {
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                tag.setId(rs.getLong(COLUMN_ID));
                tag.setName(rs.getString(COLUMN_NAME));
            }
            return tag;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during tag taking", e);
        }
    }

    /**
     * Represents the way of adding tag as an instance of
     * {@code Tag} from database.
     *
     * @param tag is an instance of {@code Tag} class that
     *            has to be placed into table.
     * @return ID of inserting tag
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Long insert(Tag tag) throws DAOException {
        if (tag == null) throw new DAOException("Invalid tag");
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_INSERT, new String[]{COLUMN_ID})) {
            ps.setString(1, tag.getName());
            ps.executeUpdate();
            Long id = null;
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getLong(1);
            }
            return id;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during tag adding", e);
        }
    }

    /**
     * Represents the way of deleting tag from database by ID.
     *
     * @param id is tag's ID which will be delete.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_DELETE)) {
            LOG.debug(id);
            ps.setLong(1, id);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    /**
     * Represents the way of updating tag as an instance of
     * {@code Tag} in database.
     *
     * @param id  is tag's ID which will be update.
     * @param tag is an instance of {@code Tag} class that
     *            has to be updating into table.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean updateById(Long id, Tag tag) throws DAOException {
        if (id == null || tag == null) throw new DAOException("Invalid id or tag");
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_UPDATE)) {
            ps.setString(1, tag.getName());
            ps.setLong(2, id);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during tag deleting", e);
        }
    }

    /**
     * Gives the list of all tags from database by news' ID.
     *
     * @param id is news' ID.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<Tag> takeByNewsId(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        List<Tag> result = new ArrayList<>();
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_FIND_BY_NEWS_ID)) {
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Tag tag = new Tag();
                tag.setId(rs.getLong(COLUMN_ID));
                tag.setName(rs.getString(COLUMN_NAME));
                result.add(tag);
            }
            return result;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during all tags taking", e);
        }
    }

    /**
     * Represents the way of adding links from news to author in database.
     *
     * @param newsId is news' ID.
     * @param tagId  is tag's ID.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean createLinkWithNews(Long newsId, List<Long> tagId) throws DAOException {
        if (newsId == null || tagId == null) new DAOException("Invalid newsId or authorId");
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_CREATE_LINK_NEWS)) {
            for (Long id: tagId) {
                ps.setLong(1, newsId);
                ps.setLong(2, id);
                ps.addBatch();
            }
            ps.executeBatch();
            return true;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during link adding", e);
        }
    }

    /**
     * Represents the way of deleting links from news to author in database
     * by news' ID.
     *
     * @param newsId is news' ID.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteLinksWithNewsByNewsId(List<Long> newsId) throws DAOException {
        if (newsId == null) throw new DAOException("Invalid newsId");
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_DELETE_LINK_BY_NEWS)) {
            for (Long id: newsId){
                ps.setLong(1, id);
                ps.addBatch();
            }
            ps.executeBatch();
            return true;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during link deleting", e);
        }
    }

    /**
     * Represents the way of deleting links from news to author in database
     * by tag's ID.
     *
     * @param tagId is news' ID.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteLinksWithNewsByTagId(Long tagId) throws DAOException {
        if (tagId == null) throw new DAOException("Invalid newsId");
        try (Connection con = dataSource.getConnection();
             PreparedStatement psDelete = con.prepareStatement(SQL_DELETE_LINK_BY_TAG)) {
            psDelete.setLong(1, tagId);
            psDelete.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during link deleting", e);
        }
    }
}
