package by.bautruk.pandanews.common.dao.hibernate;

import by.bautruk.pandanews.common.dao.NewsDAO;
import by.bautruk.pandanews.common.entity.Author;
import by.bautruk.pandanews.common.entity.News;
import by.bautruk.pandanews.common.transferobject.SearchCriteria;
import by.bautruk.pandanews.common.entity.Tag;
import by.bautruk.pandanews.common.exception.DAOException;
import org.hibernate.*;
import org.hibernate.type.IntegerType;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         7/13/2016 4:49 PM.
 *         The {@code NewsDAOImpl} class implements all the necessary methods
 *         which implements {@code NewsDAO} interface for manipulation
 *         with news' data in database.
 */
//@Transactional
public class NewsDAOImpl implements NewsDAO {

    private static final String HQL_DELETE_AUTHOR_BY_ID = "delete from News where id =:newsId";

    private static final String PARAMETER_AITHOR_ID = "newsId";

    private static final String SQL_WHERE = " WHERE ";

    private static final String SQL_AND = "  AND  ";

    private static final String SQL_TAG_ID_IN = " NEWS_TAGS.TAG_ID IN (%s)";

    private static final String SQL_AUTHOR_ID_IN = " NEWS_AUTHORS.AUTHOR_ID IN (%s)";

    private static final String SQL_COUNT_NEWS = "SELECT COUNT(DISTINCT NEWS.NEWS_ID) AS RESULT FROM NEWS " +
            "LEFT JOIN NEWS_AUTHORS ON NEWS.NEWS_ID = NEWS_AUTHORS.NEWS_ID " +
            "LEFT JOIN NEWS_TAGS ON NEWS.NEWS_ID = NEWS_TAGS.NEWS_ID";

    private static final String SQL_FIND_SORTED_BY_NUMB_COMMENTS_AND_DATE_MODIF_PART_1 =
            "SELECT b.* FROM (SELECT a.*, ROWNUM AS RN FROM" +
                    "(SELECT NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE, NEWS.OPTLOCK  " +
                    "FROM NEWS " +
                    "LEFT JOIN NEWS_TAGS ON NEWS.NEWS_ID = NEWS_TAGS.NEWS_ID " +
                    "LEFT JOIN NEWS_AUTHORS ON NEWS.NEWS_ID = NEWS_AUTHORS.NEWS_ID " +
                    "LEFT JOIN COMMENTS ON NEWS.NEWS_ID = COMMENTS.NEWS_ID ";

    private static final String SQL_FIND_SORTED_BY_NUMB_COMMENTS_AND_DATE_MODIF_PART_2 =
            "GROUP BY NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE, NEWS.OPTLOCK  " +
                    "ORDER BY COUNT(COMMENTS.COMMENT_ID) DESC, NEWS.MODIFICATION_DATE DESC) a) b " +
                    "WHERE RN BETWEEN ? AND ?";

    private static final String SQL_FIND_NEXT_NEWS_BY_NEWS_ID_PART_1 =
            "SELECT NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE FROM NEWS WHERE NEWS_ID=(\n" +
                    "SELECT RESUALT FROM(" +
                    "SELECT NEWS_ID, RN, LEAD(NEWS_ID) OVER (ORDER BY RN) AS RESUALT FROM " +
                    "(SELECT NEWS_ID, ROWNUM AS RN FROM " +
                    "(SELECT NEWS.NEWS_ID FROM NEWS " +
                    "LEFT JOIN NEWS_TAGS ON NEWS.NEWS_ID = NEWS_TAGS.NEWS_ID " +
                    "LEFT JOIN NEWS_AUTHORS ON NEWS.NEWS_ID = NEWS_AUTHORS.NEWS_ID " +
                    "LEFT JOIN COMMENTS ON NEWS.NEWS_ID = COMMENTS.NEWS_ID ";

    private static final String SQL_FIND_PREVIOUS_NEWS_BY_NEWS_ID_PART_1 =
            "SELECT NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE FROM NEWS WHERE NEWS_ID=(\n" +
                    "SELECT RESUALT FROM(" +
                    "SELECT NEWS_ID, RN, LAG(NEWS_ID) OVER (ORDER BY RN) AS RESUALT FROM " +
                    "(SELECT NEWS_ID, ROWNUM AS RN FROM " +
                    "(SELECT NEWS.NEWS_ID FROM NEWS " +
                    "LEFT JOIN NEWS_TAGS ON NEWS.NEWS_ID = NEWS_TAGS.NEWS_ID " +
                    "LEFT JOIN NEWS_AUTHORS ON NEWS.NEWS_ID = NEWS_AUTHORS.NEWS_ID " +
                    "LEFT JOIN COMMENTS ON NEWS.NEWS_ID = COMMENTS.NEWS_ID ";

    private static final String SQL_FIND_NEXT_NEWS_BY_NEWS_ID_PART_2 = "GROUP BY NEWS.NEWS_ID, NEWS.MODIFICATION_DATE " +
            "ORDER BY COUNT(COMMENTS.COMMENT_ID) DESC, NEWS.MODIFICATION_DATE DESC)))" +
            "WHERE NEWS_ID=?)";

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Gives the list of all news from database.
     *
     * @return the list of news.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<News> takeAll() throws DAOException {
        try {
            List<News> news = sessionFactory.getCurrentSession()
                    .createCriteria(News.class)
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
            return news;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during all news taking", e);
        }
    }

    /**
     * Represents the way of getting news as an instance of
     * {@code News} through {@code id} value from database.
     *
     * @param id is news' ID.
     * @return the only instance of {@code News} that defined just by using
     * {@code id}.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public News takeById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        try {
            return (News) sessionFactory.getCurrentSession().get(News.class, id);
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during news taking", e);
        }
    }

    /**
     * Represents the way of adding news as an instance of
     * {@code News} from database.
     *
     * @param news is an instance of {@code News} class that
     *             has to be placed into table.
     * @return ID of inserting news
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Long insert(News news) throws DAOException {
        if (news == null) throw new DAOException("Invalid news");
        try {
            news.setCreationDate(new Timestamp(new java.util.Date().getTime()));
            news.setModificationDate(new Timestamp(new java.util.Date().getTime()));
            Session session = sessionFactory.getCurrentSession();
            Long id = (Long) session.save(news);
            session.flush();
            return id;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during news adding", e);
        }
    }

    /**
     * Represents the way of deleting news from database by ID.
     *
     * @param id is news' ID which will be delete.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        try {
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(HQL_DELETE_AUTHOR_BY_ID);
            query.setParameter(PARAMETER_AITHOR_ID, id);
            query.executeUpdate();
            session.flush();
            return true;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during news deleting", e);
        }
    }

    /**
     * Represents the way of updating news as an instance of
     * {@code News} in database.
     *
     * @param id   is news's ID which will be update.
     * @param news is an instance of {@code News} class that
     *             has to be updating into table.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean updateById(Long id, News news) throws DAOException {
        if (id == null || news == null) throw new DAOException("Invalid id or news");
        try {
            news.setId(id);
            news.setModificationDate(new Timestamp(new java.util.Date().getTime()));
            Session session = sessionFactory.getCurrentSession();
            session.update(news);
            session.flush();
            return true;
        }catch (StaleObjectStateException e){
            throw e;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during news updating", e);
        }
    }

    /**
     * Represents the way of deleting news from database by list of ID.
     *
     * @param listId is list of news' ID which will be delete.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteById(List<Long> listId) throws DAOException {
        if (listId == null) throw new DAOException("Invalid id");
        try {
            Session session = sessionFactory.getCurrentSession();
            for (Long id : listId) {
                Query query = session.createQuery(HQL_DELETE_AUTHOR_BY_ID);
                query.setParameter(PARAMETER_AITHOR_ID, id);
                query.executeUpdate();
            }
            session.flush();
            return true;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during news deleting", e);
        }
    }

    /**
     * Gives the list of all news from database.
     *
     * @param searchCriteria is an instance of {@code SearchCriteria}.
     * @return the list of news.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<News> findNews(SearchCriteria searchCriteria) throws DAOException {
        throw new DAOException("method not supporting");
    }

    /**
     * Represents the way of getting count of news.
     *
     * @return the only instance of {@code Integer} that defined just by using
     * {@code id}.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Integer countNews(SearchCriteria searchCriteria) throws DAOException {
        if (searchCriteria == null || searchCriteria.getAuthors() == null || searchCriteria.getTags() == null)
            throw new DAOException("Invalid searchCriteria");
        try {
            Session session = sessionFactory.getCurrentSession();

            StringBuilder querySB = new StringBuilder();
            querySB.append(SQL_COUNT_NEWS);
            if (searchCriteria.getAuthors().size() != 0 && searchCriteria.getTags().size() != 0) {
                querySB.append(SQL_WHERE).append(buildSQLByAuthor(searchCriteria.getAuthors()));
                querySB.append(SQL_AND).append(buildSQLByTag(searchCriteria.getTags()));
            }
            if (searchCriteria.getAuthors().size() != 0 && searchCriteria.getTags().size() == 0) {
                querySB.append(SQL_WHERE).append(buildSQLByAuthor(searchCriteria.getAuthors()));
            }
            if (searchCriteria.getAuthors().size() == 0 && searchCriteria.getTags().size() != 0) {
                querySB.append(SQL_WHERE).append(buildSQLByTag(searchCriteria.getTags()));
            }
            Integer count = (Integer) session
                    .createSQLQuery(querySB.toString())
                    .addScalar("RESULT", IntegerType.INSTANCE)
                    .uniqueResult();
            return count;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during all news taking", e);
        }
    }

    /**
     * Gives the list of all news from database by searchCriteria between two orders.
     *
     * @param searchCriteria is an instance of {@code SearchCriteria}.
     * @param firstOrder     is
     * @param lastOrder
     * @return the list of news.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<News> findListOfNews(SearchCriteria searchCriteria, Long firstOrder, Long lastOrder) throws DAOException {
        if (searchCriteria == null || searchCriteria.getAuthors() == null || searchCriteria.getTags() == null)
            throw new DAOException("Invalid searchCriteria");
        try {
            Session session = sessionFactory.getCurrentSession();

            StringBuilder querySB = new StringBuilder();
            querySB.append(SQL_FIND_SORTED_BY_NUMB_COMMENTS_AND_DATE_MODIF_PART_1);
            if (searchCriteria.getAuthors().size() != 0 && searchCriteria.getTags().size() != 0) {
                querySB.append(SQL_WHERE).append(buildSQLByAuthor(searchCriteria.getAuthors()));
                querySB.append(SQL_AND).append(buildSQLByTag(searchCriteria.getTags()));
            }
            if (searchCriteria.getAuthors().size() != 0 && searchCriteria.getTags().size() == 0) {
                querySB.append(SQL_WHERE).append(buildSQLByAuthor(searchCriteria.getAuthors()));
            }
            if (searchCriteria.getAuthors().size() == 0 && searchCriteria.getTags().size() != 0) {
                querySB.append(SQL_WHERE).append(buildSQLByTag(searchCriteria.getTags()));
            }
            querySB.append(SQL_FIND_SORTED_BY_NUMB_COMMENTS_AND_DATE_MODIF_PART_2);
            SQLQuery query = session.createSQLQuery(querySB.toString());
            query.setLong(0, firstOrder);
            query.setLong(1, lastOrder);
            List<News> news = query.addEntity("b.*", News.class).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
            return news;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during all news taking", e);
        }
    }

    /**
     * Gives the next or the previous news for current news from database by searchCriteria between two orders.
     *
     * @param searchCriteria is an instance of {@code SearchCriteria}.
     * @param newsId         is id's current news.
     * @param next           is parameter which show next news (if true) or previous news (if false).
     * @return the list of news.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public News findNexNewsByNewsId(SearchCriteria searchCriteria, Long newsId, boolean next) throws DAOException {
        if (searchCriteria == null || searchCriteria.getAuthors() == null || searchCriteria.getTags() == null)
            throw new DAOException("Invalid searchCriteria");
        try {
            Session session = sessionFactory.getCurrentSession();

            StringBuilder querySB = new StringBuilder();
            if (next == true) {
                querySB.append(SQL_FIND_NEXT_NEWS_BY_NEWS_ID_PART_1);
            } else {
                querySB.append(SQL_FIND_PREVIOUS_NEWS_BY_NEWS_ID_PART_1);
            }
            if (searchCriteria.getAuthors().size() != 0 && searchCriteria.getTags().size() != 0) {
                querySB.append(SQL_WHERE).append(buildSQLByAuthor(searchCriteria.getAuthors()));
                querySB.append(SQL_AND).append(buildSQLByTag(searchCriteria.getTags()));
            }
            if (searchCriteria.getAuthors().size() != 0 && searchCriteria.getTags().size() == 0) {
                querySB.append(SQL_WHERE).append(buildSQLByAuthor(searchCriteria.getAuthors()));
            }
            if (searchCriteria.getAuthors().size() == 0 && searchCriteria.getTags().size() != 0) {
                querySB.append(SQL_WHERE).append(buildSQLByTag(searchCriteria.getTags()));
            }
            querySB.append(SQL_FIND_NEXT_NEWS_BY_NEWS_ID_PART_2);
            SQLQuery query = session.createSQLQuery(querySB.toString());
            query.setLong(0, newsId);
            News news = (News) query.addEntity("b.*", News.class).uniqueResult();
            return news;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during news taking", e);
        }
    }

    private String buildSQLByTag(List<Tag> tags) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < tags.size(); i++) {
            if (i != tags.size() - 1) {
                result.append(tags.get(i).getId()).append(", ");
            } else {
                result.append(tags.get(i).getId());
            }
        }
        String query = String.format(SQL_TAG_ID_IN, result);
        return query;
    }


    private String buildSQLByAuthor(List<Author> authors) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < authors.size(); i++) {
            if (i != authors.size() - 1) {
                result.append(authors.get(i).getId()).append(", ");
            } else {
                result.append(authors.get(i).getId());
            }
        }
        return String.format(SQL_AUTHOR_ID_IN, result);
    }
}
