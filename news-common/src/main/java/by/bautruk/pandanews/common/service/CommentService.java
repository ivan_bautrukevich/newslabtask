package by.bautruk.pandanews.common.service;

import by.bautruk.pandanews.common.entity.Comment;
import by.bautruk.pandanews.common.entity.News;
import by.bautruk.pandanews.common.exception.ServiceException;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         4/12/2016 8:07 PM.
 */
public interface CommentService extends BaseService<Comment> {
    List<Comment> takeCommentByNewsId(News news) throws ServiceException;
}
