package by.bautruk.pandanews.common.dao.jdbc;

import by.bautruk.pandanews.common.dao.NewsDAO;
import by.bautruk.pandanews.common.entity.Author;
import by.bautruk.pandanews.common.entity.News;
import by.bautruk.pandanews.common.transferobject.SearchCriteria;
import by.bautruk.pandanews.common.entity.Tag;
import by.bautruk.pandanews.common.exception.DAOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         4/12/2016 7:17 PM.
 *         The {@code NewsDAOImpl} class implements all the necessary methods
 *         which implements {@code NewsDAO} interface for manipulation
 *         with news' data in database.
 */
public class NewsDAOImpl implements NewsDAO {

    private final static Logger LOG = LogManager.getLogger(NewsDAOImpl.class);

    @Autowired
    private DataSource dataSource;

    private static final String COLUMN_ID = "NEWS_ID";
    private static final String COLUMN_TITLE = "TITLE";
    private static final String COLUMN_SHORT_TEXT = "SHORT_TEXT";
    private static final String COLUMN_FULL_TEXT = "FULL_TEXT";
    private static final String COLUMN_CREATION_DATE = "CREATION_DATE";
    private static final String COLUMN_MODIFICATION_DATE = "MODIFICATION_DATE";

    private static final String SQL_TAG_ID_IN = " NEWS_TAGS.TAG_ID IN (%s)";

    private static final String SQL_AUTHOR_ID_IN = " NEWS_AUTHORS.AUTHOR_ID IN (%s)";

    private static final String SQL_WHERE = " WHERE ";

    private static final String SQL_AND = "  AND  ";

    private static final String SQL_FIND_ALL = "SELECT NEWS.NEWS_ID, NEWS.TITLE, " +
            "NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE FROM NEWS " +
            "LEFT JOIN NEWS_AUTHORS ON NEWS.NEWS_ID = NEWS_AUTHORS.NEWS_ID " +
            "LEFT JOIN NEWS_TAGS ON NEWS.NEWS_ID = NEWS_TAGS.NEWS_ID";


    private static final String SQL_FIND_BY_ID = SQL_FIND_ALL + " WHERE NEWS.NEWS_ID= ?";

    private static final String SQL_INSERT = "INSERT INTO NEWS (TITLE, SHORT_TEXT, FULL_TEXT, " +
            "CREATION_DATE, MODIFICATION_DATE) " +
            "VALUES (?, ?, ?, ?, ?)";

    private static final String SQL_UPDATE = "UPDATE NEWS SET TITLE=?, SHORT_TEXT=?, FULL_TEXT=?, " +
            " MODIFICATION_DATE=? WHERE NEWS_ID=?";

    private static final String SQL_DELETE = "DELETE FROM NEWS WHERE NEWS_ID = ?";

    private static final String SQL_COUNT_NEWS = "SELECT COUNT(DISTINCT NEWS.NEWS_ID) AS RESULT FROM NEWS " +
            "LEFT JOIN NEWS_AUTHORS ON NEWS.NEWS_ID = NEWS_AUTHORS.NEWS_ID " +
            "LEFT JOIN NEWS_TAGS ON NEWS.NEWS_ID = NEWS_TAGS.NEWS_ID";

    private static final String SQL_FIND_SORTED_BY_NUMB_COMMENTS_AND_DATE_MODIF_PART_1 =
            "SELECT b.* FROM (SELECT a.*, ROWNUM AS RN FROM" +
                    "(SELECT NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE " +
                    "FROM NEWS " +
                    "LEFT JOIN NEWS_TAGS ON NEWS.NEWS_ID = NEWS_TAGS.NEWS_ID " +
                    "LEFT JOIN NEWS_AUTHORS ON NEWS.NEWS_ID = NEWS_AUTHORS.NEWS_ID " +
                    "LEFT JOIN COMMENTS ON NEWS.NEWS_ID = COMMENTS.NEWS_ID ";

    private static final String SQL_FIND_SORTED_BY_NUMB_COMMENTS_AND_DATE_MODIF_PART_2 =
            "GROUP BY NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE " +
                    "ORDER BY COUNT(COMMENTS.COMMENT_ID) DESC, NEWS.MODIFICATION_DATE DESC) a) b " +
                    "WHERE RN BETWEEN ? AND ?";

    private static final String SQL_FIND_NEXT_NEWS_BY_NEWS_ID_PART_1 =
            "SELECT NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE FROM NEWS WHERE NEWS_ID=(\n" +
                    "SELECT RESUALT FROM(" +
                    "SELECT NEWS_ID, RN, LEAD(NEWS_ID) OVER (ORDER BY RN) AS RESUALT FROM " +
                    "(SELECT NEWS_ID, ROWNUM AS RN FROM " +
                    "(SELECT NEWS.NEWS_ID FROM NEWS " +
                    "LEFT JOIN NEWS_TAGS ON NEWS.NEWS_ID = NEWS_TAGS.NEWS_ID " +
                    "LEFT JOIN NEWS_AUTHORS ON NEWS.NEWS_ID = NEWS_AUTHORS.NEWS_ID " +
                    "LEFT JOIN COMMENTS ON NEWS.NEWS_ID = COMMENTS.NEWS_ID ";

    private static final String SQL_FIND_PREVIOUS_NEWS_BY_NEWS_ID_PART_1 =
            "SELECT NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE FROM NEWS WHERE NEWS_ID=(\n" +
                    "SELECT RESUALT FROM(" +
                    "SELECT NEWS_ID, RN, LAG(NEWS_ID) OVER (ORDER BY RN) AS RESUALT FROM " +
                    "(SELECT NEWS_ID, ROWNUM AS RN FROM " +
                    "(SELECT NEWS.NEWS_ID FROM NEWS " +
                    "LEFT JOIN NEWS_TAGS ON NEWS.NEWS_ID = NEWS_TAGS.NEWS_ID " +
                    "LEFT JOIN NEWS_AUTHORS ON NEWS.NEWS_ID = NEWS_AUTHORS.NEWS_ID " +
                    "LEFT JOIN COMMENTS ON NEWS.NEWS_ID = COMMENTS.NEWS_ID ";

    private static final String SQL_FIND_NEXT_NEWS_BY_NEWS_ID_PART_2 = "GROUP BY NEWS.NEWS_ID, NEWS.MODIFICATION_DATE " +
            "ORDER BY COUNT(COMMENTS.COMMENT_ID) DESC, NEWS.MODIFICATION_DATE DESC)))" +
            "WHERE NEWS_ID=?)";

    /**
     * Gives the list of all news from database.
     *
     * @return the list of news.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<News> takeAll() throws DAOException {
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_FIND_ALL)) {
            ResultSet rs = ps.executeQuery();
            return buildNewsList(rs);
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during all news taking", e);
        }
    }

    /**
     * Represents the way of getting count of news.
     *
     * @return the only instance of {@code Integer} that defined just by using
     * {@code id}.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Integer countNews(SearchCriteria searchCriteria) throws DAOException {
        if (searchCriteria == null || searchCriteria.getAuthors() == null || searchCriteria.getTags() == null)
            throw new DAOException("Invalid searchCriteria");
        try (Connection con = dataSource.getConnection()) {
            StringBuilder query = new StringBuilder();
            query.append(SQL_COUNT_NEWS);
            if (searchCriteria.getAuthors().size() != 0 && searchCriteria.getTags().size() != 0) {
                query.append(SQL_WHERE).append(buildSQLByAuthor(searchCriteria.getAuthors()));
                query.append(SQL_AND).append(buildSQLByTag(searchCriteria.getTags()));
            }
            if (searchCriteria.getAuthors().size() != 0 && searchCriteria.getTags().size() == 0) {
                query.append(SQL_WHERE).append(buildSQLByAuthor(searchCriteria.getAuthors()));
            }
            if (searchCriteria.getAuthors().size() == 0 && searchCriteria.getTags().size() != 0) {
                query.append(SQL_WHERE).append(buildSQLByTag(searchCriteria.getTags()));
            }
            PreparedStatement ps = con.prepareStatement(query.toString());
            Integer count = null;
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                count = rs.getInt("RESULT");
            }
            return count;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during all news taking", e);
        }
    }

    /**
     * Represents the way of getting news as an instance of
     * {@code News} through {@code id} value from database.
     *
     * @param id is news' ID.
     * @return the only instance of {@code News} that defined just by using
     * {@code id}.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public News takeById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        News news = new News();
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_FIND_BY_ID)) {
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                news.setId(rs.getLong(COLUMN_ID));
                news.setTitle(rs.getString(COLUMN_TITLE));
                news.setShortText(rs.getString(COLUMN_SHORT_TEXT));
                news.setFullText(rs.getString(COLUMN_FULL_TEXT));
                news.setCreationDate(rs.getTimestamp(COLUMN_CREATION_DATE));
                news.setModificationDate(rs.getTimestamp(COLUMN_MODIFICATION_DATE));
            }
            return news;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during news taking", e);
        }
    }

    /**
     * Represents the way of adding news as an instance of
     * {@code News} from database.
     *
     * @param news is an instance of {@code News} class that
     *             has to be placed into table.
     * @return ID of inserting news
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Long insert(News news) throws DAOException {
        if (news == null) throw new DAOException("Invalid news");
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_INSERT, new String[]{COLUMN_ID})) {
            ps.setString(1, news.getTitle());
            ps.setString(2, news.getShortText());
            ps.setString(3, news.getFullText());
            ps.setTimestamp(4, new Timestamp(new java.util.Date().getTime()));
            ps.setTimestamp(5, new Timestamp(new java.util.Date().getTime()));
            ps.executeUpdate();
            Long id = null;
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getLong(1);
            }
            return id;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during news adding", e);
        }
    }

    /**
     * Represents the way of deleting news from database by ID.
     *
     * @param id is news' ID which will be delete.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_DELETE)) {
            ps.setLong(1, id);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during news deleting", e);
        }
    }

    /**
     * Represents the way of deleting news from database by list of ID.
     *
     * @param listId is list of news' ID which will be delete.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteById(List<Long> listId) throws DAOException {
        if (listId == null) throw new DAOException("Invalid id");
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_DELETE)) {
            for (Long id : listId) {
                ps.setLong(1, id);
                ps.addBatch();
            }
            ps.executeBatch();
            return true;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during news deleting", e);
        }
    }

    /**
     * Represents the way of updating news as an instance of
     * {@code News} in database.
     *
     * @param id   is news's ID which will be update.
     * @param news is an instance of {@code News} class that
     *             has to be updating into table.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean updateById(Long id, News news) throws DAOException {
        if (id == null || news == null) throw new DAOException("Invalid id or news");
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_UPDATE)) {
            ps.setString(1, news.getTitle());
            ps.setString(2, news.getShortText());
            ps.setString(3, news.getFullText());
            ps.setTimestamp(4, new Timestamp(new java.util.Date().getTime()));
            ps.setLong(5, id);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during news updating", e);
        }
    }

    /**
     * Gives the list of all news from database by searchCriteria.
     *
     * @param searchCriteria is an instance of {@code SearchCriteria}.
     * @return the list of news.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<News> findNews(SearchCriteria searchCriteria) throws DAOException {
        if (searchCriteria == null || searchCriteria.getAuthors() == null || searchCriteria.getTags() == null)
            throw new DAOException("Invalid searchCriteria");
        try (Connection con = dataSource.getConnection()) {
            StringBuilder query = new StringBuilder();
            query.append(SQL_FIND_ALL);
            if (searchCriteria.getAuthors().size() != 0 && searchCriteria.getTags().size() != 0) {
                query.append(SQL_WHERE).append(buildSQLByAuthor(searchCriteria.getAuthors()));
                query.append(SQL_AND).append(buildSQLByTag(searchCriteria.getTags()));
            }
            if (searchCriteria.getAuthors().size() != 0 && searchCriteria.getTags().size() == 0) {
                query.append(SQL_WHERE).append(buildSQLByAuthor(searchCriteria.getAuthors()));
            }
            if (searchCriteria.getAuthors().size() == 0 && searchCriteria.getTags().size() != 0) {
                query.append(SQL_WHERE).append(buildSQLByTag(searchCriteria.getTags()));
            }
            PreparedStatement ps = con.prepareStatement(query.toString());
            ResultSet rs = ps.executeQuery();
            return buildNewsList(rs);
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during all news taking", e);
        }
    }

    /**
     * Gives the list of all news from database by searchCriteria between two orders.
     *
     * @param searchCriteria is an instance of {@code SearchCriteria}.
     * @param firstOrder     is
     * @param lastOrder
     * @return the list of news.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<News> findListOfNews(SearchCriteria searchCriteria, Long firstOrder, Long lastOrder) throws DAOException {
        if (searchCriteria == null || searchCriteria.getAuthors() == null || searchCriteria.getTags() == null)
            throw new DAOException("Invalid searchCriteria");
        try (Connection con = dataSource.getConnection()) {
            StringBuilder query = new StringBuilder();
            query.append(SQL_FIND_SORTED_BY_NUMB_COMMENTS_AND_DATE_MODIF_PART_1);
            if (searchCriteria.getAuthors().size() != 0 && searchCriteria.getTags().size() != 0) {
                query.append(SQL_WHERE).append(buildSQLByAuthor(searchCriteria.getAuthors()));
                query.append(SQL_AND).append(buildSQLByTag(searchCriteria.getTags()));
            }
            if (searchCriteria.getAuthors().size() != 0 && searchCriteria.getTags().size() == 0) {
                query.append(SQL_WHERE).append(buildSQLByAuthor(searchCriteria.getAuthors()));
            }
            if (searchCriteria.getAuthors().size() == 0 && searchCriteria.getTags().size() != 0) {
                query.append(SQL_WHERE).append(buildSQLByTag(searchCriteria.getTags()));
            }
            query.append(SQL_FIND_SORTED_BY_NUMB_COMMENTS_AND_DATE_MODIF_PART_2);
            PreparedStatement ps = con.prepareStatement(query.toString());
            ps.setLong(1, firstOrder);
            ps.setLong(2, lastOrder);
            ResultSet rs = ps.executeQuery();
            return buildNewsList(rs);
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during all news taking", e);
        }
    }

    /**
     * Gives the next or the previous news for current news from database by searchCriteria between two orders.
     *
     * @param searchCriteria is an instance of {@code SearchCriteria}.
     * @param newsId         is id's current news.
     * @param next           is parameter which show next news (if true) or previous news (if false).
     * @return the list of news.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    public News findNexNewsByNewsId(SearchCriteria searchCriteria, Long newsId, boolean next) throws DAOException {
        if (searchCriteria == null || searchCriteria.getAuthors() == null || searchCriteria.getTags() == null)
            throw new DAOException("Invalid searchCriteria");
        try (Connection con = dataSource.getConnection()) {
            StringBuilder query = new StringBuilder();
            if (next == true) {
                query.append(SQL_FIND_NEXT_NEWS_BY_NEWS_ID_PART_1);
            } else {
                query.append(SQL_FIND_PREVIOUS_NEWS_BY_NEWS_ID_PART_1);
            }
            if (searchCriteria.getAuthors().size() != 0 && searchCriteria.getTags().size() != 0) {
                query.append(SQL_WHERE).append(buildSQLByAuthor(searchCriteria.getAuthors()));
                query.append(SQL_AND).append(buildSQLByTag(searchCriteria.getTags()));
            }
            if (searchCriteria.getAuthors().size() != 0 && searchCriteria.getTags().size() == 0) {
                query.append(SQL_WHERE).append(buildSQLByAuthor(searchCriteria.getAuthors()));
            }
            if (searchCriteria.getAuthors().size() == 0 && searchCriteria.getTags().size() != 0) {
                query.append(SQL_WHERE).append(buildSQLByTag(searchCriteria.getTags()));
            }
            query.append(SQL_FIND_NEXT_NEWS_BY_NEWS_ID_PART_2);
            PreparedStatement ps = con.prepareStatement(query.toString());
            ps.setLong(1, newsId);
            ResultSet rs = ps.executeQuery();
            News news = new News();
            if (rs.next()) {
                news.setId(rs.getLong(COLUMN_ID));
                news.setTitle(rs.getString(COLUMN_TITLE));
                news.setShortText(rs.getString(COLUMN_SHORT_TEXT));
                news.setFullText(rs.getString(COLUMN_FULL_TEXT));
                news.setCreationDate(rs.getTimestamp(COLUMN_CREATION_DATE));
                news.setModificationDate(rs.getTimestamp(COLUMN_MODIFICATION_DATE));
            }
            return news;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during all news taking", e);
        }
    }

    private List<News> buildNewsList(ResultSet rs) throws SQLException {
        List<News> result = new ArrayList<>();
        while (rs.next()) {
            News news = new News();
            news.setId(rs.getLong(COLUMN_ID));
            news.setTitle(rs.getString(COLUMN_TITLE));
            news.setShortText(rs.getString(COLUMN_SHORT_TEXT));
            news.setFullText(rs.getString(COLUMN_FULL_TEXT));
            news.setCreationDate(rs.getTimestamp(COLUMN_CREATION_DATE));
            news.setModificationDate(rs.getTimestamp(COLUMN_MODIFICATION_DATE));
            result.add(news);
        }
        return result;
    }

    private String buildSQLByTag(List<Tag> tags) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < tags.size(); i++) {
            if (i != tags.size() - 1) {
                result.append(tags.get(i).getId()).append(", ");
            } else {
                result.append(tags.get(i).getId());
            }
        }
        String query = String.format(SQL_TAG_ID_IN, result);
        return query;
    }


    private String buildSQLByAuthor(List<Author> authors) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < authors.size(); i++) {
            if (i != authors.size() - 1) {
                result.append(authors.get(i).getId()).append(", ");
            } else {
                result.append(authors.get(i).getId());
            }
        }
        return String.format(SQL_AUTHOR_ID_IN, result);
    }
}
