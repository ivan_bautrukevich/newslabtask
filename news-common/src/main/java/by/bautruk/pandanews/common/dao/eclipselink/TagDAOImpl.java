package by.bautruk.pandanews.common.dao.eclipselink;

import by.bautruk.pandanews.common.dao.TagDAO;
import by.bautruk.pandanews.common.entity.Tag;
import by.bautruk.pandanews.common.exception.DAOException;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         8/8/2016 9:14 AM.
 *         The {@code TagDAOImpl} class implements all the necessary methods
 *         which implements {@code TagDAO} interface for manipulation
 *         with tag's data in database.
 */
@Transactional
public class TagDAOImpl implements TagDAO {

    private static final String JPQL_FIND_ALL = "select t from Tag t";

    @PersistenceContext
    EntityManager entityManager;

    /**
     * Gives the list of all tags from database.
     *
     * @return the list of tags.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<Tag> takeAll() throws DAOException {
        Query query = entityManager.createQuery(JPQL_FIND_ALL);
        return (List<Tag>) query.getResultList();
    }

    /**
     * Represents the way of getting tag as an instance of
     * {@code Tag} through {@code id} value from database.
     *
     * @param id is tag's ID.
     * @return the only instance of {@code Tag} that defined just by using
     * {@code id}.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Tag takeById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        return entityManager.find(Tag.class, id);
    }

    /**
     * Represents the way of adding tag as an instance of
     * {@code Tag} from database.
     *
     * @param tag is an instance of {@code Tag} class that
     *            has to be placed into table.
     * @return ID of inserting tag
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Long insert(Tag tag) throws DAOException {
        if (tag == null) throw new DAOException("Invalid tag");
        entityManager.persist(tag);
        return tag.getId();
    }

    /**
     * Represents the way of deleting tag from database by ID.
     *
     * @param id is tag's ID which will be delete.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        Tag tag = entityManager.find(Tag.class, id);
        entityManager.remove(tag);
        return true;
    }

    /**
     * Represents the way of updating tag as an instance of
     * {@code Tag} in database.
     *
     * @param id  is tag's ID which will be update.
     * @param tag is an instance of {@code Tag} class that
     *            has to be updating into table.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean updateById(Long id, Tag tag) throws DAOException {
        if (id == null || tag == null) throw new DAOException("Invalid id or user");
        tag.setId(id);
        entityManager.merge(tag);
        return true;
    }

    /**
     * Gives the list of all tags from database by news' ID.
     *
     * @param id is news' ID.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<Tag> takeByNewsId(Long id) throws DAOException {
        return null;
    }

    /**
     * Represents the way of adding links from news to author in database.
     *
     * @param newsId is news' ID.
     * @param tagId  is tag's ID.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean createLinkWithNews(Long newsId, List<Long> tagId) throws DAOException {
        return false;
    }

    /**
     * Represents the way of deleting links from news to author in database
     * by news' ID.
     *
     * @param newsId is news' ID.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteLinksWithNewsByNewsId(List<Long> newsId) throws DAOException {
        return false;
    }

    /**
     * Represents the way of deleting links from news to author in database
     * by tag's ID.
     *
     * @param tagId is news' ID.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteLinksWithNewsByTagId(Long tagId) throws DAOException {
        return false;
    }
}
