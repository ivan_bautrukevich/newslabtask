package by.bautruk.pandanews.common.dao.hibernate;

import by.bautruk.pandanews.common.dao.RoleDAO;
import by.bautruk.pandanews.common.entity.Role;
import by.bautruk.pandanews.common.exception.DAOException;
import org.hibernate.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         7/13/2016 4:50 PM.
 *         The {@code RoleDAOImpl} class implements all the necessary methods
 *         which implements {@code RoleDAO} interface for manipulation
 *         with role's data in database.
 */
//@Transactional
public class RoleDAOImpl implements RoleDAO {

    private static final String HQL_DELETE_ROLE_BY_ID = "delete from Role where id =:roleId";
    private static final String PARAMETER_ROLE_ID = "roleId";

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Gives the list of all roles from database.
     *
     * @return the list of roles.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<Role> takeAll() throws DAOException {
        try {
            List<Role> roles = sessionFactory.getCurrentSession()
                    .createCriteria(Role.class)
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
            return roles;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during all roles taking", e);
        }
    }

    /**
     * Represents the way of getting role as an instance of
     * {@code Role} through {@code id} value from database.
     *
     * @param id is role's ID.
     * @return the only instance of {@code Role} that defined just by using
     * {@code id}.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Role takeById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        try {
            Role role = (Role) sessionFactory.getCurrentSession().get(Role.class, id);
            return role;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during role taking", e);
        }
    }

    /**
     * Represents the way of adding role as an instance of
     * {@code Role} from database.
     *
     * @param role is an instance of {@code Role} class that
     *             has to be placed into table.
     * @return ID of inserting role
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Long insert(Role role) throws DAOException {
        if (role == null) throw new DAOException("Invalid role");
        try {
            return (Long) sessionFactory.getCurrentSession().save(role);
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during role adding", e);
        }
    }

    /**
     * Represents the way of deleting role from database by ID.
     *
     * @param id is role's ID which will be delete.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        try {
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(HQL_DELETE_ROLE_BY_ID);
            query.setParameter(PARAMETER_ROLE_ID, id);
            query.executeUpdate();
            return true;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during role deleting", e);
        }
    }

    /**
     * Represents the way of updating role as an instance of
     * {@code Role} in database.
     *
     * @param id   is role's ID which will be update.
     * @param role is an instance of {@code Role} class that
     *             has to be updating into table.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean updateById(Long id, Role role) throws DAOException {
        if (id == null || role == null) throw new DAOException("Invalid id or role");
        try {
            role.setId(id);
            sessionFactory.getCurrentSession().update(role);
            return true;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during role updating", e);
        }
    }
}
