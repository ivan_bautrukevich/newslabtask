package by.bautruk.pandanews.common.dao.jdbc;

import by.bautruk.pandanews.common.dao.UserDAO;
import by.bautruk.pandanews.common.entity.Role;
import by.bautruk.pandanews.common.entity.User;
import by.bautruk.pandanews.common.exception.DAOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         4/12/2016 7:54 PM.
 *         The {@code UserDAOImpl} class implements all the necessary methods
 *         which implements {@code UserDAO} interface for manipulation
 *         with role's data in database.
 */

public class UserDAOImpl implements UserDAO {

    private final static Logger LOG = LogManager.getLogger(UserDAOImpl.class);

    @Autowired
    private DataSource dataSource;

    private static final String COLUMN_ID = "USER_ID";
    private static final String COLUMN_NAME = "USER_NAME";
    private static final String COLUMN_LOGIN = "LOGIN";
    private static final String COLUMN_PASSWORD = "PASSWORD";
    private static final String ROLE_COLUMN_ID = "ROLE_ID";
    private static final String ROLE_COLUMN_NAME = "ROLE_NAME";
    private static final String SQL_FIND_ALL = "SELECT USERS.USER_ID, USERS.USER_NAME, USERS.LOGIN, USERS.PASSWORD, " +
            "USERS.ROLE_ID, ROLES.ROLE_NAME FROM USERS JOIN ROLES ON USERS.ROLE_ID = ROLES.ROLE_ID";
    private static final String SQL_FIND_BY_ID = SQL_FIND_ALL + " WHERE USERS.USER_ID = ?";
    private static final String SQL_INSERT = "INSERT INTO USERS (USER_ID, USER_NAME, LOGIN, PASSWORD, ROLE_ID)  " +
            "VALUES (?, ?, ?, ?, ?)";
    private static final String SQL_UPDATE = "UPDATE  USERS SET USER_NAME=?, LOGIN=?, PASSWORD=?, ROLE_ID=? " +
            "WHERE USER_ID=?";
    private static final String SQL_DELETE = "DELETE FROM USERS WHERE USER_ID = ?";
    private static final String SQL_FIND_BY_LOGIN_ANS_PASSWORD = SQL_FIND_ALL +
            " WHERE USERS.LOGIN=? AND USERS.PASSWORD=?";

    /**
     * Gives the list of all users from database.
     *
     * @return the list of users.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<User> takeAll() throws DAOException {
        List<User> result = new ArrayList<>();
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_FIND_ALL)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getLong(COLUMN_ID));
                user.setName(rs.getString(COLUMN_NAME));
                user.setLogin(rs.getString(COLUMN_LOGIN));
                user.setPassword(rs.getString(COLUMN_PASSWORD));
                Role role = new Role();
                role.setId(rs.getLong(ROLE_COLUMN_ID));
                role.setName(rs.getString(ROLE_COLUMN_NAME));
                user.setRole(role);
                result.add(user);
            }
            return result;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during all users taking", e);
        }
    }

    /**
     * Represents the way of getting user as an instance of
     * {@code User} through {@code id} value from database.
     *
     * @param id is user's ID.
     * @return the only instance of {@code User} that defined just by using
     * {@code id}.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public User takeById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_FIND_BY_ID)) {
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            User user = buildUser(rs);
            return user;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during user taking", e);
        }
    }

    /**
     * Represents the way of adding user as an instance of
     * {@code User} from database.
     *
     * @param user is an instance of {@code User} class that
     *             has to be placed into table.
     * @return ID of inserting user
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Long insert(User user) throws DAOException {
        if (user == null) throw new DAOException("Invalid user");
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_INSERT, new String[]{COLUMN_ID})) {
            ps.setLong(1, user.getId());
            ps.setString(2, user.getName());
            ps.setString(3, user.getLogin());
            ps.setString(4, user.getPassword());
            ps.setLong(5, user.getRole().getId());
            ps.executeUpdate();
            Long id = null;
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getLong(1);
            }
            return id;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during user adding", e);
        }
    }

    /**
     * Represents the way of deleting user from database by ID.
     *
     * @param id is user's ID which will be delete.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_DELETE)) {
            ps.setLong(1, id);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during user deleting", e);
        }
    }

    /**
     * Represents the way of updating user as an instance of
     * {@code User} in database.
     *
     * @param id   is user's ID which will be update.
     * @param user is an instance of {@code User} class that
     *             has to be updating into table.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean updateById(Long id, User user) throws DAOException {
        if (id == null || user == null) throw new DAOException("Invalid id or user");
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_UPDATE)) {
            ps.setString(1, user.getName());
            ps.setString(2, user.getLogin());
            ps.setString(3, user.getPassword());
            ps.setLong(4, user.getRole().getId());
            ps.setLong(5, id);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during user updating", e);
        }
    }

    public User defineUser(String login, String password) throws DAOException {
        if (login == null || password == null) throw new DAOException("Invalid login or password");
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQL_FIND_BY_LOGIN_ANS_PASSWORD)) {
            ps.setString(1, login);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            User user = buildUser(rs);
            return user;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during user taking", e);
        }
    }

    private User buildUser(ResultSet rs) throws SQLException {
        User user = new User();
        if (rs.next()) {
            user.setId(rs.getLong(COLUMN_ID));
            user.setName(rs.getString(COLUMN_NAME));
            user.setLogin(rs.getString(COLUMN_LOGIN));
            user.setPassword(rs.getString(COLUMN_PASSWORD));
            Role role = new Role();
            role.setId(rs.getLong(ROLE_COLUMN_ID));
            role.setName(rs.getString(ROLE_COLUMN_NAME));
            user.setRole(role);
        }
        return user;
    }
}
