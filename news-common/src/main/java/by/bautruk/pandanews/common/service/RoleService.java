package by.bautruk.pandanews.common.service;

import by.bautruk.pandanews.common.entity.Role;

/**
 * @author ivan.bautruvkevich
 *         4/12/2016 6:57 PM.
 */
public interface RoleService extends BaseService<Role> {
}
