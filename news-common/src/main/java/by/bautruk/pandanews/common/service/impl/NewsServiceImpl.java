package by.bautruk.pandanews.common.service.impl;

import by.bautruk.pandanews.common.dao.CommentDAO;
import by.bautruk.pandanews.common.dao.NewsDAO;
import by.bautruk.pandanews.common.transferobject.SearchCriteria;
import by.bautruk.pandanews.common.exception.ServiceException;
import by.bautruk.pandanews.common.service.NewsService;
import by.bautruk.pandanews.common.dao.AuthorDAO;
import by.bautruk.pandanews.common.dao.TagDAO;
import by.bautruk.pandanews.common.entity.News;
import by.bautruk.pandanews.common.exception.DAOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         4/12/2016 8:11 PM.
 */
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = ServiceException.class)
public class NewsServiceImpl implements NewsService {

    @Autowired
    private NewsDAO newsDAO;

    @Autowired
    private CommentDAO commentDAO;

    @Autowired
    private AuthorDAO authorDAO;

    @Autowired
    private TagDAO tagDAO;

    @Override
    public List<News> takeAll() throws ServiceException {
        try {
            return newsDAO.takeAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public News takeById(Long id) throws ServiceException {
        try {
            return newsDAO.takeById(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Long insert(News news) throws ServiceException {
        try {
            return newsDAO.insert(news);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean deleteById(Long id) throws ServiceException {
        try {
            return newsDAO.deleteById(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean updateById(Long id, News news) throws ServiceException {
        try {
            return newsDAO.updateById(id, news);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<News> searchNews(SearchCriteria searchCriteria) throws ServiceException {
        try {
            return newsDAO.findNews(searchCriteria);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Integer countNews(SearchCriteria searchCriteria) throws ServiceException {
        try {
            return newsDAO.countNews(searchCriteria);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<News> findListIdOfNews(SearchCriteria searchCriteria,
                                       Long firstOrder, Long lastOrder) throws ServiceException {
        try {
            return newsDAO.findListOfNews(searchCriteria, firstOrder, lastOrder);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean deleteByListOfId(List<Long> listOfNewsId) throws ServiceException {
        try {
            authorDAO.deleteLinksWithNewsById(listOfNewsId);
            tagDAO.deleteLinksWithNewsByNewsId(listOfNewsId);
            commentDAO.deleteByNewsId(listOfNewsId);
            newsDAO.deleteById(listOfNewsId);
            return true;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public News findNexNewsByNewsId(SearchCriteria searchCriteria, Long newsId, boolean next) throws ServiceException{
        try {
            News news = newsDAO.findNexNewsByNewsId(searchCriteria, newsId, next);
            return news;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
