package by.bautruk.pandanews.common.dao.hibernate;

import by.bautruk.pandanews.common.dao.AuthorDAO;
import by.bautruk.pandanews.common.entity.Author;
import by.bautruk.pandanews.common.exception.DAOException;
import org.hibernate.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         7/13/2016 4:48 PM.
 *         The {@code AuthorDAOImpl} class implements all the necessary methods
 *         which implements {@code AuthorDAO} interface for manipulation
 *         with author's data in database.
 */
//@Transactional(propagation = Propagation.MANDATORY)
public class AuthorDAOImpl implements AuthorDAO {

    private static final String HQL_DELETE_BY_ID = "delete from Author where id =:authorId";
    private static final String HQL_TAKE_BY_NEWS_ID = "select a from Author a join a.newsSet n where n.id =:newsId";
    private static final String HQL_TAKE_NOT_EXPIRED = "from Author where expired is null";
    private static final String HQL_MAKE_EXPIRED = "update Author set expired=:newDate where id=:authorId";
    private static final String SQL_CREATE_LINK_NEWS = "INSERT INTO NEWS_AUTHORS (NEWS_ID, AUTHOR_ID) VALUES (?, ?)";
    private static final String SQL_DELETE_LINK_NEWS = "DELETE FROM NEWS_AUTHORS WHERE NEWS_ID=?";

    private static final String PARAMETER_AUTHOR_ID = "authorId";
    private static final String PARAMETER_NEWS_ID = "newsId";
    private static final String PARAMETER_DATE = "newDate";

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Gives the list of all authors from database.
     *
     * @return the list of authors.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<Author> takeAll() throws DAOException {
        try {
            List<Author> authors = sessionFactory.getCurrentSession()
                    .createCriteria(Author.class)
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
            return authors;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during all authors taking", e);
        }
    }

    /**
     * Represents the way of getting author as an instance of
     * {@code Author} through {@code id} value from database.
     *
     * @param id is author's ID.
     * @return the only instance of {@code Author} that defined just by using
     * {@code id}.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Author takeById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        try {
            return (Author) sessionFactory.getCurrentSession().get(Author.class, id);
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during author taking", e);
        }
    }

    /**
     * Represents the way of adding author as an instance of
     * {@code Author} from database.
     *
     * @param author is an instance of {@code Author} class that
     *               has to be placed into table.
     * @return ID of inserting author
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Long insert(Author author) throws DAOException {
        if (author == null) throw new DAOException("Invalid author");
        try {
            return (Long) sessionFactory.getCurrentSession().save(author);
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during author adding", e);
        }
    }

    /**
     * Represents the way of deleting author from database by ID.
     *
     * @param id is author's ID which will be delete.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        try {
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(HQL_DELETE_BY_ID);
            query.setParameter(PARAMETER_AUTHOR_ID, id);
            query.executeUpdate();
            return true;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during author deleting", e);
        }
    }

    /**
     * Represents the way of updating author as an instance of
     * {@code Author} in database.
     *
     * @param id     is author's ID which will be update.
     * @param author is an instance of {@code Author} class that
     *               has to be updating into table.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean updateById(Long id, Author author) throws DAOException {
        if (id == null || author == null) throw new DAOException("Invalid id or author");
        try {
            author.setId(id);
            sessionFactory.getCurrentSession().update(author);
            return true;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during author updating", e);
        }
    }

    /**
     * Gives the list of all authors from database by News' ID.
     *
     * @param id is news' ID.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<Author> takeByNewsId(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        try {
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(HQL_TAKE_BY_NEWS_ID);
            query.setParameter(PARAMETER_NEWS_ID, id);
            List<Author> list = query.list();
            return list;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during all authors taking", e);
        }
    }

    /**
     * Represents the way of adding links from news to author in database.
     *
     * @param newsId   is news' ID.
     * @param authorId is author's ID.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean createLinkWithNews(Long newsId, List<Long> authorId) throws DAOException {
        if (newsId == null || authorId == null) throw new DAOException("Invalid newsId or authorId");
        try {
            Session session = sessionFactory.getCurrentSession();
            for (Long id : authorId) {
                session.createSQLQuery(SQL_CREATE_LINK_NEWS)
                        .setParameter(0, newsId)
                        .setParameter(1, id)
                        .executeUpdate();
            }
            session.flush();
            return true;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during link adding", e);
        }
    }

    /**
     * Represents the way of deleting links from news to author in database.
     *
     * @param newsId is list of news' ID.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteLinksWithNewsById(List<Long> newsId) throws DAOException {
        if (newsId == null) throw new DAOException("Invalid newsId");
        try {
            Session session = sessionFactory.getCurrentSession();
            for (Long id : newsId) {
                Query query = session.createSQLQuery(SQL_DELETE_LINK_NEWS);
                query.setParameter(0, id);
                query.executeUpdate();
            }
            session.flush();
            return true;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during link deleting", e);
        }
    }

    /**
     * Gives the list of all authors from database which don't have expired date.
     *
     * @return the list of authors.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<Author> takeAllNotExpired() throws DAOException {
        try {
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(HQL_TAKE_NOT_EXPIRED);
            List<Author> list = query.list();
            return list;
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during all authors taking", e);
        }
    }

    /**
     * Represents the way of updating author as an instance of
     * {@code Author} in database.
     *
     * @param id is author's ID which will be expired.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean updateAuthorExpired(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id or author");
        try {
            Session session = sessionFactory.getCurrentSession();
            Author author = (Author) session.get(Author.class, id);
            if (author != null) {
                author.setExpired(new Timestamp(new java.util.Date().getTime()));
                session.merge(author);
                session.flush();
                return true;
            }
            throw new DAOException("Invalid id or author");
        } catch (HibernateException e) {
            throw new DAOException("Invalid database operation occurred during author updating", e);
        }
    }
}
