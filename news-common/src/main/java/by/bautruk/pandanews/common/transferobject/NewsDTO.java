package by.bautruk.pandanews.common.transferobject;

import by.bautruk.pandanews.common.entity.Author;
import by.bautruk.pandanews.common.entity.Comment;
import by.bautruk.pandanews.common.entity.News;
import by.bautruk.pandanews.common.entity.Tag;

import java.io.Serializable;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         4/18/2016 7:15 PM.
 *         The {@code NewsDTO} class is a data trasnsfer object class which includes all
 *         the necessary getters and setters for its fields and also overrides
 *         {@code equals()}, {@code hashCode()} and {@code toString()}.
 */

public class NewsDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private News news;
    private List<Author> authors;
    private List<Comment> comments;
    private List<Tag> tags;

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NewsDTO)) return false;

        NewsDTO newsDTO = (NewsDTO) o;

        if (news != null ? !news.equals(newsDTO.news) : newsDTO.news != null) return false;
        if (authors != null ? !authors.equals(newsDTO.authors) : newsDTO.authors != null) return false;
        if (comments != null ? !comments.equals(newsDTO.comments) : newsDTO.comments != null) return false;
        return tags != null ? tags.equals(newsDTO.tags) : newsDTO.tags == null;

    }

    @Override
    public int hashCode() {
        int result = news != null ? news.hashCode() : 0;
        result = 31 * result + (authors != null ? authors.hashCode() : 0);
        result = 31 * result + (comments != null ? comments.hashCode() : 0);
        result = 31 * result + (tags != null ? tags.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "NewsDTO{" +
                "news=" + news +
                ", authors=" + authors +
                ", comments=" + comments +
                ", tags=" + tags +
                '}';
    }
}
