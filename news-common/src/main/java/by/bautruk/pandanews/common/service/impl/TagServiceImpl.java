package by.bautruk.pandanews.common.service.impl;

import by.bautruk.pandanews.common.dao.TagDAO;
import by.bautruk.pandanews.common.entity.Tag;
import by.bautruk.pandanews.common.exception.DAOException;
import by.bautruk.pandanews.common.exception.ServiceException;
import by.bautruk.pandanews.common.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         4/12/2016 8:16 PM.
 */
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = ServiceException.class)
public class TagServiceImpl implements TagService {

    @Autowired
    private TagDAO tagDAO;

    @Override
    public List<Tag> takeAll() throws ServiceException {
        try {
            return tagDAO.takeAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Tag takeById(Long id) throws ServiceException {
        try {
            return tagDAO.takeById(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Long insert(Tag tag) throws ServiceException {
        try {
            return tagDAO.insert(tag);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean deleteById(Long id) throws ServiceException {
        try {
            return tagDAO.deleteById(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean updateById(Long id, Tag tag) throws ServiceException {
        try {
            return tagDAO.updateById(id, tag);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Tag> takeByNewsId(Long id) throws ServiceException {
        try {
            return tagDAO.takeByNewsId(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean createLinkWithNews(Long newsId, List<Long> tagId) throws ServiceException {
        try {
            return tagDAO.createLinkWithNews(newsId, tagId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }


    @Override
    public boolean deleteLinksWithNewsByNewsId(List<Long> newsId) throws ServiceException {
        try {
            return tagDAO.deleteLinksWithNewsByNewsId(newsId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean deleteLinksWithNewsByTagId(Long tagId) throws ServiceException {
        try {
            return tagDAO.deleteLinksWithNewsByTagId(tagId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
