package by.bautruk.pandanews.common.dao;

import by.bautruk.pandanews.common.entity.Tag;
import by.bautruk.pandanews.common.exception.DAOException;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         4/12/2016 7:18 PM.
 */
public interface TagDAO extends BaseDAO<Tag> {
    List<Tag> takeByNewsId(Long id) throws DAOException;

    boolean createLinkWithNews(Long newsId, List<Long> tagId) throws DAOException;

    boolean deleteLinksWithNewsByNewsId(List<Long> newsId) throws DAOException;

    boolean deleteLinksWithNewsByTagId(Long tagId) throws DAOException;
}
