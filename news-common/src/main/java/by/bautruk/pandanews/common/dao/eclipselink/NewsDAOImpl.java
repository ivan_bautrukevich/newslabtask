package by.bautruk.pandanews.common.dao.eclipselink;

import by.bautruk.pandanews.common.dao.NewsDAO;
import by.bautruk.pandanews.common.entity.Author;
import by.bautruk.pandanews.common.entity.News;
import by.bautruk.pandanews.common.entity.Tag;
import by.bautruk.pandanews.common.exception.DAOException;
import by.bautruk.pandanews.common.transferobject.SearchCriteria;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.sql.Timestamp;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         8/8/2016 9:13 AM.
 *         The {@code NewsDAOImpl} class implements all the necessary methods
 *         which implements {@code NewsDAO} interface for manipulation
 *         with news' data in database.
 */
@Transactional
public class NewsDAOImpl implements NewsDAO {

    @PersistenceContext
    EntityManager entityManager;

    private static final String JPQL_FIND_ALL = "select n from News n";
    private static final String SQL_TAG_ID_IN = " NEWS_TAGS.TAG_ID IN (%s)";

    private static final String SQL_AUTHOR_ID_IN = " NEWS_AUTHORS.AUTHOR_ID IN (%s)";

    /**
     * Gives the list of all news from database.
     *
     * @return the list of news.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<News> takeAll() throws DAOException {
        Query query = entityManager.createQuery(JPQL_FIND_ALL);
        return (List<News>) query.getResultList();
    }

    /**
     * Represents the way of getting news as an instance of
     * {@code News} through {@code id} value from database.
     *
     * @param id is news' ID.
     * @return the only instance of {@code News} that defined just by using
     * {@code id}.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public News takeById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        return entityManager.find(News.class, id);
    }

    /**
     * Represents the way of adding news as an instance of
     * {@code News} from database.
     *
     * @param news is an instance of {@code News} class that
     *             has to be placed into table.
     * @return ID of inserting news
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Long insert(News news) throws DAOException {
        if (news == null) throw new DAOException("Invalid news");
        entityManager.persist(news);
        return news.getId();
    }

    /**
     * Represents the way of deleting news from database by ID.
     *
     * @param id is news' ID which will be delete.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteById(Long id) throws DAOException {
        if (id == null) throw new DAOException("Invalid id");
        News news = entityManager.find(News.class, id);
        entityManager.remove(news);
        return true;
    }

    /**
     * Represents the way of updating news as an instance of
     * {@code News} in database.
     *
     * @param id   is news's ID which will be update.
     * @param news is an instance of {@code News} class that
     *             has to be updating into table.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean updateById(Long id, News news) throws DAOException {
        if (id == null || news == null) throw new DAOException("Invalid id or news");
        news.setId(id);
        news.setModificationDate(new Timestamp(new java.util.Date().getTime()));
        entityManager.merge(news);
        return true;
    }

    /**
     * Represents the way of deleting news from database by list of ID.
     *
     * @param listId is list of news' ID which will be delete.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public boolean deleteById(List<Long> listId) throws DAOException {
        return false;
    }

    /**
     * Gives the list of all news from database.
     *
     * @param searchCriteria is an instance of {@code SearchCriteria}.
     * @return the list of news.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<News> findNews(SearchCriteria searchCriteria) throws DAOException {
        return null;
    }

    /**
     * Represents the way of getting count of news.
     *
     * @return the only instance of {@code Integer} that defined just by using
     * {@code id}.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Integer countNews(SearchCriteria searchCriteria) throws DAOException {
        return null;
    }

    /**
     * Gives the list of all news from database by searchCriteria between two orders.
     *
     * @param searchCriteria is an instance of {@code SearchCriteria}.
     * @param firstOrder     is
     * @param lastOrder
     * @return the list of news.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<News> findListOfNews(SearchCriteria searchCriteria, Long firstOrder, Long lastOrder) throws DAOException {
        return null;
    }

    /**
     * Gives the next or the previous news for current news from database by searchCriteria between two orders.
     *
     * @param searchCriteria is an instance of {@code SearchCriteria}.
     * @param newsId         is id's current news.
     * @param next           is parameter which show next news (if true) or previous news (if false).
     * @return the list of news.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public News findNexNewsByNewsId(SearchCriteria searchCriteria, Long newsId, boolean next) throws DAOException {
        return null;
    }

    private String buildSQLByTag(List<Tag> tags) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < tags.size(); i++) {
            if (i != tags.size() - 1) {
                result.append(tags.get(i).getId()).append(", ");
            } else {
                result.append(tags.get(i).getId());
            }
        }
        String query = String.format(SQL_TAG_ID_IN, result);
        return query;
    }


    private String buildSQLByAuthor(List<Author> authors) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < authors.size(); i++) {
            if (i != authors.size() - 1) {
                result.append(authors.get(i).getId()).append(", ");
            } else {
                result.append(authors.get(i).getId());
            }
        }
        return String.format(SQL_AUTHOR_ID_IN, result);
    }
}
