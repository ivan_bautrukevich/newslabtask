package by.bautruk.pandanews.common.service;

import by.bautruk.pandanews.common.transferobject.NewsDTO;
import by.bautruk.pandanews.common.transferobject.NewsTO;
import by.bautruk.pandanews.common.transferobject.SearchCriteria;
import by.bautruk.pandanews.common.exception.ServiceException;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         4/19/2016 11:35 AM.
 */
public interface NewsDTOService {

    Long addNewsTO(NewsTO newsTO) throws ServiceException;

    NewsDTO takeNews(Long id) throws ServiceException;

    boolean updateNewsTO(NewsTO newsTO) throws ServiceException;

    List<NewsDTO> findNewsByOrder(SearchCriteria searchCriteria,
                                  Long firstOrder, Long lastOrder) throws ServiceException;

    NewsDTO findNexNewsByNewsId(SearchCriteria searchCriteria, Long newsId, boolean next) throws ServiceException;
}
