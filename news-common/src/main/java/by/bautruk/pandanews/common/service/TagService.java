package by.bautruk.pandanews.common.service;

import by.bautruk.pandanews.common.entity.Tag;
import by.bautruk.pandanews.common.exception.ServiceException;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         4/12/2016 8:09 PM.
 */
public interface TagService extends BaseService<Tag> {
    List<Tag> takeByNewsId(Long id) throws ServiceException;

    boolean createLinkWithNews(Long newsId, List<Long> tagId) throws ServiceException;

    boolean deleteLinksWithNewsByNewsId(List<Long> newsId) throws ServiceException;

    boolean deleteLinksWithNewsByTagId(Long tagId) throws ServiceException;
}
