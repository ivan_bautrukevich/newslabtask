package by.bautruk.pandanews.common.dao;

import by.bautruk.pandanews.common.entity.News;
import by.bautruk.pandanews.common.transferobject.SearchCriteria;
import by.bautruk.pandanews.common.exception.DAOException;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         4/12/2016 7:15 PM.
 */
public interface NewsDAO extends BaseDAO<News> {

    boolean deleteById(List<Long> id) throws DAOException;

    List<News> findNews(SearchCriteria searchCriteria) throws DAOException;

    Integer countNews(SearchCriteria searchCriteria) throws DAOException;

    List<News> findListOfNews(SearchCriteria searchCriteria, Long firstOrder, Long lastOrder) throws DAOException;

    News findNexNewsByNewsId(SearchCriteria searchCriteria, Long newsId, boolean next) throws DAOException;
}
