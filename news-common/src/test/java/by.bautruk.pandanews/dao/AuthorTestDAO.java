package by.bautruk.pandanews.dao;

import by.bautruk.pandanews.common.dao.AuthorDAO;
import by.bautruk.pandanews.common.entity.Author;
import by.bautruk.pandanews.common.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author ivan.bautruvkevich
 *         4/18/2016 6:11 PM.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-spring-context.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@DatabaseSetup(value = "classpath:test-data.xml")
@DatabaseTearDown(value = "classpath:test-data.xml", type = DatabaseOperation.DELETE_ALL)
@Transactional
public class AuthorTestDAO {

    private final static Logger LOG = LogManager.getLogger(AuthorTestDAO.class);

    @Autowired
    private AuthorDAO authorDAO;

    @Test
    @Transactional
    public void takeAll() throws DAOException {
        List<Author> authors = authorDAO.takeAll();
        assertEquals(4, authors.size());
    }

    @Test
    public void takeById() throws DAOException {
        Author author = authorDAO.takeById(1L);
        assertEquals("Иванов", author.getName());
    }

    @Test(expected = DAOException.class)
    public void takeByIdException() throws DAOException {
        authorDAO.takeById(null);
    }

    @Test
    public void insert() throws DAOException {
        Author author = new Author();
        author.setName("Test");
        author.setExpired(new Timestamp(new java.util.Date().getTime()));
        Long id = authorDAO.insert(author);
        assertNotNull(id);
    }

    @Test
    public void updateById() throws DAOException {
        Author author = new Author();
        author.setName("Test");
        author.setExpired(new Timestamp(new java.util.Date().getTime()));
        boolean test = authorDAO.updateById(2L, author);
        assertTrue(test);
    }

    @Test
    public void takeByNewsId() throws DAOException {
        List<Author> authors = authorDAO.takeByNewsId(1L);
        assertEquals(authors.size(), 1);
    }

    @Test
    public void createLink() throws DAOException {
        List<Long> listId = new ArrayList<>();
        listId.add(1L);
        authorDAO.createLinkWithNews(3L, listId);
    }

    @Test(expected = DAOException.class)
    public void createLinkDAOException() throws DAOException {
        List<Long> listId = new ArrayList<>();
        listId.add(1L);
        authorDAO.createLinkWithNews(5L, listId);
    }

    @Test
    public void deleteLink() throws DAOException {
        authorDAO.deleteLinksWithNewsById(Arrays.asList(1L));
    }

    @Test
    public void updateLink() throws DAOException {
        List<Long> listId = new ArrayList<>();
        listId.add(1L);
        authorDAO.createLinkWithNews(3L, listId);
    }

    @Test
    public void updateAuthorExpired() throws DAOException {
        authorDAO.updateAuthorExpired(20L);
        Author author = authorDAO.takeById(20L);
        LOG.debug(author);
        assertNotNull(author.getExpired());
    }
}
