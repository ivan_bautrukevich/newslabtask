package by.bautruk.pandanews.dao;

import by.bautruk.pandanews.common.dao.TagDAO;
import by.bautruk.pandanews.common.dao.jdbc.TagDAOImpl;
import by.bautruk.pandanews.common.entity.Tag;
import by.bautruk.pandanews.common.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author ivan.bautruvkevich
 *         4/18/2016 9:39 AM.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-spring-context.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@DatabaseSetup(value = "classpath:test-data.xml")
@DatabaseTearDown(value = "classpath:test-data.xml", type = DatabaseOperation.DELETE_ALL)
@Transactional
public class TagTestDAO {

    private final static Logger LOG = LogManager.getLogger(TagDAOImpl.class);

    @Autowired
    private TagDAO tagDAO;

    @Test
    public void takeAll() throws DAOException {
        List<Tag> tags = tagDAO.takeAll();
        assertEquals(tags.size(), 3);
    }

    @Test
    public void insert() throws DAOException {
        Tag tag = new Tag();
        tag.setId(4L);
        tag.setName("history");
        Long id = tagDAO.insert(tag);
        assertNotNull(id);
    }

    @Test
    public void takeById() throws DAOException {
        Tag tag = tagDAO.takeById((long) 1);
        assertEquals("sport", tag.getName());
    }

    @Test
    public void deleteById() throws DAOException {
        List<Tag> tagsBefore = tagDAO.takeAll();
        Tag tag = new Tag();
        tag.setId(4L);
        tag.setName("history");
        Long id = tagDAO.insert(tag);
        boolean bool = tagDAO.deleteById(id);
        List<Tag> tagsAfter = tagDAO.takeAll();
        assertEquals(true, bool);
        assertEquals(tagsBefore.size(), tagsAfter.size());
    }

    @Test
    public void updateById() throws DAOException {
        Tag tag = new Tag();
        tag.setName("test");
        boolean test = tagDAO.updateById(2L, tag);
        assertTrue(test);
    }

    @Test
    public void takeByNewsId() throws DAOException {
        List<Tag> tags = tagDAO.takeByNewsId(1L);
        assertEquals(2, tags.size());
    }

    @Test
    public void createLink() throws DAOException {
        List<Long> listId = new ArrayList<>();
        listId.add(1L);
        tagDAO.createLinkWithNews(3L, listId); //// TODO: 4/22/2016 make more logical test
    }

    @Test
    public void updateLink() throws DAOException {
        List<Long> listId = new ArrayList<>();
        listId.add(1L);
        tagDAO.createLinkWithNews(3L, listId); //// TODO: 4/22/2016 make more logical test
    }
}
