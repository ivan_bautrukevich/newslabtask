package by.bautruk.pandanews.dao;

import by.bautruk.pandanews.common.dao.RoleDAO;
import by.bautruk.pandanews.common.dao.jdbc.TagDAOImpl;
import by.bautruk.pandanews.common.entity.Role;
import by.bautruk.pandanews.common.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * @author ivan.bautruvkevich
 *         4/14/2016 6:01 PM.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-spring-context.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@DatabaseSetup(value = "classpath:test-data.xml")
@DatabaseTearDown(value = "classpath:test-data.xml", type = DatabaseOperation.DELETE_ALL)
@Transactional
public class RoleTestDAO {

    private final static Logger LOG = LogManager.getLogger(TagDAOImpl.class);

    @Autowired
    private RoleDAO roleDAO;


    @Test
    public void takeAll() throws DAOException {
        List<Role> roles = roleDAO.takeAll();
        assertEquals(2, roles.size());
    }

    @Test
    public void takeById() throws DAOException {
        Role role = roleDAO.takeById(1L);
        assertEquals("admin", role.getName());
    }

    @Test
    public void insert() throws DAOException {
        Role role = new Role();
        role.setId(3L);
        role.setName("super");
        Long id = roleDAO.insert(role);
        assertEquals(role.getId(), id);
    }

    @Test(expected = DAOException.class)
    public void takeByIdNullException() throws DAOException {
        roleDAO.takeById(null);
    }

    @Test
    public void takeByIdNull() throws DAOException {
        Role role = roleDAO.takeById(100L);
        assertEquals(null, role);
    }

    @Test
    public void updateById() throws DAOException {
        Role role = new Role();
        role.setName("test");
        boolean test = roleDAO.updateById(2L, role);
        assertTrue(test);
    }

    @Test
    public void delete() throws DAOException {
        Role role = new Role();
        role.setId(3L);
        role.setName("super");
        Long id = roleDAO.insert(role);
        boolean resualt = roleDAO.deleteById(id);
        assertEquals(true, resualt);
    }
}

