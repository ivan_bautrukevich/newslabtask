package by.bautruk.pandanews.dao;

import by.bautruk.pandanews.common.dao.CommentDAO;
import by.bautruk.pandanews.common.entity.Comment;
import by.bautruk.pandanews.common.entity.News;
import by.bautruk.pandanews.common.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author ivan.bautruvkevich
 *         4/18/2016 11:23 AM.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-spring-context.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@DatabaseSetup(value = "classpath:test-data.xml")
@DatabaseTearDown(value = "classpath:test-data.xml", type = DatabaseOperation.DELETE_ALL)
@Transactional
public class CommentTestDAO {

    private final static Logger LOG = LogManager.getLogger(CommentTestDAO.class);

    @Autowired
    private CommentDAO commentDAO;

    @Test
    public void takeAll() throws DAOException {
        List<Comment> comments = commentDAO.takeAll();
        assertEquals(4, comments.size());
    }

    @Test
    public void insert() throws DAOException {
        Comment comment = new Comment();
        comment.setNewsId(1L);
        comment.setCommentText("text");
        comment.setCreationDate(new Timestamp(new java.util.Date().getTime()));
        Long id = commentDAO.insert(comment);
        assertNotNull(id);
    }

    @Test(expected = DAOException.class)
    public void insertWithNull() throws DAOException {
        Comment comment = null;
        commentDAO.insert(comment);
    }

    @Test
    public void delete() throws DAOException {
        commentDAO.deleteById(1L);
    }

    @Test
    public void takeById() throws DAOException {
        Comment comment = commentDAO.takeById(1L);
        assertEquals(comment.getCommentText(), "super");
    }

    @Test
    public void updateById() throws DAOException {
        Comment comment = new Comment();
        comment.setNewsId(3L);
        comment.setCommentText("text");
        comment.setCreationDate(new Timestamp(new java.util.Date().getTime()));
        boolean test = commentDAO.updateById(3L, comment);
        assertTrue(test);
    }

    @Test
    public void takeCommentByNews() throws DAOException {
        News news = new News();
        news.setId(1L);
        List<Comment> comment = commentDAO.takeCommentByNews(news);
        assertEquals(2, comment.size());
    }
}
