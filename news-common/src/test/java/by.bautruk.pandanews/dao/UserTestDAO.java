package by.bautruk.pandanews.dao;

import by.bautruk.pandanews.common.dao.UserDAO;
import by.bautruk.pandanews.common.entity.Role;
import by.bautruk.pandanews.common.entity.User;
import by.bautruk.pandanews.common.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author ivan.bautruvkevich
 *         4/18/2016 1:45 PM.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-spring-context.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@DatabaseSetup(value = "classpath:test-data.xml")
@DatabaseTearDown(value = "classpath:test-data.xml", type = DatabaseOperation.DELETE_ALL)
@Transactional
public class UserTestDAO {

    private final static Logger LOG = LogManager.getLogger(UserTestDAO.class);

    @Autowired
    private UserDAO userDAO;

    @Test
    public void takeAll() throws DAOException {
        List<User> tags = userDAO.takeAll();
        assertEquals(3, tags.size());
    }

    @Test
    public void takeById() throws DAOException {
        User user = userDAO.takeById(1L);
        assertEquals("Ivan", user.getName());
    }

    @Test
    public void insert() throws DAOException {
        User user = new User();
        user.setId(5L);
        user.setName("Test");
        user.setLogin("test");
        user.setPassword("test");
        Role role = new Role();
        role.setId(1L);
        user.setRole(role);
        Long id = userDAO.insert(user);
        assertEquals(id, user.getId());
    }

    @Test
    public void updateById() throws DAOException {
        User user = new User();
        user.setName("Test");
        user.setLogin("test");
        user.setPassword("test");
        Role role = new Role();
        role.setId(1L);
        user.setRole(role);
        boolean test = userDAO.updateById(3L, user);
        assertTrue(test);
    }

    @Test
    public void defineUser() throws DAOException {
        User user = userDAO.defineUser("ivan", "ivan");
        assertEquals(new Long(1), user.getId());
    }
}
