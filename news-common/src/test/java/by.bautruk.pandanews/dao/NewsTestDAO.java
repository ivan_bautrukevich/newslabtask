package by.bautruk.pandanews.dao;

import by.bautruk.pandanews.common.dao.NewsDAO;
import by.bautruk.pandanews.common.entity.News;
import by.bautruk.pandanews.common.exception.DAOException;
import by.bautruk.pandanews.common.transferobject.SearchCriteria;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author ivan.bautruvkevich
 *         4/18/2016 7:29 PM.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-spring-context.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@DatabaseSetup(value = "classpath:test-data.xml")
@DatabaseTearDown(value = "classpath:test-data.xml", type = DatabaseOperation.DELETE_ALL)
@Transactional
public class NewsTestDAO {

    private final static Logger LOG = LogManager.getLogger(NewsTestDAO.class);

    @Autowired
    private NewsDAO newsDAO;

    @Test
    public void takeAll() throws DAOException {
        List<News> news = newsDAO.takeAll();
        assertEquals(3, news.size());
    }

    @Test
    public void insert() throws DAOException {
        News news = new News();
        news.setTitle("text");
        news.setShortText("test");
        news.setFullText("test");
        news.setCreationDate(new Timestamp(new java.util.Date().getTime()));
        news.setModificationDate(new Timestamp(new java.util.Date().getTime()));
        Long id = newsDAO.insert(news);
        assertNotNull(id);
    }


    @Test
    public void delete() throws DAOException {
        News news = new News();
        news.setTitle("text");
        news.setShortText("test");
        news.setFullText("test");
        news.setCreationDate(new Timestamp(new java.util.Date().getTime()));
        news.setModificationDate(new Timestamp(new java.util.Date().getTime()));
        Long id = newsDAO.insert(news);
        newsDAO.deleteById(id);
        assertNotNull(id);
    }

    @Test
    public void takeById() throws DAOException {
        News news = newsDAO.takeById(1L);
        assertEquals("Winner", news.getTitle());
    }

    @Test
    public void updateById() throws DAOException {
        News news = newsDAO.takeById(3L);
        news.setTitle("text");
        news.setCreationDate(null);
        news.setModificationDate(null);
        boolean test = newsDAO.updateById(3L, news);
        assertTrue(test);
    }

//    @Test
//    public void searchNewsWithTagAndAuthor() throws DAOException {
//        SearchCriteria searchCriteria = new SearchCriteria();
//        List<Tag> tags = new ArrayList<>();
//        Tag tag = new Tag();
//        tag.setId(1L);
//        tags.add(tag);
//        Tag tagSecond = new Tag();
//        tag.setId(2L);
//        tags.add(tagSecond);
//        searchCriteria.setTags(tags);
//        Author author = new Author();
//        author.setId(1L);
//        List<Author> authors = new ArrayList<>();
//        authors.add(author);
//        searchCriteria.setAuthors(authors);
//        List<News> news = newsDAO.findNews(searchCriteria);
//    }

//    @Test
//    public void searchNewsWithTag() throws DAOException {
//        SearchCriteria searchCriteria = new SearchCriteria();
//        List<Tag> tags = new ArrayList<>();
//        Tag tag = new Tag();
//        tag.setId(1L);
//        tags.add(tag);
//        searchCriteria.setTags(tags);
//        List<Author> authors = new ArrayList<>();
//        searchCriteria.setAuthors(authors);
//        List<News> news = newsDAO.findNews(searchCriteria);
//    }

//    @Test
//    public void searchNewsWithAuthor() throws DAOException {
//        SearchCriteria searchCriteria = new SearchCriteria();
//        Author author = new Author();
//        author.setId(1L);
//        List<Author> authors = new ArrayList<>();
//        authors.add(author);
//        searchCriteria.setAuthors(authors);
//        List<Tag> tags = new ArrayList<>();
//        searchCriteria.setTags(tags);
//        List<News> news = newsDAO.findNews(searchCriteria);
//    }

//    @Test
//    public void searchNews() throws DAOException {
//        SearchCriteria searchCriteria = new SearchCriteria();
//        List<Author> authors = new ArrayList<>();
//        searchCriteria.setAuthors(authors);
//        List<Tag> tags = new ArrayList<>();
//        searchCriteria.setTags(tags);
//        List<News> news = newsDAO.findNews(searchCriteria);
//    }

    @Test(expected = DAOException.class)
    public void searchNewsDAOException() throws DAOException {
        SearchCriteria searchCriteria = new SearchCriteria();
        newsDAO.findNews(searchCriteria);
    }

    @Test(expected = DAOException.class)
    public void searchNewsWithNull() throws DAOException {
        newsDAO.findNews(null);
    }

    @Test
    public void countNews() throws DAOException {
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setTags(new ArrayList<>());
        searchCriteria.setAuthors(new ArrayList<>());
        int count = newsDAO.countNews(searchCriteria);
        assertEquals(3, count);
    }

    @Test
    public void findListOfNews() throws DAOException {
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setTags(new ArrayList<>());
        searchCriteria.setAuthors(new ArrayList<>());
        List<News> news = newsDAO.findListOfNews(searchCriteria, 1L, 3L);
        assertEquals(3, news.size());
    }

//    @Test
//    public void findNexNewsByNewsId() throws DAOException {
//        SearchCriteria searchCriteria = new SearchCriteria();
//        searchCriteria.setTags(new ArrayList<>());
//        searchCriteria.setAuthors(new ArrayList<>());
//        News news = newsDAO.findNexNewsByNewsId(searchCriteria, 1L, true);
//        assertEquals(new Long(3L), news.getId());
//    }
}
