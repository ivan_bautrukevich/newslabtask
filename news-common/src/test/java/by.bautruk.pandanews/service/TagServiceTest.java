package by.bautruk.pandanews.service;

import by.bautruk.pandanews.common.dao.TagDAO;
import by.bautruk.pandanews.common.entity.Tag;
import by.bautruk.pandanews.common.exception.DAOException;
import by.bautruk.pandanews.common.exception.ServiceException;
import by.bautruk.pandanews.common.service.impl.TagServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * @author ivan.bautruvkevich
 *         4/21/2016 8:57 AM.
 */
public class TagServiceTest {

    @InjectMocks
    TagServiceImpl tagService;

    @Mock
    TagDAO tagDAO;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void takeById() throws DAOException, ServiceException {
        Long id = 1L;
        Tag tag = new Tag();
        when(tagDAO.takeById(id)).thenReturn(tag);
        Tag tagTest = tagService.takeById(id);
        assertEquals(tag.getId(), tagTest.getId());
    }

    @Test
    public void takeAll() throws DAOException, ServiceException {
        List<Tag> tags = new ArrayList<>();
        tags.add(new Tag());
        tags.add(new Tag());
        when(tagDAO.takeAll()).thenReturn(tags);
        List<Tag> tagsTest = tagService.takeAll();
        assertEquals(tags.size(), tagsTest.size());
    }

    @Test
    public void insert() throws ServiceException, DAOException {
        Tag tag = new Tag();
        tag.setId(1L);
        tag.setName("test");
        when(tagDAO.insert(tag)).thenReturn(1L);
        Long id = tagService.insert(tag);
        assertEquals(id, tag.getId());
    }

    @Test
    public void deleteById() throws DAOException {
        //// TODO: 4/25/2016 create test
    }

    @Test
    public void updateById() throws DAOException, ServiceException {
        Long id = 1L;
        Tag tag = new Tag();
        tag.setId(id);
        when(tagDAO.updateById(id, tag)).thenReturn(true);
        boolean result = tagService.updateById(id, tag);
        assertEquals(result, true);
    }

    @Test
    public void takeByNewsId() throws DAOException, ServiceException {
        //// TODO: 4/25/2016 create test
    }


    @Test
    public void createLinkWithNews() throws DAOException, ServiceException{
        //// TODO: 4/25/2016 create test
    }


    @Test
    public void deleteLinksWithNewsById() throws DAOException, ServiceException{
        //// TODO: 4/25/2016 create test
    }
}
