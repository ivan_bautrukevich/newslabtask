package by.bautruk.pandanews.service;

import by.bautruk.pandanews.common.dao.CommentDAO;
import by.bautruk.pandanews.common.entity.Comment;
import by.bautruk.pandanews.common.exception.DAOException;
import by.bautruk.pandanews.common.exception.ServiceException;
import by.bautruk.pandanews.common.service.impl.CommentServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * @author ivan.bautruvkevich
 *         4/21/2016 8:55 AM.
 */
public class CommentServiceTest {

    @InjectMocks
    CommentServiceImpl commentService;

    @Mock
    CommentDAO commentDAO;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void takeById() throws DAOException, ServiceException {
        Long id = 1L;
        Comment comment = new Comment();
        when(commentDAO.takeById(id)).thenReturn(comment);
        Comment commentTest = commentService.takeById(id);
        assertEquals(comment.getId(), commentTest.getId());
    }

    @Test
    public void takeAll() throws DAOException, ServiceException {
        List<Comment> comments = new ArrayList<>();
        comments.add(new Comment());
        comments.add(new Comment());
        when(commentDAO.takeAll()).thenReturn(comments);
        List<Comment> commentsTest = commentService.takeAll();
        assertEquals(comments.size(), commentsTest.size());
    }

    @Test
    public void insert() throws ServiceException, DAOException {
        Comment comment = new Comment();
        comment.setId(1L);
        comment.setCommentText("test");
        when(commentDAO.insert(comment)).thenReturn(1L);
        Long id = commentService.insert(comment);
        assertEquals(id, comment.getId());
    }

    @Test
    public void deleteById() throws DAOException {

    }

    @Test
    public void updateById() throws DAOException, ServiceException {
        Long id = 1L;
        Comment comment = new Comment();
        comment.setId(id);
        when(commentDAO.updateById(id, comment)).thenReturn(true);
        boolean result = commentService.updateById(id, comment);
        assertEquals(result, true);
    }

    @Test
    public void takeCommentByNewsId() throws DAOException, ServiceException {
       //// TODO: 5/12/2016 implement test
    }
}
