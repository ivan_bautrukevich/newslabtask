package by.bautruk.pandanews.service;

import by.bautruk.pandanews.common.dao.UserDAO;
import by.bautruk.pandanews.common.entity.User;
import by.bautruk.pandanews.common.exception.DAOException;
import by.bautruk.pandanews.common.exception.ServiceException;
import by.bautruk.pandanews.common.service.impl.UserServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * @author ivan.bautruvkevich
 *         4/21/2016 8:57 AM.
 */
public class UserServiceTest {

    @InjectMocks
    UserServiceImpl userService;

    @Mock
    UserDAO userDAO;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void takeById() throws DAOException, ServiceException {
        Long id = 1L;
        User user = new User();
        when(userDAO.takeById(id)).thenReturn(user);
        User userTest = userService.takeById(id);
        assertEquals(user.getId(), userTest.getId());
    }

    @Test
    public void takeAll() throws DAOException, ServiceException {
        List<User> users = new ArrayList<>();
        users.add(new User());
        users.add(new User());
        when(userDAO.takeAll()).thenReturn(users);
        List<User> userTest = userService.takeAll();
        assertEquals(users.size(), userTest.size());
    }

    @Test
    public void insert() throws ServiceException, DAOException {
        User user = new User();
        user.setId(1L);
        user.setName("test");
        when(userDAO.insert(user)).thenReturn(1L);
        Long id = userService.insert(user);
        assertEquals(id, user.getId());
    }

    @Test
    public void deleteById() throws DAOException {

    }

    @Test
    public void updateById() throws DAOException, ServiceException {
        Long id = 1L;
        User user = new User();
        user.setId(id);
        when(userDAO.updateById(id, user)).thenReturn(true);
        boolean result = userService.updateById(id, user);
        assertEquals(result, true);
    }
}
