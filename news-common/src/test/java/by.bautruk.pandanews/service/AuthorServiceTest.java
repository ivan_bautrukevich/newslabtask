package by.bautruk.pandanews.service;

import by.bautruk.pandanews.common.dao.AuthorDAO;
import by.bautruk.pandanews.common.entity.Author;
import by.bautruk.pandanews.common.exception.DAOException;
import by.bautruk.pandanews.common.exception.ServiceException;
import by.bautruk.pandanews.common.service.impl.AuthorServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * @author ivan.bautruvkevich
 *         4/21/2016 8:56 AM.
 */
public class AuthorServiceTest {

    @InjectMocks
    AuthorServiceImpl authorService;

    @Mock
    AuthorDAO authorDAO;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void takeById() throws DAOException, ServiceException {
        Long id = 1L;
        Author author = new Author();
        when(authorDAO.takeById(id)).thenReturn(author);
        Author authorTest = authorService.takeById(id);
        assertEquals(author.getId(), authorTest.getId());
    }

    @Test
    public void takeAll() throws DAOException, ServiceException {
        List<Author> authors = new ArrayList<>();
        authors.add(new Author());
        authors.add(new Author());
        when(authorDAO.takeAll()).thenReturn(authors);
        List<Author> authorsService = authorService.takeAll();
        assertEquals(authors.size(), authorsService.size());
    }

    @Test
    public void insert() throws ServiceException, DAOException {
        Author author = new Author();
        author.setId(1L);
        author.setName("test");
        when(authorDAO.insert(author)).thenReturn(1L);
        Long id = authorService.insert(author);
        assertEquals(id, author.getId());
    }

    @Test
    public void deleteById() throws DAOException {

    }

    @Test
    public void updateById() throws DAOException, ServiceException {
        Long id = 1L;
        Author author = new Author();
        author.setId(id);
        when(authorDAO.updateById(id, author)).thenReturn(true);
        boolean result = authorService.updateById(id, author);
        assertEquals(result, true);
    }

    @Test
    public void takeByNewId() throws DAOException, ServiceException {
        Long id = 1L;
        List<Author> authors = new ArrayList<>();
        authors.add(new Author());
        authors.add(new Author());
        when(authorDAO.takeByNewsId(id)).thenReturn(authors);
        List<Author> authorsTest = authorService.takeByNewsId(id);
        assertEquals(authorsTest.size(), 2);
    }

    @Test
    public void createLinkWithNewsId() throws DAOException, ServiceException {
        //// TODO: 4/25/2016 write some test
    }

    @Test
    public void deleteLinksWithNewsById() throws DAOException, ServiceException {
        //// TODO: 4/25/2016 write some test
    }
}
