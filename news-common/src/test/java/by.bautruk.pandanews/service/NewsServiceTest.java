package by.bautruk.pandanews.service;

import by.bautruk.pandanews.common.dao.NewsDAO;
import by.bautruk.pandanews.common.entity.News;
import by.bautruk.pandanews.common.exception.DAOException;
import by.bautruk.pandanews.common.exception.ServiceException;
import by.bautruk.pandanews.common.service.impl.NewsServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * @author ivan.bautruvkevich
 *         4/21/2016 8:56 AM.
 */
public class NewsServiceTest {

    @InjectMocks
    NewsServiceImpl newsService;

    @Mock
    NewsDAO newsDAO;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void takeById() throws DAOException, ServiceException {
        Long id = 1L;
        News news = new News();
        when(newsDAO.takeById(id)).thenReturn(news);
        News newsTest = newsDAO.takeById(id);
        assertEquals(news.getId(), newsTest.getId());
    }

    @Test
    public void takeAll() throws DAOException, ServiceException {
        List<News> news = new ArrayList<>();
        news.add(new News());
        news.add(new News());
        news.add(new News());
        when(newsDAO.takeAll()).thenReturn(news);
        List<News> newsTest = newsService.takeAll();
        assertEquals(news.size(), newsTest.size());
    }

    @Test
    public void insert() throws ServiceException, DAOException {
        News news = new News();
        news.setId(1L);
        news.setTitle("test");
        when(newsDAO.insert(news)).thenReturn(1L);
        Long id = newsService.insert(news);
        assertEquals(id, news.getId());
    }

    @Test
    public void deleteById() throws DAOException {
//// TODO: 5/17/2016  create this test
    }

    @Test
    public void updateById() throws DAOException, ServiceException {
        Long id = 1L;
        News news = new News();
        news.setId(id);
        when(newsDAO.updateById(id, news)).thenReturn(true);
        boolean result = newsService.updateById(id, news);
        assertEquals(true, result);
    }

    @Test
    public void takeAllSortedNewsByComments() throws ServiceException {
//// TODO: 5/17/2016 create this test
    }
}
