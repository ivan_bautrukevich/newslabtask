package by.bautruk.pandanews.service;

import by.bautruk.pandanews.common.dao.RoleDAO;
import by.bautruk.pandanews.common.entity.Role;
import by.bautruk.pandanews.common.exception.DAOException;
import by.bautruk.pandanews.common.exception.ServiceException;
import by.bautruk.pandanews.common.service.impl.RoleServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * @author ivan.bautruvkevich
 *         4/18/2016 4:51 PM.
 */
public class RoleServiceTest {

    @InjectMocks
    RoleServiceImpl roleService;

    @Mock
    RoleDAO roleDAO;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void takeById() throws DAOException, ServiceException {
        Long id = 1L;
        Role role = new Role();
        when(roleDAO.takeById(id)).thenReturn(role);
        Role roleTest = roleService.takeById(id);
        assertEquals(role.getId(), roleTest.getId());
    }

    @Test
    public void takeAll() throws DAOException, ServiceException {
        List<Role> roles = new ArrayList<>();
        roles.add(new Role());
        roles.add(new Role());
        when(roleDAO.takeAll()).thenReturn(roles);
        List<Role> rolesTest = roleService.takeAll();
        assertEquals(roles.size(), rolesTest.size());
    }

    @Test
    public void insert() throws ServiceException, DAOException {
        Role role = new Role();
        role.setId(1L);
        role.setName("test");
        when(roleDAO.insert(role)).thenReturn(1L);
        Long id = roleService.insert(role);
        assertEquals(id, role.getId());
    }

    @Test
    public void deleteById() throws DAOException {

    }

    @Test
    public void updateById() throws DAOException, ServiceException {
        Long id = 1L;
        Role role = new Role();
        role.setId(id);
        when(roleDAO.updateById(id, role)).thenReturn(true);
        boolean result = roleService.updateById(id, role);
        assertEquals(result, true);
    }
}
