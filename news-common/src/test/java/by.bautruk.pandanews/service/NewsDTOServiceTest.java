package by.bautruk.pandanews.service;

import by.bautruk.pandanews.common.exception.ServiceException;
import by.bautruk.pandanews.common.service.AuthorService;
import by.bautruk.pandanews.common.service.NewsService;
import by.bautruk.pandanews.common.service.TagService;
import by.bautruk.pandanews.common.service.impl.NewsDTOServiceImpl;
import by.bautruk.pandanews.common.transferobject.NewsDTO;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

/**
 * @author ivan.bautruvkevich
 *         4/21/2016 8:57 AM.
 */
public class NewsDTOServiceTest {
    @InjectMocks
    NewsDTOServiceImpl newsDTOService;

    @Mock
    NewsService newsService;

    @Mock
    TagService tagService;

    @Mock
    AuthorService authorService;


    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addNewsTest() throws ServiceException {
        NewsDTO newsDTO = new NewsDTO();
//        when(newsService.insert(newsDTO.getNews()));
//// TODO: 4/25/2016 create test
    }

    @Test
    public void updateNewsTest() {
//// TODO: 4/25/2016 create test
    }

    @Test
    public void takeNewsTest() {
//// TODO: 4/25/2016 create test
    }
}
