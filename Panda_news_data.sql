insert into "ROLES" (ROLE_ID, ROLE_NAME)
values (1, 'user');
insert into "ROLES" (ROLE_ID, ROLE_NAME)
values (2, 'admin');

insert into Users (ROLE_ID, USER_NAME, LOGIN, PASSWORD) 
values (2, 'Ivan', 'ivan', 'ivan');
insert into Users (ROLE_ID, USER_NAME, LOGIN, PASSWORD)
values (1, 'Andrew', 'andrew', 'andrew');
insert into Users (ROLE_ID, USER_NAME, LOGIN, PASSWORD)
values (1, 'Anna', 'anna', 'anna');
insert into Users (ROLE_ID, USER_NAME, LOGIN, PASSWORD)
values (1, 'Olga', 'olga', 'olga');
insert into Users (ROLE_ID, USER_NAME, LOGIN, PASSWORD)
values (1, 'Sergey', 'sergey', 'sergey');
insert into Users (ROLE_ID, USER_NAME, LOGIN, PASSWORD)
values (1, 'Yan', 'yan', 'yan');
insert into Users (ROLE_ID, USER_NAME, LOGIN, PASSWORD)
values (1, 'Sveta', 'sveta', 'sveta');
insert into Users (ROLE_ID, USER_NAME, LOGIN, PASSWORD)
values (1, 'Vladimir', 'vladimir', 'vladimir');
insert into Users (ROLE_ID, USER_NAME, LOGIN, PASSWORD)
values (1, 'Sofia', 'sofia', 'sofia');
insert into Users (ROLE_ID, USER_NAME, LOGIN, PASSWORD)
values (1, 'Oleg', 'oleg', 'oleg');
insert into Users (ROLE_ID, USER_NAME, LOGIN, PASSWORD)
values (1, 'Ilya', 'ilya', 'ilya');
insert into Users (ROLE_ID, USER_NAME, LOGIN, PASSWORD)
values (1, 'Ira', 'ira', 'ira');

insert into TAGS (TAG_ID, TAG_NAME)
values (1, 'politics');
insert into TAGS (TAG_ID, TAG_NAME)
values (2, 'sport');
insert into TAGS (TAG_ID, TAG_NAME)
values (3, 'culture');
insert into TAGS (TAG_ID, TAG_NAME)
values (4, 'finance');
insert into TAGS (TAG_ID, TAG_NAME)
values (5, 'car');
insert into TAGS (TAG_ID, TAG_NAME)
values (6, 'people');
insert into TAGS (TAG_ID, TAG_NAME)
values (7, 'world');
insert into TAGS (TAG_ID, TAG_NAME)
values (8, 'food');
insert into TAGS (TAG_ID, TAG_NAME)
values (9, 'travel');
insert into TAGS (TAG_ID, TAG_NAME)
values (10, 'nature');

insert into AUTHORS (AUTHOR_ID, AUTHOR_NAME, EXPIRED)
values (1, 'Dave Lee', TIMESTAMP'1999-12-02 10:00:00 -3:00');
insert into AUTHORS (AUTHOR_ID, AUTHOR_NAME, EXPIRED)
values (2, 'Достоевский', TIMESTAMP'1999-12-02 10:00:00 -3:00');
insert into AUTHORS (AUTHOR_ID, AUTHOR_NAME, EXPIRED)
values (3, 'Eric Weiner', TIMESTAMP'1999-12-02 10:00:00 -3:00');

insert into NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
values (1, 'Magnetic Hyperloop pod unveiled at MIT', 'A people-carrying pod designed to levitate and travel at extremely
 high speeds is unveiled in Boston, US.', 'A people-carrying pod designed to levitate and travel at extremely high
 speeds has been unveiled in Boston.
A 30-strong team from the Massachusetts Institute of Technology (MIT) is one of several groups and companies working on
 making the Hyperloop concept a reality.
The idea, first envisioned by Tesla founder Elon Musk, is to create a transport system that propels pods through
airtight tubes.
The MIT team said its pod design paved the way for "a mode transportation that could change how we think about travel".
Critics of Hyperloop say it is unlikely to succeed because of prohibitive costs.
A white paper by Mr Musk published in 2013 proposed a Hyperloop tube connection from San Francisco to Los Angeles. At
speeds of around 700mph (1,127km/h), Mr Musk predicted the journey time would be around 30 minutes.
Right now, travellers face either a six-hour drive, or just under an hour of flying.
Magnets
Critics of the Hyperloop concept say it will prove to be prohibitively expensive to realise, while others say it may be
uncomfortable for riders.
Through his SpaceX firm, Mr Musk will be funding a series of tests in tubes, expected to begin around August this year.
MIT''s droplet-shaped pod uses magnets to lift itself off the aluminium track, reducing friction.It is not currently
big enough to carry a human being, but the team said once the full testing is complete it would be relatively
straightforward to scale up to full size.
There are hurdles, however - the team said making the pod turn, even slightly, was a "huge problem".
Chief engineer Christopher Merian said while the team was confident with the levitation systems, the brakes needed
"more testing".
MIT''s unveiling took place in the same week another group developing the technology, a firm called Hyperloop One,
tested its propulsion system in the Nevada desert.',
 TIMESTAMP'14-MAY-16', NULL);
insert into NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
values (2, 'Lightning kills 50 in Bangladesh', 'Police in Bangladesh say more than 50 people across the country have
died after being struck by lightning in the past two days.', 'More than 50 people across Bangladesh have died after
being struck by lightning in the past two days, police say.
Many of the dead were farmers who were killed as they worked in their paddy fields.
Bangladesh is prone to electrical storms but this year they have been particularly severe.
Experts suggest a general rise in temperatures and deforestation may be factors.
Other victims included two students in the capital, Dhaka, who were struck as they played football, and a teenage boy
who died when he went to collect mangoes.
About 90 people have been killed by lightning since March, compared to a total of 51 people in the whole of 2015, Voice
of America (VoA) reported.
The head of Bangladesh''s disaster management body Mohammad Riaz Ahmed told VoA he was "indeed concerned" by the rise
in the number of deaths.
He said further thunderstorms were predicted for later this month.
Strong tropical storms regularly hit Bangladesh ahead of and during the monsoon season, which runs from June to
September.
', TIMESTAMP'05-APR-16', NULL);
insert into NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
values (3, 'Van Gaal expects to keep Man Utd job', 'Manchester United manager Louis van Gaal expects to serve the final
12 months of the three-year contract he signed in 2014.', 'Louis van Gaal thinks he will still be Manchester United
manager next season.
The Dutchman has not been given any assurance on his future but he expects to be serving the final season of a
three-year contract he signed in 2014.
He said: "I have said I shall be here, that is my opinion, so the board has to decide if it is like that."
Van Gaal has been under intense pressure since December and former Chelsea manager Jose Mourinho is known to have
spoken with United officials.
United, who announced record third quarter results on Friday and said they expect overall revenue to pass £500m for
the year, know they will miss out on the Champions League for the second time in three seasons if Manchester City avoid
defeat in their final game at Swansea on Sunday.
With a place in the FA Cup final against Crystal Palace on 21 May already assured, Van Gaal says taking the battle for
a top four place to the final game represents some degree of success.
"We can win the FA Cup," he said. "And we are still in the race. How many teams can say that? Not many. Our aim was to
qualify. Of course you can say Manchester United need to be champions, yes I know the expectations are like that but I
don''t think that is realistic."
Van Gaal repeated his assertion that the media "sacked" him six months ago.
And whether he stays or not, Van Gaal has already been heavily involved in planning for the 2016-17 campaign, reducing
the club''s pre-season tour to six full days and two matches in China.
He also knows the area he needs to strengthen, having backed out of a potential deal for Renato Sanches, who joined
Bayern Munich from Benfica on Tuesday.
"I don''t think we have to improve too much," he said. "But we need fast, creative players in attack. That is what I
said before this season."
Never want to miss the latest Manchester United news? You can now add United and all the other sports and teams you
follow to your personalised My Sport home.', TIMESTAMP'05-APR-16', '05-APR-16');
insert into NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
values (4, 'The truth about Icelandic happiness', 'Centuries of hardship has resulted in one of the world’s most
resilient nations', 'Several years back, I visited Iceland in the dead of winter. I was researching a book on global
 happiness, and the small Nordic nation intrigued me. What was this country, adrift in the freezing North Atlantic,
 doing perched atop the world’s happiness rankings?
In pursuit of answers, I buttonholed anyone willing to talk, dined on harkl (rotten shark), drank excessively, and,
of course, took a dip in the Blue Lagoon, the otherworldly geothermal waters that have become synonymous with Icelandic
 bliss.
Shortly after I left, Iceland’s largest banks went belly up and the nation’s economy teetered on the verge of collapse,
collateral damage from the global financial meltdown of 2008. The unemployment rate spiked eightfold. Trust in institutions, like the banks and parliament, plummeted.I assumed that the nation’s happiness also nosedived.
I was wrong.
“The economic crisis had a limited effect on happiness,” according to health scientist Dora Gudmundsdottir, author of an
 exhaustive study published in the Social Indicators Researchjournal. Not only did the nation’s overall happiness dip
 only slightly during the crisis, but 25% of Icelanders reported greater happiness. What was going on?
I emailed Karl Blöndal, a newspaper editor I had met in Reykjavik. “A lot of individuals have been hit hard, pensioners
lost their savings. But one thing about living in a small community is that everyone you know is within reach,” he
explained. “Those who lose their jobs are not isolated, the risk of estrangement is not the same as it would be in
bigger societies. Here was an essential truth about Icelandic happiness: it is largely a collective enterprise.
Iceland, even with its cosmopolitan capital of Reykjavik, resembles a small town in many ways. People needn’t worry
about falling into a black hole, Icelanders say, because there is no black hole to fall into you. There’s always someone
 to catch you. As one American immigrant to Iceland told me, if your car is stuck in the snow, someone will always,
 always stop.  In fact, trust levels are so high that it’s not unusual to see six-year-olds walking to school alone in
 the winter darkness.
In a typically optimistic Icelandic statement, Blöndal even managed to see the opportunity embedded in the financial
crisis. “Now we can wipe the slate clean. Who knows – this might just as well be an opportunity to forge a better, more
open society where power is more diffused and the old vested interests and economic blocks have been cleared out of
the way.', TIMESTAMP'05-APR-16', '05-APR-16');

INSERT INTO NEWS_AUTHORS (NEWS_ID, AUTHOR_ID)
VALUES (1, 1);
INSERT INTO NEWS_AUTHORS (NEWS_ID, AUTHOR_ID)
VALUES (2, 2);
INSERT INTO NEWS_AUTHORS (NEWS_ID, AUTHOR_ID)
VALUES (3, 3);

insert into NEWS_TAGS (NEWS_ID, TAG_ID)
values (1, 1);
insert into NEWS_TAGS (NEWS_ID, TAG_ID)
values (2, 2);
insert into NEWS_TAGS (NEWS_ID, TAG_ID)
values (3, 3);

insert into COMMENTS (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
values (1, 1, 'Ужас', TIMESTAMP'2016-01-01 10:00:00 -3:00');
insert into COMMENTS (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
values (2, 2, 'Супер', TIMESTAMP'2016-01-01 10:00:00 -3:00');
insert into COMMENTS (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
values (3, 3, 'Отлично', TIMESTAMP'2016-01-01 10:00:00 -3:00');